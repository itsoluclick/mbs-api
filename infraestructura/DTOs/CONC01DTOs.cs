﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestructura.DTOs
{
    public class CONC01DTOs
    {
        public int? NUM_REG;
        public short? NUM_CPTO;
        public string DESCR;
        public string TIPO;
        public string CUEN_CONT;
        public string CON_REFER;
        public short? GEN_CPTO;
        public string T_CRED;
        public string CATEGORIA;
        public string TIPO_TRANS;
        public string NC;
        public double? RETENCION;
        public string TIPO_REF;
        public string USA_BANCO;
    }
}
