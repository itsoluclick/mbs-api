﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestructura.DTOs
{
   public class TB_REC_DETALLEDTOs
    {
		public string CCLIE;
		public int? REC_AUTO;
		public string REC_MANUAL;
		public float? MONTO_TOT_AUTO;
		public float? MONTO_TOT_MAN;
		public string FACTURA;
		public DateTime? FECHA_REC;
		public int? TIPO_MOV1;
		public float? MONTOAPLICA1;
		public int? TIPO_MOV2;
		public float? MONTOAPLICA2;
		public int? TIPO_MOV3;
		public float? MONTOAPLICA3;
		public int? COD_COBRADOR;
		public float? SALDOANTERIOR;
		public string ALMACEN;
		public int? TIPO_PAGO;
		public string NOTA_CREDITO;
		public string NUM_MONED;
		public float TCAMBIO;
	}
}
