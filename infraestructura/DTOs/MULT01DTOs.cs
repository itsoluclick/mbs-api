﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestructura.DTOs
{
  public  class MULT01DTOs
    {
        public int NUM_REG;
        public string CLV_ART;
        public string STATUS;
        public short? ALMACEN;
        public string CTRL_ALM;
        public double? EXIST;
        public double? STOCK_MIN;
        public double? STOCK_MAX;
        public double? COMP_X_REC;
        public double? EXISTREAL;
        public double? EXISTCONG;
        public short? BSECAPTIF;
        public double? DB8INVFIS;
        public string STRCAMPO1;
        public string STRCAMPO2;
        public string STRCAMPO3;
        public short? NUMCAMPO4;
        public double? NUMCAMPO5;
        public double? NUMCAMPO6;
        public double? Apart;
        public double? Invefisico;
        public string USUARIO_TOMAINVE;
        public double? LUNES;
        public double? MARTES;
        public double? MIERCOLES;
        public double? JUEVES;
        public double? VIERNES;
        public double? SABADO;
        public double? DIFERENCIA;
        public double? Cantidad_saco;
        public int SECUEN;
        public string ENOFERTA;
        public string PASILLOs;
        public string Lado;
        public string Bandeja;
        public string Seccion;
        public double? EXIST_ANTE;
    }
}
