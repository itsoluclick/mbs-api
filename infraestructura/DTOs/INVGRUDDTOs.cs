﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestructura.DTOs
{
   public class INVGRUDDTOs
    {
        public int INVGRU_CODIGO;
        public string INVGRU_DESCRIPCION;
        public int INVGRU_MASTER;
        public int? Id_grupo_padre;
    }
}
