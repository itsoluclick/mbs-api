﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestructura.DTOs
{
   public class IMPU01DTOs
    {
		public int? NUM_REG;
		public string CVEESQIMPU;
		public string DESCRIPESQ;
		public float? IMPUESTO1;
		public Int16? IMP1APLICA;
		public float? IMPUESTO2;
		public Int16? IMP2APLICA;
		public float? IMPUESTO3;
		public Int16? IMP3APLICA;
		public float? IMPUESTO4;
		public Int16 IMP4APLICA;
	}
}
