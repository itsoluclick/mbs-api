﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestructura.DTOs
{ 
   public class MOVS0101DTOs
	{
	   public int? NUM_REG;
		public short TIP_REG;
		public short CON_PART;
		public short CTA_BAN;
		public string CVE_CONCEP;
		public int? NUM_CHEQUE;
		public string REF;
		public string STATUS;
		public DateTime? FECHA;
		public DateTime? F_COBRO;
		public string TIPO;
		public string BAND_PRN;
		public string BAND_CONT;
		public string BAND_CONC;
		public string ACT_SAE;
		public string NUM_POL;
		public string TIP_POL;
		public string COBRADO;
		public short SAE_COI;
		public double? MONTO_TOT;
		public double? T_CAMBIO;
		public int? HORA;
		public string CLPV;
		public string FACT;
		public string REFER;
		public double? MONTO_DOC;
		public short CTA_TRANSF;
		public string TRANSFER;
		public string BENEF;
		public int? OBSER;
		public DateTime? FECHA_LIQ;
		public short CVE_INST;
		public string MONDIFSAE;
		public double? TCAMBIOSAE;
		public string RFC;
		public short IVA;
		public double? MONIVAACR;
		public short CONC_SAE;
		public string SOLICIT;
		public int? TRANS_COI;
		public short INTSAENOI;
		public string FORMA_PAGO;
		public int? REVISADO;
		public char? ASOCIADO;
		public int? id_projecto;
		public int IDNUM_REG;
	}
}

