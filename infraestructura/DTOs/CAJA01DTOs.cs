﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestructura.DTOs
{
   public class CAJA01DTOs
    {
		public int ID_CAJA;
		public string DESCR_CAJA;
		public string ABRECAJON;
		public int ALMACEN;
		public string LETRA;
		public int? Sec_Facturacion;
		public int? SEC_DEVOLUCION;
		public int? SEC_COTIZACION;
		public string EQUIPO;
		public string tienda;
		public int? NIVEL;
		public string CAJA_FISCAL;
		public string CONCOPIA_FISCAL;
		public int? SEC_PEDIDO;
		public string AMitad;
		public string AdicionarTarjeta;
		public string usamagellan;
		public string magellanc;
		public int? PRINTERFISCAL;
		public int? TIPOTARJETA;
		public int? TIPOTARJETA2;
		public int? COPIATARJETA;
		public float? magellandivide;
		public int? magellantimeout;
		public int? abrecajonextencion;
		public string CAJON;
		public int? TIPOAZUL;
		public string VALIDAPRECIOCERO;
		public string multipantalla;
		public string ZEBRASCAN;
		public string puertobixolon;
	}
}
