﻿using System;

namespace Infraestructura.DTOs
{

    public class OrdenesCompraDTOs
    {
        public string documento { get; set; }
        public string CCLIE { get; set; }
        public  DateTime? FECHA_DOC { get; set; }
        public string almacen { get; set; }
        public string cajero { get; set; }
        public string TIP_DOC { get; set; }
        public string NCFMODIFICA { get; set; }
        public string SUREFER { get; set; }
        public int? ALMACEN_ENT { get; set; }
        public DateTime? FECHA_ENT { get; set; }
        public int MONEDA { get; set; }
        public double? TASA { get; set; }
        public string tipodocumento { get; set; }
        public string observacion { get; set; }




    }
}