﻿using System;

namespace Infraestructura.DTOs
{
    public  class CUEN01DTOs
	
{
	public int? NUM_REG;
		public string CCLIE;
		public string STATUS;
		public Int16? TIPO_MOV;
		public string NO_FACTURA;
		public string DOCTO;
		public string REFER;
		public float? IMPORTE;
		public DateTime? FECHA_APLI;
		public DateTime? FECHA_VENC;
		public string COB;
		public string AFEC_COI;
		public int? OBS_CXCCXP;
		public int? BANK_COM;
		public string STRCVEVEND;
		public string NUM_MONED;
		public float? TCAMBIO;
		public float? IMPMON_EXT;
		public string CCONREFER;
		public DateTime? FECHAELAB;
		public Int16? USUARIO;
		public string NCF;
		public int? REC_AUTO;
		public string DOCUMENTO;
		public string OBSERVACION;
		public float? IMPUESTO;
		public float? PORC_COMISION;
		public int? ALMACEN;
		public string TIPO_TRANS;
		public string cobranza;
		public int? COD_CONDICION_PAGO;
		public int? cod_dev;
		public string AFEC_CXP;
		public string cclie_old;
		public string VEND_OLD;
		public string contabilizado;
		public float? importe_anterior;
		public int? USUARIO_CANCELA;
	}
}
