﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestructura.DTOs
{
	public class DGII_RNCDTOs
	{
		public string RNC;
		public string EMPRESA;
		public string EMPRESA2;
		public string ACTIVIDAD;
		public string CAMPO1;
		public string CAMPO2;
		public string CAMPO3;
		public string CAMPO4;
		public string FECHA;
		public string NORMAL;
		public string ESTATUS;
		public string TIPO_NCF;
	}
}
