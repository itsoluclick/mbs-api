﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestructura.DTOs
{
  public   class FA0TY1DTOs
    {
		public int NUM_REG;
		public string TIP_REG;
		public string TIP_DOC;
		public string CVE_DOC;
		public string CVE_ART;
		public string CVE_CLPV;
		public DateTime? FECHA_DOC;
		public string USUARIO;
		public float? CANT;
		public float? PXS;
		public float? PREC;
		public float? COST;
		public float? IMPU1;
		public float? IMPU2;
		public float? DESC1;
		public float? DESC2;
		public float? DESC3;
		public float? COMI;
		public Int16? PAR_ANT;
		public string TIP_ANT;
		public string DOC_ANT;
		public Int16? PAR_SIG;
		public string TIP_SIG;
		public string DOC_SIG;
		public Int16? PAR_ASIG;
		public string TIP_ASIG;
		public string DOC_ASIG;
		public string ACT_INV;
		public Int16? NUM_ALM;
		public string POLITS;
		public float? TIP_CAM;
		public int? REG_SERIE;
		public int? REG_OBS;
		public float? APAR;
		public string UNI_VENTA;
		public string TIPO_PROD;
		public int? U4SEC;
		public float? MONFLETE;
		public float? IMPU3;
		public float? IMPU4;
		public Int16? IMP1APLA;
		public Int16? IMP2APLA;
		public Int16? IMP3APLA;
		public Int16? IMP4APLA;
		public int? IDDATOSADICIONALES;
		public string obs_prod;
		public int? secuencia;
		public int? sec_fact_dev;
		public int? autoriza_desc;
		public string OBSDETALLE;
		public float descuento;
		public float? pxsc;
		public string CLAVE_ALTERNA;
		public string lote;
	}
}
