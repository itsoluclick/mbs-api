﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestructura.DTOs
{
	public class MARCADTOs
	{
		public int CODIGO_MARCA;
		public string DESCR_MARCA;
		public int CODIGO_FAB;
		public string ESTADO;
	}
}
