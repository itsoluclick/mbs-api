﻿using System;

namespace Infraestructura.DTOs
{
   public class CTAS01DTOs
    {
		public int NUM_REG;
		public Int16 CVE_CTA;
		public string STATUS;
		public string NUM_CTA;
		public string BANCO;
		public string SUCURSAL;
		public Int16 MONEDA;
		public string CTA_CONTAB;
		public Int16 DIA_CORTE;
		public int SIG_CHEQUE;
		public float SALDO_INI;
		public float SDO_CONCIL;
		public float TOT_CARGOS;
		public float TOT_ABONOS;
		public float T_CAR_TRAN;
		public float T_ABO_TRAN;
		public float LIM_CRED;
		public Int16 TIPO_CTA;
		public DateTime FECH_APER;
		public float PAGO_MIN;
		public string CHE_FTO;
		public byte[] BMP_BAN;
		public string CONTACTO;
		public string TELEFONO;
		public string LEYEN_PST;
		public int COMISION;
		public float T_ACCIONES;
		public float CAR_INVERS;
		public float ABO_INVERS;
		public string RESTO;

	}
}
