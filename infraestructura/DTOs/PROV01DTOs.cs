﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestructura.DTOs
{
  public  class PROV01DTOs
    {
        public int? NUM_REG;
        public string CPROV;
        public string STATUS;
        public string NOMBRE;
        public string RFC;
        public string DIR;
        public string POB;
        public string CODIGO;
        public string TELEFONO;
        public string ATENCION;
        public string ATEN_COB;
        public string REV_PAG;
        public string CLASIFIC;
        public short? DIAS_CRE;
        public DateTime? FCH_ULTCOM;
        public string VEND;
        public double? DESCUENTO;
        public double? LIM_CRED;
        public double? SALDO;
        public double? VTAS;
        public int? OBSER;
        public string COLONIA;
        public string FAX;
        public string EMAIL;
        public string CURP;
        public string CVE_ZONA;
        public string TELEFONOS;
        public string ULT_PAGOD;
        public string ULT_VENTD;
        public string CAMLIBRE1;
        public string CAMLIBRE2;
        public string CAMLIBRE3;
        public DateTime? ULTPAGOF;
        public string RETXFLETE;
        public short? CAMLIBRE4;
        public double? PORCRETEN;
        public double? CAMLIBRE5;
        public double? ULT_PAGOM;
        public double? ULT_VENTM;
        public double? CAMLIBRE6;
    }
}

