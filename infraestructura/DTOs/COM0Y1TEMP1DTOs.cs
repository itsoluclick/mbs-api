﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestructura.DTOs
{
  public  class COM0Y1TEMP1DTOs
    {
        public int? NUM_REG;
        public string TIP_REG;
        public string TIP_DOC;
        public string CVE_DOC;
        public string CVE_ART;
        public string CVE_CLPV;
        public DateTime? FECHA_DOC;
        public string USUARIO;
        public double? CANT;
        public double? PXS;
        public double? PREC;
        public double? COST;
        public double? IMPU1;
        public double? IMPU2;
        public double? DESC1;
        public double? DESC2;
        public double? DESC3;
        public double? COMI;
        public short? PAR_ANT;
        public string TIP_ANT;
        public string DOC_ANT;
        public short? PAR_SIG;
        public string TIP_SIG;
        public string DOC_SIG;
        public short? PAR_ASIG;
        public string TIP_ASIG;
        public string DOC_ASIG;
        public string ACT_INV;
        public short? NUM_ALM;
        public string POLITS;
        public double? TIP_CAM;
        public int? REG_SERIE;
        public int? REG_OBS;
        public double? APAR;
        public string UNI_VENTA;
        public string TIPO_PROD;
        public int? U4SEC;
        public double? MONFLETE;
        public double? IMPU3;
        public double? IMPU4;
        public short? IMP1APLA;
        public short? IMP2APLA;
        public short? IMP3APLA;
        public short? IMP4APLA;
        public int? IDDATOSADICIONALES;
        public string Obs_prod;
        public int? NUMREGCONTACTO;
        public int Secuencia;
        public int? Sec_fact_dev;
        public double? Ofertas;
        public double? Margen;
        public double? Precio_sugerido;
        public double? PRECIO_NUEVO;
        public string Fecha_vencimiento;
        public int? Depto_id;
        public double? TASA;
        public string CLAVE_ALTERNA;
        public double? BIEN;
        public double? SERVICIO;
        public string APLICA_PROPORCION;
        public string LLEVAR_AL_GASTO;
    }
}
