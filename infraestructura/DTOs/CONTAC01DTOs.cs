﻿namespace Infraestructura.DTOs
{
    public   class CONTAC01DTOs
    {
        public int NUM_REG;
        public string USRNAMESAE;
        public string CCLIE;
        public string NOMBRE;
        public string DIRECCION;
        public string TELEFONO;
        public string EMAIL;
        public string TIPOCONTAC;
        public int? IDDEPTO;
        public int Secu;
        public string CEDULA;
        public int? CLV_TIPO;
        public string NO_SOCIO;
    }
}
