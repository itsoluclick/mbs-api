﻿using System;

namespace Infraestructura.DTOs
{

    public class DocumentosDTOs
    {
        public string NumFact { get; set; }
        public string IdCliente { get; set; }
        public DateTime? fecha { get; set; }
        public string vend { get; set; }
        public string NumAlma { get; set; }
        public double? efectivo { get; set; }
        public double? tarjeta1 { get; set; }
        public double? tarjeta2 { get; set; }
        public double? cheque { get; set; }
        public double? nota { get; set; }
        public double? credito { get; set; }
        public double? devolver { get; set; }
        public string UID { get; set; }
        public string Observacion { get; set; }
        public string Condicion { get; set; }

        public string DescClie { get; set; }
        public string DocClie { get; set; }
        public int? IdPc { get; set; }
        public string CodCliPref { get; set; }
        public double? acobrar { get; set; }
        public string aprobacion1 { get; set; }
        public string aprobacion2 { get; set; }
        public string aprobacionck { get; set; }
        public string aprobacionNota { get; set; }
        public string RNC { get; set; }
        public string Rbono { get; set; }
        public string aprobacionpuntos { get; set; }
        public string aprobacionpuntos2 { get; set; }
        public double? puntos { get; set; }
        public double? puntos2 { get; set; }
        public double? bono { get; set; }
        public double? otrospagos { get; set; }
        public string OtroDoc { get; set; }
        public double? transferencia { get; set; }
        public string RTransf { get; set; }
        public double? RetencionItbis { get; set; }
        public string aprobRetencionitbis { get; set; }



    }
}