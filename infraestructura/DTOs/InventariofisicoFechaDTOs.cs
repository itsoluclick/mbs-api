﻿using System;

namespace Infraestructura.DTOs
{
    public  class InventariofisicoFechaDTOs
    {
		public string clv_art;
		public double? exist;
		public DateTime? fecha;
		public int? almacen;
		//public int IDNUM_REG;
	}
}
