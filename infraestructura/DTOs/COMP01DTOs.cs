﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestructura.DTOs
{
 public   class COMP01DTOs
    {
        public int? NUM_REG;
        public string TIP_REG;
        public string TIP_DOC;
        public string TIP_REF;
        public string CVE_DOC;
        public string CVE_REF;
        public string CVE_CLPV;
        public string STATUS;
        public string SU_REFER;
        public DateTime? FECHA_DOC;
        public DateTime? FECHA_REC;
        public DateTime? FECHA_PAG;
        public double? CAN_TOT;
        public double? IMP_TOT1;
        public double? IMP_TOT2;
        public double? DES_TOT;
        public double? DES_FIN;
        public double? TOT_IND;
        public string OBS_COND;
        public int? OBS_COMP;
        public short? NUM_PART;
        public short? NUM_ALMA;
        public string ACT_CXP;
        public string ACT_COI;
        public short? BLOQ;
        public string NUM_MONED;
        public double? TIPCAMB;
        public string DOCANTSIG;
        public short? NOPAGOS;
        public double? COSTOFLET;
        public double? IMPUEFLET;
        public double? RETENFLET;
        public DateTime? FECHA_ENV;
        public DateTime? FECHAELAB;
        public short? USUARIO;
        public double? IMP_TOT3;
        public double? IMP_TOT4;
        public string CVE_VEND;
        public string NCFMODIFICA;
        public double? RETENCION;
        public double? RETENCION2;
        public double? RETENCION10;
        public double? RETENCION25;
        public int? CP;
        public string TIPODOCUMENTO;
        public string TIPOPROVEEDOR;
        public string OBS_COND_OLD;
        public string NCF;
        public string ID_NCF;
        public int? Almacen_ent;
        public string APROBADA;
        public string Recepcionada;
        public int? Usuario_autoriza;
        public string Prec_act;
        public double? EFECTIVO;
        public double? TARJETA1;
        public string APROBACIONTARJETA;
        public double? CHEQUE;
        public string APROBACIONCHEQUE;
        public double? Pagocxp;
        public double? BIEN;
        public double? SERVICIO;
        public double? ISC;
        public double? PROPINA;
        public string TIPORET;
        public double? TASA;
        public double Notacredito;
        public double Aprobacionnota;
        public DateTime? Fecha_retencion;
        public string Contabilizado;
        public double? Monto_gravado;
        public double? Monto_exento;
        public string APLICA_PROPORCION;
        public string LLEVAR_AL_GASTO;
    }
}
