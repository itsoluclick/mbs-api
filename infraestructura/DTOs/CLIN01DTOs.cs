﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestructura.DTOs
{
   public class CLIN01DTOs
    {
		public int? NUM_REG;
		public string CLV_LIN;
		public string DESC_LIN;
		public string CUENTA_COI;
		public string cuenta_coi_costo;
		public string CUENTA_COI_VENTA;
	}
}
