﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestructura.DTOs
{
   public class CajaChica_DETDTOs
    {
		public int? CODIGO_CAJA;
		public int? cve_doc;
		public string RNC_CED;
		public DateTime? fecha_Trans;
		public float? IMPORTE;
		public float? IMPUESTO;
		public string NCF;
		public string TIPO_GASTO;
		public string CUENTA_CONTA;
		public int? Gasto_menor_Cve_Doc;
		public int secuencia;
		public string tip_doc;
		public int? DEPTO;
		public float propina;
	}
}
