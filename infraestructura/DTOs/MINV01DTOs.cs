﻿using System;

namespace Infraestructura.DTOs
{

    public class MINV01DTOs
	{
		public int? NUM_REG;
		public string CLV_ART;
        public Int16? TIPO_MOV;
        public DateTime? FECHA_DOCU;
        public string REFER;
		public string CLAVE_CLPV;
		public string VEND;
		public float? CANT;
		public float? CANT_COST;
		public float? PRECIO;
        public float? COSTO;
        public Int16? ALMACEN;
        public string AFEC_COI;
		public int? OBS_MINV;
		public int? REG_SERIE;
		public string UNI_VENTA;
		public int? U4SEC;
		public float? DB8EXIST;
		public string TIPO_PROD;
		public float? FACTOR_CON;
		public DateTime? FECHAELAB;
		public Int16? USUARIO;
		public float? M_FLETE;
		public int? DOCUMENTO;
		public string OBSERVACION;
		public int? ALMACEN_DESTINO;
		public char? TIPO;
		public int? ALMACEN_ORIGEN;
		public int? CTLPOL;
		public int? CVEFOLIO;
		public string CLV_ART_EQUI;
		public string descr_EQUI;
		public string NOMBRE;
		public string DIRECCION;
		public string TELEFONO;
		public string CONTACTO;
		public string EOS;
		public string COP;
		public string Referencia;
		public string Referencia2;
        public string almacen2;
        public string documento2;
        public string documento3;
        public int secuencia;
        public float? PXT;
        public string contabilizado;
    }
}
