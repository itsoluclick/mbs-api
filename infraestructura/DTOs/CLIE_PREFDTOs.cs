﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestructura.DTOs
{
   public class CLIE_PREFDTOs
    {
        public int Cve_pref;
        public string Tarj_pref;
        public string Saludo;
        public string Nombre;
        public string Empresa;
        public string Direccion;
        public string Correoe;
        public DateTime? Fecha_cumple;
        public string Telefonos;
        public string Estado;
        public string Cedula;
        public int? Almacen;
        public DateTime? Fecha_nacimiento;
        public double? Puntos;
    }
}
