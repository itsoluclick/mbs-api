﻿namespace Infraestructura.DTOs
{
    public   class CONP01DTOs
    {
        public int? NUM_REG;
        public short? NUM_CPTO;
        public string DESCR;
        public string TIPO;
        public string CUEN_CONT;
        public string CON_REFER;
        public short? GEN_CPTO;
        public string T_CRED;
        public string Categoria;
    }
}

