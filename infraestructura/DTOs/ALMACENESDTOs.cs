﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestructura.DTOs
{
   public class ALMACENESDTOs
    {
		public Int16 ALMACEN;
		public char? LETRA;
		public string DESC_ALMACEN;
		public Int16? COD_TIPO_ALMACEN;
		public char? ESTATUS;
		public int? SEC_FACTURA_CONTADO;
		public int? SEC_FACTURA_CREDITO;
		public int? SEC_DEVOLUCION;
		public int? SEC_COTIZACION;
		public int? SEC_PEDIDO;
		public int? SEC_CONDUCE;
		public int? SEC_ENTRADA;
		public int? SEC_SALIDA;
		public string visible_compra;
		public string RUTALOGO;
		public string RUTA_ALM_DOCUMENTOS;
		public int? SEC_FACTURA;
		public bool? IMPRIME_CONDUCE;
		public int? SEC_FACTURACION;
		public int? SEC_RECEPCION_MERC;
		public int? SEC_ORDEN_COMP;
		public int? SEC_DEVOLUCION_MERC;
		public string DEVUELVE;
		public string Toma_inventario;
		public int? SEC_GASTOS;
		public string dir;
		public string Punto_Ventas;
		public string clv_vend;
		public int? sec_dev_gasto;
		public int sec;
		public string TELEFONO;
		public string PrinterEtiqueta;
		public string DESC_INDEPENDIENTE;
		public float? MONTO_OFERTA_IND;
		public float? PORC_OFERTA_IND;
		public string pedir_condicion;
		public string promoSiempre;
		public string descprefalmacen;
		public string Ofertaporcada1000;
		public int? NUM_CUADRE;
		public int? tiempoimagenes;
		public string rutaimagenes;
		public string conduce_factura_varias;
		public string decpromo;
		public int? SEC_TRANSF;
		public string permite_transferencia;
		public string transferencia_abierta;
		public char? cambio_precio_compra;

	}
}
