﻿using System;

namespace Infraestructura.DTOs
{
    public class PedidoDTOs
    {
      //public  string Documento { get; set; }
        public string Cliente { get; set; }
        public DateTime? Fecha_Doc { get; set; }
        public string cve_vend { get; set; }
        public string Almacen { get; set; }
        public string Condicion { get; set; }
    }
}