# MBS API

REST API Sistema MBS

## Requisitos

Visual Studio 2017 

Framework: .NET 4.8

[Postman](https://www.postman.com/)

## Registro de Usiario

```
POST: http://localhost:[numero de puerto]/api/Account/Register

{
  "Email": "Email address",
  "Password": "", 
  "ConfirmPassword": ""
}

```

## Obtener token de seguridad 

```
POST: http://localhost:[numero de puerto]/token

Body:

username=me@correo.com&password=mi_clave&grant_type=password

```


## Consultas
Para consultar los API de productos, clientes y otros necesita pasar el token por el header del request.

 
#### Header:
### key: Authorization value: Bearer [NUMERO DE TOKEN]

tome en cuenta que el Bearer debe estar delante del numero de token

#### Body:

username=me@correo.com&password=mi_clave&grant_type=password
 

## Otros

El Api está construido utilizando .NET framework y para las consultas de la base de datos utiliza [LINQ](https://www.tutorialsteacher.com/linq/what-is-linq) para manejar los campos y consultas como objetos.

Para agregar más tablas al Linq context abra el archivo MBS-API\DB\MBSDataClasses.dbml y utilizando Server Exprorer puede arrastrar la nueva tabla a utilizar.

```
//inicializar el context 
 MBSDataClassesDataContext db = new MBSDataClassesDataContext();

//Obtener Todos los productos donde el precio1 sea mayor a 100 
 var productos = (from producto in db.INVE01
                            where producto.PRECIO1 > 100
                            select producto
```

### Para agregar un nuevo controlador 
-Click derecho Controllers

-Add -> Controller

-Web API 2 Controller with read/write actions


para hacer que este nuevo controlador sea seguro, recuerde agregar
```
  [Authorize] //Necesita token para aceder a este recurso
```

## Problemas y Mejoras 
Para crear problemas y nuevas funcionalidades es importante usar nuestra cuenta ITSoluclick de [JIRA](https://itsoluclick.atlassian.net/jira)