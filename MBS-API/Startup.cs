﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(MBS_API.Startup))]

namespace MBS_API
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            
        }

        
    } 
}
