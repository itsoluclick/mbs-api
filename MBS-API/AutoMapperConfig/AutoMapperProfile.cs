﻿using AutoMapper;
using MBS_API.Areas.DTOS;
using MBS_API.DB;

namespace MBS_API.AutoMapperConfig
{
    public class AutoMapperProfile:Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<MINV01, MINV01DTO>();
            CreateMap<MINV01DTO, MINV01>();
        }
    }
}