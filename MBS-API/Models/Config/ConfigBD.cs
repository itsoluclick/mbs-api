﻿using MBS_API.DB;
using MBS_API.Models.LogManager;
using System;
using System.Linq;

namespace MBS_API.Models.Config
{
 
    public class ConfigBD
    {
        private MBSDataClassesDataContext db;

        // EmailClient nr = new EmailClient();
        private readonly Log log;
             
        public ConfigBD()
        {
            
            db = new MBSDataClassesDataContext();
            log = new Log();
            
        }
    
        public void ConfiguracionTabla()
        {
            try
            {
                //creacion de tabla  CONFIGURACIONDB
                db.ExecuteCommand("IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CONFIGURACIONDB]') AND type in (N'U')) " +
                                  "DROP TABLE[dbo].[CONFIGURACIONDB] " +
                                  "CREATE TABLE CONFIGURACIONDB (  IDNUM_REGS INT PRIMARY KEY IDENTITY(1, 1) not null,  TABLE_NAME VARCHAR(100),  TABLE_TYPE VARCHAR(100))");
                db.SubmitChanges();
                //creacion de tabla  CONFIGURACIONDBIDEN
                db.ExecuteCommand("IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CONFIGURACIONDBIDEN]') AND type in (N'U')) " +
                                   "DROP TABLE[dbo].[CONFIGURACIONDBIDEN] CREATE TABLE CONFIGURACIONDBIDEN (  IDNUM_REGS  INT PRIMARY KEY IDENTITY(1, 1) NOT null,  TABLE_NAME VARCHAR(100), " +
                                   " COLUMN_NAME VARCHAR(100))");
                db.SubmitChanges();

                //insertar tabla CONFIGURACIONDB
                db.ExecuteCommand("TRUNCATE TABLE CONFIGURACIONDB INSERT INTO CONFIGURACIONDB(TABLE_NAME, TABLE_TYPE)" +
                                  "  SELECT a.TABLE_NAME, a.TABLE_TYPE from INFORMATION_SCHEMA.TABLES A WHERE A.TABLE_NAME NOT IN( " +
                                  "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS  WHERE CONSTRAINT_TYPE  IN('UNIQUE', 'PRIMARY KEY', 'FOREIGN KEY') " +
                                  ") and a.TABLE_SCHEMA = 'dbo' and a.TABLE_NAME not in ( select TABLE_NAME from INFORMATION_SCHEMA.COLUMNS " +
                                  "where COLUMNPROPERTY(object_id(TABLE_SCHEMA + '.' + TABLE_NAME), COLUMN_NAME, 'IsIdentity') = 1 " +
                                  " ) and A.TABLE_TYPE = 'BASE TABLE'  order by a.TABLE_NAME, a.TABLE_TYPE  ");
                db.SubmitChanges();
                //insertar tabla CONFIGURACIONDBIDEN
                db.ExecuteCommand("TRUNCATE  TABLE CONFIGURACIONDBIDEN  INSERT INTO CONFIGURACIONDBIDEN (TABLE_NAME, COLUMN_NAME)  " +
                                  " SELECT a.TABLE_NAME, COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS a  " +
                                  " where TABLE_SCHEMA = 'dbo' and COLUMNPROPERTY(object_id(TABLE_NAME), COLUMN_NAME, 'IsIdentity') = 1  and  a.TABLE_NAME not in ( " +
                                  "  SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS  where CONSTRAINT_TYPE = 'PRIMARY KEY') AND A.TABLE_NAME IN( " +
                                  "  SELECT a.TABLE_NAME from INFORMATION_SCHEMA.TABLES a  WHERE A.TABLE_TYPE = 'BASE TABLE') " +
                                  "order by TABLE_NAME ");
                db.SubmitChanges();
            }
            catch (Exception e)
            {
                throw e;
                log.Error("error en el proceso configurando tabla: " + e);
            }
        }
        public int CONFIGURACIONDB()
        {
            try
            {

                var config = (from e in db.CONFIGURACIONDB
                              orderby e.TABLE_NAME
                              select e).DefaultIfEmpty().ToArray();
                if (!config.Contains(null))
                {
                    foreach (var tabla in config.ToList())
                    {
                        db.CommandTimeout = 0;
                        db.ExecuteCommand("ALTER TABLE " + tabla.TABLE_NAME + " ADD IDNUM_REG INT PRIMARY KEY IDENTITY(1, 1) NOT NULL");
                        db.SubmitChanges();
                    }
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception x)
            {

                throw x;
                log.Error("error en el proceso configurando creando campo en CONFIGURACIONDB: " + x);
            }
  
        }
        
        public int CONFIGURACIONDBIDEN()
        {
            try
            {
                var configIDEN = (from e in db.CONFIGURACIONDBIDEN
                                  orderby e.TABLE_NAME
                                  select e).DefaultIfEmpty().ToArray();
                if (!configIDEN.Contains(null))
                {
                    foreach (var tabla in configIDEN.ToList())
                    {
                        db.CommandTimeout = 0;
                        db.ExecuteCommand("ALTER TABLE " + tabla.TABLE_NAME + " ADD   PRIMARY KEY(" + tabla.COLUMN_NAME + ")");
                        db.SubmitChanges();
                    }
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception x)
            {

                throw x;
                log.Error("error en el proceso configurando creando campo en CONFIGURACIONDBIDEN: " + x);
            }
        }
    }
       








}