﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Configuration;
using System.IO;

namespace MBS_API.Models.LogManager
{
    public class Log
    {
        string path;
        
        public Log()
        {
           
            try
            {
                JObject config = (JObject)JsonConvert.DeserializeObject(File.ReadAllText(ConfigurationSettings.AppSettings["JSON_URL"].ToString()));
                var logfillename = string.Format("{0}{1}.txt", config["log_params"]["log_path"], DateTime.Now.ToString("yyyyMMdd"));
                this.path = logfillename;
            }
            catch
            {
                //creacion de carpeta automatica sino configuran el json
              string rutalertna = "C:\\MBS\\ErrorLog\\";
              var  logfillename = string.Format("{0}{1}.txt", rutalertna, DateTime.Now.ToString("yyyyMMdd"));
             this.path = logfillename;

            }



        }
       
        public string Path
        {
            get { return path; }
        }
       
        public void Info(string text)
        {
            this.Append(text, "INFO");
        }
       
        public void Error(string text)
        {
            this.Append(text, "ERROR");
        }
       
        private void Append(string text, string type)
        {

            if (!Directory.Exists(this.path))
            {
                Directory.CreateDirectory(this.path.Remove(this.path.LastIndexOf('\\')));
            }
            using (var fs = new FileStream(this.path, FileMode.Append, FileAccess.Write))
            {
                using (var writer = new StreamWriter(fs))
                {

                    writer.WriteLine(string.Format("{0} [{1}] {2}", DateTime.Now.ToString(), type, text));
                    writer.Close();
                    writer.Dispose();
                }
               
            }
        }
    }
}



