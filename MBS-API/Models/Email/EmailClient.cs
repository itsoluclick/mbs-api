﻿using MBS_API.Models.LogManager;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;

namespace MBS_API.Models.Email
{
    public class EmailClient
    {
        private SmtpClient client;
        private MailAddress mailfrom;
        EmailParameter cl = new EmailParameter();
        Log log = new Log();
        private string clienteapi;
        public EmailClient()
        {
            this.client = new SmtpClient(cl.host, cl.port);
            this.mailfrom = new MailAddress(cl.mailfrom);
            clienteapi = cl.Nombrecliente;

        }


        public void SendEmail(List<string> mailto, string subject, string body, List<string> attachmentPathList = null)
        {
            string htmlBody = "<div id=mailFormat><p style=text-align:center;font-family:Arial,Helvetica,sans-serif><h2 " +
       "style=text-align:center;font-family:Arial,Helvetica,sans-serif>MBS-API-Cliente: " + clienteapi + " Loader - REPORTE DE ERROR</h2><p" +
       " style=text-align:center;font-family:Arial,Helvetica,sans-serif>Error de proceso: </br> Si ha recibido este " +
       "mensaje significa que el proceso de sincronización de insumos de MBS-API ha fallado por el siguiente motivo:<h5 style=text-align:center;" +
       " font-family:Arial,Helvetica,sans-serif><span style=font-family:Arial;color:red;>bodytemplate</span></h5><p" +
       " style=text-align:center;font-family:Arial,Helvetica,sans-serif>Adjunto a este correo encontrará un archivo log " +
       "con información al respecto. Disculpe los inconvenientes.</div>";

            var mail = new MailMessage();

            mail.IsBodyHtml = true;
            mail.Subject = subject;
            mail.SubjectEncoding = System.Text.Encoding.UTF8;
            htmlBody = htmlBody.Replace("bodytemplate", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + " " + body);
            mail.Body = htmlBody;
            try
            {

                if (attachmentPathList != null)
                {
                    foreach (var attach in attachmentPathList)
                    {
                        mail.Attachments.Add(new Attachment(attach));
                    }

                }
                mail.From = this.mailfrom;

                foreach (var address in mailto)
                {
                    mail.Bcc.Add(new MailAddress(address));
                }
                 client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential("testsender@itsoluclick.com ", "%yjhf,]N?[KK");
                client.EnableSsl = true;
                client.Send(mail);

            }
            catch (Exception e)
            {
                mail.Dispose();
                log.Error("error en el proceso de envio de email: " + e);
                throw;
            }



        }

        public void SendEmail(string mailto, string subject, string body, List<string> attachmentPathList = null, string x = null)
        {
            SendEmail(new List<string> { mailto }, subject, body, attachmentPathList);
        }


        public void SendEmail(string mailto, string subject, string body, string attachmentPath)
        {
            SendEmail(new List<string> { mailto }, subject, body, new List<string> { attachmentPath });
        }

        public void SendEmail(List<string> mailto, string subject, string body, string attachmentPath)
        {
            SendEmail(mailto, subject, body, new List<string> { attachmentPath });
        }




    }
}


