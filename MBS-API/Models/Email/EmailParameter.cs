﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.IO;

namespace MBS_API.Models.Email
{
    public class EmailParameter
    {

        private List<string> listCliente = new List<string>();
        JObject config = (JObject)JsonConvert.DeserializeObject(File.ReadAllText(ConfigurationSettings.AppSettings["JSON_URL"].ToString()));
        public string Clienteapi { get; set; }
        public string host { get; set; }
        public int port { get; set; }
        public string mailfrom { get; set; }
        public string Nombrecliente { get; set; }
        public EmailParameter()
        {
            var mailconfig = config["mail_client_params"];
            host = mailconfig["host"].ToString();
            port = mailconfig["port"].ToObject<int>();
            mailfrom = mailconfig["source_mail"].ToString();
            Nombrecliente = mailconfig["Cliente"].ToString();


        }
        public List<string> listacliente()
        {
            var mailconfig = config["mail_client_params"];
            var clientelis = mailconfig["destination_mail_list"].ToObject<List<string>>();
            return clientelis;
        }



    }
}