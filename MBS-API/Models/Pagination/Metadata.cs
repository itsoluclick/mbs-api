﻿using System;

namespace MBS_API.Models.Pagination
{
    public class Metadata
    {

        public int TotalCout { get; set; }
        public int PageSize { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPage { get; set; }
        public bool HasPreviospage { get; set; }
        public bool HasNextPage { get; set; }       
        public string MaxpageSize { get; set; }
        public string Note { get; set; }

    }
}