﻿using MBS_API.Models.LogManager;
using MBS_API.Models.Pagination.Interface;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MBS_API.Models.Pagination
{
    public class Responses<T>:IResponses
    { 

        private readonly Log log = new Log();
        private T Estatu { get; set; }
        private T Data { get; set; }
  
        public object RespGet(IEnumerable<object> list, string CanpoResp, string Search, object mapper, Metadata metadata)
        {
            try
            {
                if (list.Contains(null))
                {
                    var response = new
                    {
                        informacion =  "There is no " + CanpoResp.ToUpper()+ " with this information:( " + Search + " )not found to Search"
                    };
                   return response;
                }
                else
                {
                    HttpContext.Current.Response.Headers.Add("X - Pagination", JsonConvert.SerializeObject(metadata));                                     
                    var response = new
                    {
                        Meta = new object[]
                        {
                           metadata
                        },
                        Data = mapper
                       
                        
                    };
                   return response;
                }
            }
            catch (Exception e)
            {
                log.Error("error en el proceso Post: " + e);
                return e;
            }
        }
        public object RespUdate(object mapper)
        {
            var response = new
            {
                Data = mapper,
                Estatu = "Dato Atualizado con existo"
            };
            return response;

        }


    }
}