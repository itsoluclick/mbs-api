﻿using MBS_API.Models.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MBS_API
{
    public class PageList<T> : List<T>
    {
        public int CurrentPage { get; set; }
        public int TotalPage { get; set; }
        public int PageSize { get; set; }
        public int TotalCout { get; set; }
        public bool HasPreviospage => CurrentPage > 1;
        public bool HasNextPage => CurrentPage < TotalPage;
        public int? NextPageNumber => HasNextPage ? CurrentPage + 1 : (int?)null;
        public int? PriviousPageNunber => HasPreviospage ? CurrentPage - 1 : (int?)null;

        



        /// <param name="items"></param>
        /// <param name="count"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        public PageList(List<T> items, int count, int pageNumber, int pageSize)
        {
            //proceso de paginacion

            TotalCout = count;
            PageSize = pageSize;
            CurrentPage = pageNumber;
            TotalPage = (int)Math.Ceiling(TotalCout / (double)pageSize);
            AddRange(items);


        }

        /// <param name="souce"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public static PageList<T> Create(IEnumerable<T> souce, int pageNumber, int pageSize)
        {
            //Resultado de proceso de paginacion
            var count = souce.Count();
            var items = souce.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

            return new PageList<T>(items, count, pageNumber, pageSize);




        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Metadata metadata()
        {
            //Encabezado de paginacion
            var _metadata = new Metadata
            {

                TotalCout = TotalCout,
                TotalPage = TotalPage,
                PageSize = PageSize,
                CurrentPage = CurrentPage,
                HasNextPage = HasNextPage,
                HasPreviospage = HasPreviospage,
                MaxpageSize = " Maximun Size the page 35 ".ToUpper(),          
                Note = "If you do not configure the pageNumber & pageSize fields, MBS_API configures it by default, with a maximum pageSize of 20 records".ToUpper()
                       
            };
            return _metadata;
        }



    }

}







