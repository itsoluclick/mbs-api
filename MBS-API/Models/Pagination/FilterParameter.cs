﻿using MBS_API.Models.Pagination.Interface;
using System;

namespace MBS_API.Models.Pagination
{
    public class FilterParameter 
    {
        private const int MaxpageSize = 35;
        public int pageNumber { get; set; } = 1;
        private int _pageSize = 20;
        public int PageSize
        {
            get => _pageSize;

            set => _pageSize = (value > MaxpageSize) ? MaxpageSize : value;


        }
        public string Search { get; set; }
        private DateTime? date { get; set; }

        public DateTime? Date()
        {

            if (DateTime.TryParse(Search, out DateTime e))
            {
                return e;
            }
            else
            {

                return null;
            }
        }
    }

}