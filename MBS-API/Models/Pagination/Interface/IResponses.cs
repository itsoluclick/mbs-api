﻿using System.Collections.Generic;

namespace MBS_API.Models.Pagination.Interface
{
    public interface IResponses
    {
        object RespGet(IEnumerable<object> list, string CanpoResp, string Search, object mapper, Metadata metadata);
        object RespUdate(object mapper);
    }
}