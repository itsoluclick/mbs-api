﻿using AutoMapper;
using Infraestructura.DTOs;
using MBS_API.AutoMapper;
using MBS_API.DB;
using MBS_API.Models.LogManager;
using MBS_API.Models.Pagination;
using MBS_API.Models.Pagination.Interface;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MBS_API.Controllers
{
    public class MarcaController : ApiController
    {
        private MBSDataClassesDataContext db;

        // EmailClient nr = new EmailClient();
         private readonly IMapper mapper;
         private readonly Log log; 
         private readonly IResponses responses;
        
        public MarcaController()
        {   
            db = new MBSDataClassesDataContext();
             mapper = AutoMapperConfig1._Imapper;
            responses = new Responses<object>();           
            log = new Log();
        }
        // GET: api/Marca/5
        [HttpGet]
        public async Task<HttpResponseMessage> Get([FromUri] FilterParameter filterParameter)
        {
            try
            {
                var marca = (from e in db.MARCA
                             where (SqlMethods.Like(e.DESCR_MARCA.Trim(), '%' + filterParameter.Search + '%') ||
                                   SqlMethods.Like(e.CODIGO_FAB.ToString(), '%' + filterParameter.Search + '%') ||
                                   SqlMethods.Like(e.CODIGO_MARCA.ToString(), '%' + filterParameter.Search + '%') ||
                                    SqlMethods.Like(e.ESTADO.Trim(), '%' + filterParameter.Search + '%'))
                             orderby e.ESTADO
                             select e).DefaultIfEmpty();

                marca.GroupBy(e => e.ESTADO).FirstOrDefault();
                var marcapage = await Task.FromResult(PageList<MARCA>.Create(marca, filterParameter.pageNumber, filterParameter.PageSize));
                var map = mapper.Map<IEnumerable<MARCADTOs>>(marcapage);
                var Responses = responses.RespGet(marcapage, "Marca", filterParameter.Search, map, marcapage.metadata()); 
                return Request.CreateResponse(HttpStatusCode.OK, Responses);
               

            }
            catch (Exception e)
            {

                log.Error("error en el proceso GET: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);

            }
        }

        // POST: api/Marca
        [Authorize]
        [HttpPost]
        public async Task<  IHttpActionResult> Post([FromBody] MARCADTOs value)
        {

            try
            {
                if (value == null)
                {
                    var Information = new
                    {
                        informacion = "configuration error the entity cannot be downloaded please complete the information "
                    };

                    return Ok(Information.ToString());

                }
                else
                {
                   var map = mapper.Map<MARCA>(value);
                   db.MARCA.InsertOnSubmit(map);
                    db.SubmitChanges();
                    return Ok(" Marca Insertada");
                }
            }

            catch (Exception e)
            {

                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();



            }




        }
        [Authorize]
        [HttpPut]
        // PUT: api/Marca/5
        public async Task<HttpResponseMessage> Put([FromUri]int codigo, [FromBody] MARCADTOs Values)
        {
            try
            {
                var Entidad = db.MARCA.FirstOrDefault(e => e.CODIGO_MARCA == codigo);
                if (Entidad == null || Values == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                                "field empty please complete the code or code no exist");
                }
                else
                {
                    // Entidad.NUM_REG = Values.NUM_REG;
                    Entidad.CODIGO_FAB = Values.CODIGO_FAB;
                    Entidad.ESTADO = Values.ESTADO;
                    Entidad.DESCR_MARCA = Values.DESCR_MARCA;
                    db.SubmitChanges();
                    var map = mapper.Map<MARCADTOs>(Entidad);
                    var response =responses.RespUdate(map) ;
                    return await Task.FromResult(Request.CreateResponse(HttpStatusCode.OK, response));


                }


            }
            catch (Exception e)
            {
                log.Error("error en el proceso PUT: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e);

            }

        }

        // DELETE: api/Marca/5
        [Authorize]
        [HttpDelete]     

        public IHttpActionResult Delete([FromUri] int Codigo)
        {
            try
            {
                var paageMarca = db.MARCA.FirstOrDefault(e => e.CODIGO_MARCA == Codigo);
                if (paageMarca == null)
                {
                    return Ok("please complete the code or NUM_CPTO NO exist:(" + Codigo + ")");
                }
                else
                {
                    db.MARCA.DeleteOnSubmit(paageMarca);
                    db.SubmitChanges();
                    return Ok(" MARCA  Eliminada");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }

        }
    }

    }
