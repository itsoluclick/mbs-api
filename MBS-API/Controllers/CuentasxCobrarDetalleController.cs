﻿using AutoMapper;
using Infraestructura.DTOs;
using MBS_API.AutoMapper;
using MBS_API.DB;
using MBS_API.Models.LogManager;
using MBS_API.Models.Pagination;
using MBS_API.Models.Pagination.Interface;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MBS_API.Controllers
{
    public class CuentasxCobrarDetalleController : ApiController
    {
        private MBSDataClassesDataContext db;

        // EmailClient nr = new EmailClient();
        private readonly IMapper mapper; 
        private readonly Log log;
        private readonly IResponses responses;       

        public CuentasxCobrarDetalleController()
        {
            db = new MBSDataClassesDataContext();
            mapper = AutoMapperConfig.Mapper;
            log = new Log();
            responses = new Responses<object>();
        }
        [HttpGet]
        // GET: api/  CuentasXCobarDetalla{}
        public async Task<HttpResponseMessage> GET([FromUri] FilterParameter filterParameter)
        {
            try
            {
                var CXC = (from e in db.TB_REC_DETALLE
                           where (SqlMethods.Like(e.IDNUM_REG.ToString(), '%' + filterParameter.Search + '%') ||
                                  SqlMethods.Like(e.CCLIE.ToString(), '%' + filterParameter.Search + '%') ||
                                  SqlMethods.Like(e.REC_MANUAL.Trim(), '%' + filterParameter.Search + '%') ||
                                  e.FECHA_REC == filterParameter.Date() ||
                                  SqlMethods.Like(e.REC_AUTO.ToString(), '%' + filterParameter.Search + '%') ||
                                  SqlMethods.Like(e.FACTURA.Trim(), '%' + filterParameter.Search + '%') ||
                                  SqlMethods.Like(e.FECHA_REC.ToString(), '%' + filterParameter.Search + '%') ||
                                  SqlMethods.Like(e.COD_COBRADOR.ToString(), '%' + filterParameter.Search + '%') ||
                                  SqlMethods.Like(e.ALMACEN.ToString(), '%' + filterParameter.Search + '%') ||
                                  SqlMethods.Like(e.TIPO_PAGO.ToString(), '%' + filterParameter.Search + '%') ||
                                  SqlMethods.Like(e.TIPO_MOV1.ToString(), '%' + filterParameter.Search + '%') ||
                                  SqlMethods.Like(e.TIPO_MOV2.ToString(), '%' + filterParameter.Search + '%'
                                 ))
                           orderby (e.CCLIE)
                           select e).DefaultIfEmpty();

                CXC.GroupBy(e => e.FACTURA).FirstOrDefault();
                var CXCpage = await Task.FromResult(PageList<TB_REC_DETALLE>.Create(CXC, filterParameter.pageNumber, filterParameter.PageSize));
                var map = mapper.Map<IEnumerable<TB_REC_DETALLEDTOs>>(CXCpage);
                var Responses = responses.RespGet(CXCpage, "CXC", filterParameter.Search, map, CXCpage.metadata()); 
                return Request.CreateResponse(HttpStatusCode.OK, Responses);
                
            }

            catch (Exception e)
            {
                log.Error("error en el proceso Get: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);

            }
        }
        [HttpPost]
        [Authorize]
        // POST: api/  CuentasXCobar detalle
        public IHttpActionResult Post([FromBody] TB_REC_DETALLEDTOs value)
        {
            try
            {
                if (value == null)
                {
                    var Information = new
                    {
                        informacion = "configuration error the entity cannot be downloaded please complete the information "
                    };

                    return Ok(Information.ToString());

                }
                else
                {
                    var map = mapper.Map<TB_REC_DETALLE>(value);
                    db.TB_REC_DETALLE.InsertOnSubmit(map);
                    db.SubmitChanges();
                    return Ok("CxC detalle Insertado");

                }
            }

            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }
        }
    }
}


