﻿using AutoMapper;
using Infraestructura.DTOs;
using MBS_API.AutoMapper;
using MBS_API.DB;
using MBS_API.Models.LogManager;
using MBS_API.Models.Pagination;
using MBS_API.Models.Pagination.Interface;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MBS_API.Controllers
{
    public class CajaChicaController : ApiController
    {
        private MBSDataClassesDataContext db;

        // EmailClient nr = new EmailClient();
        private readonly IMapper mapper; 
        private readonly Log log;
        private readonly IResponses responses; 
       
        public CajaChicaController()
        {
            db = new MBSDataClassesDataContext();
            mapper = AutoMapperConfig.Mapper;
            log = new Log();
            responses = new Responses<object>();
        }
        [HttpGet]
        // GET: api/ CajaChica
        public async Task<HttpResponseMessage> GET([FromUri] FilterParameter filterParameter)
        {
            try
            {
                var caja_Chicas = (from e in db.Caja_chica
                                     where (SqlMethods.Like(e.codigo.ToString(), '%' + filterParameter.Search + '%') ||
                                            SqlMethods.Like(e.RNC, '%' + filterParameter.Search + '%') ||
                                            SqlMethods.Like(e.descripcion.Trim(), '%' + filterParameter.Search + '%') ||
                                            SqlMethods.Like(e.CAJA_BANCO.Trim(), '%' + filterParameter.Search + '%') ||                                          
                                           SqlMethods.Like(e.cprov.ToString(), '%' + filterParameter.Search + '%'))
                                     orderby (e.cprov)
                                     select e).DefaultIfEmpty();

                caja_Chicas.GroupBy(e => e.codigo).FirstOrDefault();
                var caja_Chicaspage = await Task.FromResult(PageList<Caja_chica>.Create(caja_Chicas, filterParameter.pageNumber, filterParameter.PageSize));
                var map = mapper.Map<IEnumerable<Caja_chicaDTOs>>(caja_Chicaspage);
                var Responses = responses.RespGet(caja_Chicaspage, "", filterParameter.Search, map, caja_Chicaspage.metadata()); 
                return Request.CreateResponse(HttpStatusCode.OK, Responses);
            }

            catch (Exception e)
            {
                log.Error("error en el proceso Get: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);

            }
        }
        [HttpPost]
        [Authorize]
        // POST: api/ CajaChica
        public IHttpActionResult Post([FromBody] Caja_chicaDTOs value)
        {
            try
            {
                if (value == null)
                {
                    var Information = new
                    {
                        informacion = "configuration error the entity cannot be downloaded please complete the information "
                    };

                    return Ok(Information.ToString());

                }
                else
                {
                    var caja = mapper.Map<Caja_chica>(value);

                        db.Caja_chica.InsertOnSubmit(caja);
                        db.SubmitChanges();

                    


                    return Ok("Caja Chica Insertado");
                }

            }



            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }
        }

        [HttpPut]
        [Authorize]
        // PUT: api/ CajaChica
        public async Task< HttpResponseMessage> Put([FromUri]int codigo, [FromBody] Caja_chicaDTOs Values)
        {

            try
            {
                var Entidad = db.Caja_chica.FirstOrDefault(e => e.codigo == codigo);
                if (Entidad == null || Values == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                                "field empty please complete the code or code no exist");
                }
                else
                {
                    Entidad.descripcion = Values.descripcion;
                    Entidad.cprov = Values.cprov;
                    Entidad.RNC = Values.RNC;
                    Entidad.CAJA_BANCO = Values.CAJA_BANCO;
                
                    db.SubmitChanges();

                    var map = mapper.Map<Caja_chicaDTOs>(Entidad);
                    var response = responses.RespUdate(map);                
                   
                    return await Task.FromResult(   Request.CreateResponse(HttpStatusCode.OK, response));


                }
            }
            catch (Exception e)
            {
                log.Error("error en el proceso PUT: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e);

            }


        }

        // DELETE: api/ CajaChica/5

        [HttpDelete]
        [Authorize]
        public IHttpActionResult Delete([FromUri]int Codigo)
        {
            try
            {
                var cajachica = db.Caja_chica.FirstOrDefault(e => e.codigo == Codigo);
                if (cajachica == null)
                {
                    return Ok("please complete the code or SubGrupo NO exist:(" + Codigo + ")");
                }
                else
                {
                    db.Caja_chica.DeleteOnSubmit(cajachica);
                    db.SubmitChanges();
                    return Ok("caja chica Eliminado");
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso DELETE: " + e);
                // var f =new EmailClient.SendEmail(" Reporte de error", e.Message, log.Path);

                return NotFound();
            }
        }
    }


}

