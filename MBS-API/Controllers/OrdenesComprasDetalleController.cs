﻿using AutoMapper;
using Infraestructura.DTOs;
using MBS_API.AutoMapper;
using MBS_API.DB;
using MBS_API.Models.LogManager;
using MBS_API.Models.Pagination;
using MBS_API.Models.Pagination.Interface;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MBS_API.Controllers
{
    public class OrdenesComprasDetalleController:ApiController
    {
        private MBSDataClassesDataContext db;

        // EmailClient nr = new EmailClient();
        private readonly IMapper mapper;  
        private readonly Log log; 
        public readonly IResponses responses; 
       
        public OrdenesComprasDetalleController()
        {
            db = new MBSDataClassesDataContext();
            mapper = AutoMapperConfig.Mapper;
            log = new Log();
            responses = new Responses<object>();
        }
        [HttpGet]
        //get Oden compra detalle
        public async Task<HttpResponseMessage> GET([FromUri] string Search, [FromUri] string TipoDocumento)
        {
            try
            {
                if (string.IsNullOrEmpty(Search) || string.IsNullOrEmpty(TipoDocumento))
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "fields empty please complete the fields :(TipoDocumento,Search)to make the transaction");

                }
                else
                {
                    var OrdenesCompra = (from Ordenes in db.Sp_imprimeDocCOMP01(Search, TipoDocumento)
                                      select Ordenes).DefaultIfEmpty().ToList();
                    OrdenesCompra.GroupBy(e => e.almacen).FirstOrDefault();
                    if (OrdenesCompra.Contains(null))
                    {
                        string informacion = "There is no Documentos with this information:( " + Search + ", " + TipoDocumento + " )not found to Search";
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound, informacion);
                    }
                    else
                    {

                        var Response = new 
                        {
                         Data = OrdenesCompra.ToList()
                        
                        };
                        return   Request.CreateResponse(HttpStatusCode.OK, Response);
                    }
                }

            }
            catch (Exception e)
            {
                log.Error("error en el proceso GET: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }

        }
        //detalle de Oden compra detalle
        [HttpGet]
        public async Task<HttpResponseMessage> GETDetalle([FromUri] FilterParameter filterParameter)
        {

            try
            {

                var OrdenesCompra = (from Ordenes in db.COM0Y1TEMP1
                          where SqlMethods.Like(Ordenes.NUM_REG.ToString().Trim(), '%' + filterParameter.Search + '%') ||
                                SqlMethods.Like(Ordenes.CVE_DOC.Trim(), '%' + filterParameter.Search + '%') ||
                                SqlMethods.Like(Ordenes.TIP_DOC.Trim(), '%' + filterParameter.Search + '%') ||                               
                                SqlMethods.Like(Ordenes.TIP_REG.Trim(), '%' + filterParameter.Search + '%') ||                               
                                SqlMethods.Like(Ordenes.CVE_CLPV.Trim(), '%' + filterParameter.Search + '%') ||                               
                                SqlMethods.Like(Ordenes.USUARIO.ToString(), '%' + filterParameter.Search + '%') ||
                                SqlMethods.Like(Ordenes.TIP_DOC.Trim(), '%' + filterParameter.Search + '%') ||                               
                                SqlMethods.Like(Ordenes.TIP_DOC.Trim(), '%' + filterParameter.Search + '%') ||
                                SqlMethods.Like(Ordenes.CVE_CLPV.Trim(), '%' + filterParameter.Search + '%') ||                               
                                SqlMethods.Like(Ordenes.TIP_DOC.Trim(), '%' + filterParameter.Search + '%') ||
                                Ordenes.FECHA_DOC == filterParameter.Date()                            
                         select Ordenes).DefaultIfEmpty();


                OrdenesCompra.GroupBy(e => e.TIP_DOC).FirstOrDefault();
                var OrdenesComprapage = await Task.FromResult(PageList<COM0Y1TEMP1>.Create(OrdenesCompra, filterParameter.pageNumber, filterParameter.PageSize));
                var map = mapper.Map<IEnumerable<COM0Y1TEMP1DTOs>>(OrdenesComprapage);
                var Responses = responses.RespGet(OrdenesComprapage, "Documentos", filterParameter.Search, map, OrdenesComprapage.metadata());
                return Request.CreateResponse(HttpStatusCode.OK, Responses);                
            }

            catch (Exception e)
            {
                log.Error("error en el proceso GET: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }



        }
        // inset de Oden compra detalle mediante lista la cual debe estar
        // configurada asi[{},{}]

        [Authorize]
        [HttpPost]
        public IHttpActionResult Post([FromBody] List<COM0Y1TEMP1DTOs> value)
        {
            try
            {

                if (value == null)
                {
                    var Information = new
                    {
                        informacion = "configuration error the entity cannot be downloaded please complete the information "
                    };

                    return Ok(Information.ToString());

                }
                else
                {
                    var map = mapper.Map<IEnumerable<COM0Y1TEMP1>>(value);
                    foreach (COM0Y1TEMP1 x in map)
                    {
                        db.COM0Y1TEMP1.InsertOnSubmit(x);
                        db.SubmitChanges();
                    }
                    return Ok("Oden compra detalle Insertado");
                }
        }



            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
    }
}
    }

}
