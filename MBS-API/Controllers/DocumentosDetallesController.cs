﻿using AutoMapper;
using Infraestructura.DTOs;
using MBS_API.AutoMapper;
using MBS_API.DB;
using MBS_API.Models.LogManager;
using MBS_API.Models.Pagination;
using MBS_API.Models.Pagination.Interface;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MBS_API.Controllers
{
    public class DocumentosDetallesController : ApiController
    {
        private MBSDataClassesDataContext db;

        // EmailClient nr = new EmailClient();
        private readonly IMapper mapper; 
        private readonly Log log;
        private readonly IResponses responses;       
        public DocumentosDetallesController()
        { 
           db = new MBSDataClassesDataContext();
            mapper = AutoMapperConfig.Mapper;
            log = new Log();
            responses = new Responses<object>();
        }
        [HttpGet]
        //get documentacion detalle
        public async Task<HttpResponseMessage> GET([FromUri] string Search, [FromUri] string TipoDocumento)
        {
            try
            {
                if (string.IsNullOrEmpty(Search) || string.IsNullOrEmpty(TipoDocumento))
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "fields empty please complete the fields :(TipoDocumento,Search)to make the transaction");

                }
                else
                {
                    var Documentos = (from Documento in db.Sp_imprimeDocfact01(Search, TipoDocumento)
                                      select Documento).DefaultIfEmpty().ToList();
                    Documentos.FirstOrDefault();
                   
                    if (Documentos.Contains(null))
                    {
                        string informacion = "There is no Documentos with this information:( " + Search + ", " + TipoDocumento + " )not found to Search";
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound, informacion);
                    }
                    else
                    {
                        var Response = new
                        {
                            Data = Documentos.ToList()
                        };
                        return   Request.CreateResponse(HttpStatusCode.OK, Response);
                    }
                }

            }
            catch (Exception e)
            {
                log.Error("error en el proceso GET: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
           
        }
        //detalle de factura
        [HttpGet]
        public async Task<HttpResponseMessage> GETDetalle([FromUri] FilterParameter filterParameter)
        {

            try
            {
                var Documentos = (from Documento in db.FA0TY1
                          where SqlMethods.Like(Documento.NUM_REG.ToString().Trim(), '%' + filterParameter.Search + '%') ||
                                SqlMethods.Like(Documento.CVE_DOC.Trim(), '%' + filterParameter.Search + '%') ||
                                SqlMethods.Like(Documento.TIP_DOC.Trim(), '%' + filterParameter.Search + '%') ||
                                SqlMethods.Like(Documento.CVE_CLPV.Trim(), '%' + filterParameter.Search + '%') ||
                                SqlMethods.Like(Documento.TIP_DOC.Trim(), '%' + filterParameter.Search + '%') ||
                                Documento.FECHA_DOC == filterParameter.Date()
                         select Documento).DefaultIfEmpty();
                Documentos.GroupBy(e => e.CVE_DOC).FirstOrDefault();
                var Documentopage = await Task.FromResult(PageList<FA0TY1>.Create(Documentos, filterParameter.pageNumber, filterParameter.PageSize));
                var map = mapper.Map<IEnumerable<FA0TY1DTOs>>(Documentopage);
                var Responses = responses.RespGet(Documentopage, "Documentos", filterParameter.Search, map, Documentopage.metadata());
                return Request.CreateResponse(HttpStatusCode.OK, Responses);
                
            
            }

            catch (Exception e)
            {
                log.Error("error en el proceso GET: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }

        }
        // inset de documentos mediante lista la cual debe estar
        // configurada asi[{},{}]
        [Authorize]
        [HttpPost]
        public IHttpActionResult Post([FromBody] List<FA0TY1DTOs> value)
        {
            try
            {
                if (value == null)
                {
                    var Information = new
                    {
                        informacion = "configuration error the entity cannot be downloaded please complete the information "
                    };
                    return Ok(Information.ToString());

                }
                else
                {
                    var map = mapper.Map<IEnumerable<FA0TY1>>(value);
                    foreach (FA0TY1 x in map)
                    {
                        db.FA0TY1.InsertOnSubmit(x);
                        db.SubmitChanges();
                    }
                    return Ok("Factura Insertado".ToUpper());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }
        }
}

    }









