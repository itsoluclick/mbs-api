﻿using AutoMapper;
using Infraestructura.DTOs;
using MBS_API.AutoMapper;
using MBS_API.DB;
using MBS_API.Models.LogManager;
using MBS_API.Models.Pagination;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace MBS_API.Controllers
{
    public class ConfiguracionBDController : ApiController
    {
     

        private MBSDataClassesDataContext db;

        // EmailClient nr = new EmailClient();
     
        private Log log;
        // post: api/ Configuracion

        public ConfiguracionBDController()
        {
            db = new MBSDataClassesDataContext();
          
            log = new Log();
        }
        [HttpPost]
        // post: api/ Configuracion
        public IHttpActionResult Post([FromUri] string IDConfig)
        {

            try
            {
                if (string.IsNullOrEmpty(IDConfig)  )
                {
                    var Information = new
                    {
                        informacion = "configuration error the entity cannot be downloaded please complete the information "
                    };

                    return Ok(Information.ToString());

                }
                else if(IDConfig =="IT@2012".ToUpper())
                {
                    //creacion de tabla  CONFIGURACIONDB
                    db.ExecuteCommand("IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CONFIGURACIONDB]') AND type in (N'U')) " +
                                      "DROP TABLE[dbo].[CONFIGURACIONDB] " +
                                      "CREATE TABLE CONFIGURACIONDB (  ID INT PRIMARY KEY IDENTITY(1, 1) not null,  TABLE_NAME VARCHAR(100),  TABLE_TYPE VARCHAR(100))");
                    db.SubmitChanges();
                    //creacion de tabla  CONFIGURACIONDBIDEN
                    db.ExecuteCommand("IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CONFIGURACIONDBIDEN]') AND type in (N'U')) " +
                                       "DROP TABLE[dbo].[CONFIGURACIONDBIDEN] CREATE TABLE CONFIGURACIONDBIDEN (  ID INT PRIMARY KEY IDENTITY(1, 1) not null,  TABLE_NAME VARCHAR(100), " +
                                       " COLUMN_NAME VARCHAR(100)");
                    db.SubmitChanges();

                    //insertar tabla CONFIGURACIONDB
                    db.ExecuteCommand("TRUNCATE TABLE CONFIGURACIONDB INSERT INTO CONFIGURACIONDB(TABLE_NAME, TABLE_TYPE)"+ 
                                      "SELECT a.TABLE_NAME, a.TABLE_TYPE from INFORMATION_SCHEMA.TABLES A WHERE A.TABLE_NAME NOT IN( " +
                                      "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS  WHERE CONSTRAINT_TYPE NOT IN('UNIQUE', 'PRIMARY KEY', 'FOREIGN KEY') AND TABLE_NAME " +
                                      "NOT IN(SELECT A.TABLE_NAME FROM INFORMATION_SCHEMA.COLUMNS a where TABLE_SCHEMA = 'dbo'" +
                                      "and COLUMNPROPERTY(object_id(TABLE_NAME), COLUMN_NAME, 'IsIdentity') = 1) ) AND A.TABLE_TYPE = 'BASE TABLE'");
                    db.SubmitChanges();
                    //insertar tabla CONFIGURACIONDBIDEN
                    db.ExecuteCommand("truncate  table CONFIGURACIONDBIDEN" +
                                      "INSERT INTO CONFIGURACIONDBIDEN(TABLE_NAME, COLUMN_NAME) " +
                                      "SELECT a.TABLE_NAME, COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS a " +
                                      "where TABLE_SCHEMA = 'dbo' and COLUMNPROPERTY(object_id(TABLE_NAME), COLUMN_NAME, 'IsIdentity') = 1  and  a.TABLE_NAME not in ( " +
                                      "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS  " +
                                       "where CONSTRAINT_TYPE = 'PRIMARY KEY') AND A.TABLE_NAME IN(SELECT a.TABLE_NAME from INFORMATION_SCHEMA.TABLES a  WHERE A.TABLE_TYPE = 'BASE TABLE')" +
                                       "order by TABLE_NAME");
                    db.SubmitChanges();
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }





        }

    }



}
}
