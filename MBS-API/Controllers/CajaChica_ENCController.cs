﻿using AutoMapper;
using Infraestructura.DTOs;
using MBS_API.AutoMapper;
using MBS_API.DB;
using MBS_API.Models.LogManager;
using MBS_API.Models.Pagination;
using MBS_API.Models.Pagination.Interface;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MBS_API.Controllers
{
    public class CajaChica_ENCController : ApiController
    {
        private MBSDataClassesDataContext db;

        // EmailClient nr = new EmailClient();
        private readonly IMapper mapper;  
        private readonly Log log;
        private readonly IResponses responses; 
       
       
        public CajaChica_ENCController()
        {
            db = new MBSDataClassesDataContext();
            mapper = AutoMapperConfig.Mapper;
            log = new Log();
            responses = new Responses<object>();
        }
        [HttpGet]
        // GET: api/  cajaChica_enc
        public async Task<HttpResponseMessage> GET([FromUri] FilterParameter filterParameter)
        {
            try
            {

                var caja_Chica_encs = (from e in db.cajaChica_enc
                                     where (SqlMethods.Like(e.CODIGO_CAJA.ToString(), '%' + filterParameter.Search + '%') ||
                                            SqlMethods.Like(e.cve_doc.ToString(), '%' + filterParameter.Search + '%') ||
                                            SqlMethods.Like(e.CVE_CLPV.Trim(), '%' + filterParameter.Search + '%') ||
                                            e.fecha == filterParameter.Date() ||
                                            SqlMethods.Like(e.IMP_TOT.ToString(), '%' + filterParameter.Search + '%') ||
                                            SqlMethods.Like(e.tip_doc.Trim(), '%' + filterParameter.Search + '%') ||
                                            SqlMethods.Like(e.bien.ToString(), '%' + filterParameter.Search + '%') ||
                                            SqlMethods.Like(e.servicio.ToString(), '%' + filterParameter.Search + '%'
                                           ))
                                     orderby (e.CODIGO_CAJA)
                                     select e).DefaultIfEmpty();

                caja_Chica_encs.GroupBy(e => e.CODIGO_CAJA).FirstOrDefault();
                var caja_Chicaspage = await Task.FromResult(PageList<cajaChica_enc>.Create(caja_Chica_encs, filterParameter.pageNumber, filterParameter.PageSize));
                var map = mapper.Map<IEnumerable<cajaChica_encDTOs>>(caja_Chicaspage);
                var Responses = responses.RespGet(caja_Chicaspage, "caja_Chica_encs", filterParameter.Search, map, caja_Chicaspage.metadata()); return Request.CreateResponse(HttpStatusCode.OK, Responses);
              
            }

            catch (Exception e)
            {
                log.Error("error en el proceso Get: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);

            }
        }
        [HttpPost]
        [Authorize]
        // POST: api/  cajaChica_enc[{},{},{}]
        public IHttpActionResult Post([FromBody] List<cajaChica_encDTOs> value)
        {
            try
            {
                if (value == null)
                {
                    var Information = new
                    {
                        informacion = "configuration error the entity cannot be downloaded please complete the information "
                    };

                    return Ok(Information.ToString());

                }
                else
                {

                    var map = mapper.Map<IEnumerable<  cajaChica_enc>>(value);

                    foreach (var e in map)
                    {

                       var NUM_REG = db.cajaChica_enc.Max(x => x.IDNUM_REG + 1);
                       e.IDNUM_REG = NUM_REG;
                        db.cajaChica_enc.InsertOnSubmit(e);
                        db.SubmitChanges();
                        
                    }
                   
                    return Ok("Caja Chica_enc Insertado");
                  
                }

            }



            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }
        }
    }

}

