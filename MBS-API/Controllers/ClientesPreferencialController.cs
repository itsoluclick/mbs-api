﻿using AutoMapper;
using Infraestructura.DTOs;
using MBS_API.Areas.Repositorio;
using MBS_API.AutoMapper;
using MBS_API.DB;
using MBS_API.Models.LogManager;
using MBS_API.Models.Pagination;
using MBS_API.Models.Pagination.Interface;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MBS_API.Controllers
{
    public class ClientesPreferencialController : ApiController
    {
        private MBSDataClassesDataContext db;

        // EmailClient nr = new EmailClient();
        private readonly IMapper mapper;
        private readonly Log log;
        private readonly IResponses responses;
        private readonly Repositorio repositorio;
       
        public ClientesPreferencialController()
        {
            db = new MBSDataClassesDataContext();
            mapper = AutoMapperConfig.Mapper;
            log = new Log();
            responses = new Responses<object>();
            repositorio = new Repositorio();
        }
        [HttpGet]
        //get /ClientesPreferencial/
        public async Task<HttpResponseMessage> GET([FromUri] FilterParameter filterParameter)
        {

            try
            {
            var CLIE_PREF = (from e in db.CLIE_PREF
                             where (SqlMethods.Like(e.cve_pref.ToString(), '%' + filterParameter.Search + '%') ||
                                    SqlMethods.Like(e.Tarj_pref.ToString(), '%' + filterParameter.Search + '%') ||
                                    SqlMethods.Like(e.nombre.Trim(), '%' + filterParameter.Search + '%') ||
                                    SqlMethods.Like(e.empresa.Trim(), '%' + filterParameter.Search + '%') ||
                                    SqlMethods.Like(e.direccion.Trim(), '%' + filterParameter.Search + '%') ||
                                    SqlMethods.Like(e.correoe.ToString(), '%' + filterParameter.Search + '%') ||
                                    SqlMethods.Like(e.telefonos.ToString(), '%' + filterParameter.Search + '%') ||
                                    SqlMethods.Like(e.estado.ToString(), '%' + filterParameter.Search + '%') ||
                                              e.fecha_nacimiento == filterParameter.Date() ||
                                    SqlMethods.Like(e.cedula.ToString(), '%' + filterParameter.Search + '%'))
                             orderby (e.nombre)
                             select e).DefaultIfEmpty();
                CLIE_PREF.GroupBy(e => e.cve_pref).FirstOrDefault();
                var paageCLIE_PREF = await Task.FromResult(PageList<CLIE_PREF>.Create(CLIE_PREF, filterParameter.pageNumber, filterParameter.PageSize));
                var map = mapper.Map<IEnumerable<CLIE_PREFDTOs>>(paageCLIE_PREF);
                var Responses = responses.RespGet(paageCLIE_PREF, "Cliente Preferencial", filterParameter.Search, map, paageCLIE_PREF.metadata());
                return Request.CreateResponse(HttpStatusCode.OK, Responses);
                        
            }
            catch (Exception e)
            {

              log.Error("error en el proceso GET: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }
        // Post: api/ClientesPreferencial
        [HttpPost]
        [Authorize]
        public IHttpActionResult Post([FromBody] CLIE_PREFDTOs Value)
        {

            try
            {
                if (Value == null)
                {
                    var Information = new
                    {
                        informacion = "configuration error the entity cannot be downloaded please complete the information "
                    };

                    return Ok(Information.ToString());

                }
                else
                {
                    var map = mapper.Map<CLIE_PREF>(Value);
                    db.CLIE_PREF.InsertOnSubmit(map);
                    db.SubmitChanges();
                    return Ok(" Cliente Preferencial Insertado");
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }

        }
        [HttpPut]
        [Authorize]
        // PUT: api/ClientesPreferencial
        public async Task< HttpResponseMessage> Put([FromUri] int codigo, [FromBody] CLIE_PREFDTOs values)
        {

            try
            {
                var Entidad = db.CLIE_PREF.FirstOrDefault(e => e.cve_pref == codigo);
                if (Entidad == null || values == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                                "field empty please complete the code or code no exist");
                }
                else
                {


                    var rp = repositorio.ClientePrefUpdate(Entidad, values);
                    var map = mapper.Map<CLIE_PREFDTOs>(Entidad);
                    var response = responses.RespUdate(map) ;
                    return await Task.FromResult(   Request.CreateResponse(HttpStatusCode.OK, response));

                }


            }
            catch (Exception e)
            {
                log.Error("error en el proceso PUT: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e);

            }


        }

        [HttpDelete]
        [Authorize]
        // Delete: api/ClientesPreferencial
        public IHttpActionResult Delete([FromUri] int Codigo)
        {
            try
            {
                var Caja = db.CLIE_PREF.FirstOrDefault(e => e.cve_pref == Codigo);
                if (Caja == null)
                {
                    return Ok("please complete the code or NUM_CPTO NO exist:(" + Codigo + ")");
                }
                else
                {
                    db.CLIE_PREF.DeleteOnSubmit(Caja);
                    db.SubmitChanges();
                    return Ok(" Cliente Preferencial Eliminado");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }

        }
    }
}

















