﻿using AutoMapper;
using Infraestructura.DTOs;
using MBS_API.AutoMapper;
using MBS_API.DB;
using MBS_API.Models.LogManager;
using MBS_API.Models.Pagination;
using MBS_API.Models.Pagination.Interface;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MBS_API.Controllers
{
    public class SubGruposController: ApiController
    {

        private MBSDataClassesDataContext db;
        // EmailClient nr = new EmailClient();
        private readonly IMapper mapper;  
        private readonly Log log; 
        public readonly IResponses responses;        
        public SubGruposController()
        {
            db = new MBSDataClassesDataContext();
            mapper = AutoMapperConfig.Mapper;
            log = new Log();
            responses = new Responses<object>();
        }

        [HttpGet]        
        // GET: api/SUBGROPO
        public async Task<HttpResponseMessage> GET([FromUri] FilterParameter filterParameter)
        {
            try
            {
                var Grupos = (from Grupo in db.INVGRUD
                              where (SqlMethods.Like(Grupo.INVGRU_MASTER.ToString(), '%' + filterParameter.Search + '%') ||
                              SqlMethods.Like(Grupo.INVGRU_CODIGO.ToString(), '%' + filterParameter.Search + '%') ||
                              SqlMethods.Like(Grupo.id_grupo_padre.ToString(), '%' + filterParameter.Search + '%') ||
                              SqlMethods.Like(Grupo.INVGRU_DESCRIPCION.ToString(), '%' + filterParameter.Search + '%'))
                              orderby Grupo.INVGRU_CODIGO
                              select Grupo).DefaultIfEmpty();

                Grupos.GroupBy(e => e.INVGRU_CODIGO).FirstOrDefault();
                var SubGrupospage = await Task.FromResult(PageList<INVGRUD>.Create(Grupos, filterParameter.pageNumber, filterParameter.PageSize));
                var map = mapper.Map<IEnumerable<INVGRUDDTOs>>(SubGrupospage);
                var Responses = responses.RespGet(SubGrupospage, "", filterParameter.Search, map, SubGrupospage.metadata()); return Request.CreateResponse(HttpStatusCode.OK, Responses);

            }

            catch (Exception e)
            {
                log.Error("error en el proceso Get: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);

            }
        }
        [HttpPost]
       [Authorize]
        // POST: api/SubGROUP
        public IHttpActionResult Post([FromBody] INVGRUDDTOs value)
        {
            try
            {
                if (value == null)
                {
                    var Information = new
                    {
                        informacion = "configuration error the entity cannot be downloaded please complete the information "
                    };

                    return Ok(Information.ToString());

                }
                else
                {
                    var map = mapper.Map< INVGRUD > (value);
                    db.INVGRUD.InsertOnSubmit(map);
                    db.SubmitChanges();
                    return Ok("SubGrupo Insertado");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }
        }



        [HttpPut]
        [Authorize]
        // PUT: api/api/SubGROUP
        public async Task< HttpResponseMessage> Put([FromUri]int codigo, [FromBody] INVGRUDDTOs Values)
        {

            try
            {
                var Entidad = db.INVGRUD.FirstOrDefault(e => e.INVGRU_CODIGO == codigo);
                if (Entidad == null || Values == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                                "field empty please complete the code or code no exist");
                }
                else
                {
                                                
                    Entidad.INVGRU_MASTER = Values.INVGRU_MASTER;
                    Entidad.id_grupo_padre = Values.Id_grupo_padre;
                    Entidad.INVGRU_DESCRIPCION = Values.INVGRU_DESCRIPCION;
                    db.SubmitChanges();
                    var map = mapper.Map<INVGRUDDTOs>(Entidad);
                    var response = responses.RespUdate(map) ;
                    return await Task.FromResult(   Request.CreateResponse(HttpStatusCode.OK, response));


                }


            }
            catch (Exception e)
            {
                log.Error("error en el proceso PUT: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e);

            }


        }
        [HttpDelete]
        [Authorize]
        // DELETE: api/api/SubGROUP
        public IHttpActionResult Delete([FromUri]int Codigo)
        {
            try
            {
                var SubGrupos = db.INVGRUD.FirstOrDefault(e => e.INVGRU_CODIGO == Codigo);
                if (SubGrupos == null)
                {
                    return Ok("please complete the code or SubGrupo NO exist:(" + Codigo + ")");
                }
                else
                {
                    db.INVGRUD.DeleteOnSubmit(SubGrupos);
                    db.SubmitChanges();
                    return Ok("SubGrupo Eliminado");
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso DELETE: " + e);
                // var f =new EmailClient.SendEmail(" Reporte de error", e.Message, log.Path);

                return NotFound();
            }
        }
    }



}
    
