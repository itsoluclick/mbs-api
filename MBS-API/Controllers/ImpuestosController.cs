﻿using AutoMapper;
using Infraestructura.DTOs;
using MBS_API.Areas.Repositorio;
using MBS_API.AutoMapper;
using MBS_API.DB;
using MBS_API.Models.LogManager;
using MBS_API.Models.Pagination;
using MBS_API.Models.Pagination.Interface;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MBS_API.Controllers
{
    public class ImpuestosController : ApiController
    {
        private MBSDataClassesDataContext db;

        // EmailClient nr = new EmailClient();
        private readonly IMapper mapper;  
        private readonly Log log;
        private readonly IResponses responses;
        private readonly Repositorio repositorio;
        
       
        public ImpuestosController()
        {
            db = new MBSDataClassesDataContext();
            mapper = AutoMapperConfig.Mapper;
            log = new Log();
            responses = new Responses<object>();
            repositorio = new Repositorio();
        }
        // Get: api/api/impuesto
        [HttpGet]
        public async Task<HttpResponseMessage> GET([FromUri] FilterParameter filterParameter)
        {

            try
            {
                var Impuesto = (from e in db.IMPU01
                                where (SqlMethods.Like(e.NUM_REG.ToString().Trim(), '%' + filterParameter.Search + '%') ||
                                       SqlMethods.Like(e.CVEESQIMPU.ToString().Trim(), '%' + filterParameter.Search + '%') ||
                                       SqlMethods.Like(e.DESCRIPESQ.Trim(), '%' + filterParameter.Search + '%') ||
                                       SqlMethods.Like(e.IMPUESTO1.ToString().Trim(), '%' + filterParameter.Search + '%'))
                                orderby (e.NUM_REG)
                                select e).DefaultIfEmpty();
                Impuesto.GroupBy(e => e.NUM_REG).FirstOrDefault();
                var impugecaja = await Task.FromResult(PageList<IMPU01>.Create(Impuesto, filterParameter.pageNumber, filterParameter.PageSize));
                var map = mapper.Map<IEnumerable<IMPU01DTOs>>(impugecaja);
                var Responses = responses.RespGet(impugecaja, "Concepto", filterParameter.Search, map, impugecaja.metadata()); return Request.CreateResponse(HttpStatusCode.OK, Responses);
               
            }
            catch (Exception e)
            {

                log.Error("error en el proceso GET: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }
        // Post: api/api/impuesto
        [HttpPost]
        [Authorize]
        public IHttpActionResult Post([FromBody] IMPU01DTOs Value)
        {

            try
            {
                if (Value == null)
                {
                    var Information = new
                    {
                        informacion = "configuration error the entity cannot be downloaded please complete the information "
                    };

                    return Ok(Information.ToString());

                }
                else
                {
                    var map = mapper.Map<IMPU01>(Value);
                    db.IMPU01.InsertOnSubmit(map);
                    db.SubmitChanges();
                    return Ok(" impuesto Insertado");
                }
            }

            catch (Exception e)
            {

                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();



            }
}


    
       [HttpPut]
       [Authorize]
        //PUT: api/api/impuesto
        public async Task< HttpResponseMessage> Put(string codigo, [FromBody] IMPU01DTOs values)
        {

            try
            {
                var Entidad = db.IMPU01.FirstOrDefault(e => e.CVEESQIMPU.Trim() == codigo);
                if (Entidad == null || values == null)
                {


                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                            "field empty please complete the code");
                }
                else
                {

                    var rp = repositorio.ImpuestoUpdate(Entidad, values);
                    var map = mapper.Map<IMPU01DTOs>(Entidad);
                    var response = responses.RespUdate(rp) ;
                    return await Task.FromResult(   Request.CreateResponse(HttpStatusCode.OK, response));


                }


            }
            catch (Exception e)
            {
                log.Error("error en el proceso PUT: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e);

            }


        }

        [HttpDelete]
        [Authorize]
        // Delete: api/api/Impuesto
        public IHttpActionResult Delete([FromUri]string Codigo)
        {
            try
            {
                if (string.IsNullOrEmpty(Codigo))
                {
                    var Information = new
                    {
                        informacion = "empty field,please complete the code"
                    };

                    return Ok(Information);

                }
                else
                {
                    var Impuesto = db.IMPU01.FirstOrDefault(e => e.CVEESQIMPU.Trim() == Codigo);
                                                            
                    if (Impuesto == null)
                    {
                        return Ok("please complete the code or Impuesto NO exist:(" + Codigo + ")");
                    }
                    else
                    {

                        db.IMPU01.DeleteOnSubmit(Impuesto);
                        db.SubmitChanges();
                        return Ok("Impuesto Eliminado");
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }

        }

    }
}