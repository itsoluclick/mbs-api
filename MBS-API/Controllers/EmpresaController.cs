﻿using AutoMapper;
using Infraestructura.DTOs;
using MBS_API.Areas.Repositorio;
using MBS_API.AutoMapper;
using MBS_API.DB;
using MBS_API.Models.LogManager;
using MBS_API.Models.Pagination;
using MBS_API.Models.Pagination.Interface;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MBS_API.Controllers
{
    public class EmpresaController : ApiController
    {
        private MBSDataClassesDataContext db;

        // EmailClient nr = new EmailClient();
        private readonly IMapper mapper; 
        private readonly Log log;
        private readonly IResponses responses;
        private readonly Repositorio repositorio;
        public EmpresaController()
        {
            db = new MBSDataClassesDataContext();
             mapper = AutoMapperConfig1._Imapper;
            responses = new Responses<object>();
            log = new Log();
            repositorio = new Repositorio();
        }
        [HttpGet]
        // GET: api/Empresa/5
        public async Task<HttpResponseMessage> Get([FromUri] FilterParameter filterParameter)
        {
            try
            {
                var Empresa = (from e in db.EMPRESAS
                               where (SqlMethods.Like(e.COD_EMPRESA.ToString().Trim(), '%' + filterParameter.Search + '%') ||
                   SqlMethods.Like(e.NOMBRE_EMPRESA.Trim(), '%' + filterParameter.Search + '%') ||
                   SqlMethods.Like(e.CDKEY.Trim(), '%' + filterParameter.Search + '%') ||
                   SqlMethods.Like(e.ESTATUS.Trim(), '%' + filterParameter.Search + '%') ||
                   SqlMethods.Like(e.DIRECCION.Trim(), '%' + filterParameter.Search + '%') ||
                   SqlMethods.Like(e.RNC.Trim(), '%' + filterParameter.Search + '%') ||
                   SqlMethods.Like(e.TELEFONO.Trim(), '%' + filterParameter.Search + '%') ||
                   SqlMethods.Like(e.CORREO.Trim(), '%' + filterParameter.Search + '%') ||
                   SqlMethods.Like(e.CLIENTELIBRE1.Trim(), '%' + filterParameter.Search + '%') ||
                   SqlMethods.Like(e.CLIENTELIBRE2.Trim(), '%' + filterParameter.Search + '%') ||
                   SqlMethods.Like(e.CLIENTELIBRE3.Trim(), '%' + filterParameter.Search + '%') ||
                   SqlMethods.Like(e.CLIENTELIBRE4.Trim(), '%' + filterParameter.Search + '%') ||
                   SqlMethods.Like(e.INVENTARIOLIBRE1.Trim(), '%' + filterParameter.Search + '%') ||
                   SqlMethods.Like(e.INVENTARIOLIBRE2.Trim(), '%' + filterParameter.Search + '%') ||
                   SqlMethods.Like(e.INVENTARIOLIBRE3.Trim(), '%' + filterParameter.Search + '%') ||
                   SqlMethods.Like(e.PROVEEDORLIBRE1.Trim(), '%' + filterParameter.Search + '%') ||
                   SqlMethods.Like(e.PROVEEDORLIBRE2.Trim(), '%' + filterParameter.Search + '%') ||
                   SqlMethods.Like(e.PROVEEDORLIBRE3.Trim(), '%' + filterParameter.Search + '%') ||
                   SqlMethods.Like(e.PROVEEDORLIBRE4.Trim(), '%' + filterParameter.Search + '%') ||
                   SqlMethods.Like(e.PROVEEDORLIBRE5.Trim(), '%' + filterParameter.Search + '%') ||
                   SqlMethods.Like(e.TIPO_NEGOCIO.ToString(), '%' + filterParameter.Search + '%') ||
                   SqlMethods.Like(e.nombre_comercial.Trim(), '%' + filterParameter.Search + '%'))
                               orderby (e.IDNUM_REG)
                               select e).DefaultIfEmpty();
                Empresa.GroupBy(e => e.COD_EMPRESA).FirstOrDefault();
                var empresapage = await Task.FromResult(PageList<EMPRESAS>.Create(Empresa, filterParameter.pageNumber, filterParameter.PageSize));
                var map = mapper.Map<IEnumerable<EMPRESASDTOs>>(empresapage);
                var Responses = responses.RespGet(empresapage, "EMPRESA", filterParameter.Search, map, empresapage.metadata());
                return Request.CreateResponse(HttpStatusCode.OK, Responses);
               

            }
            catch (Exception e)
            {
                log.Error("error en el proceso Get: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);

            }

        }

        // POST: api/Empresa
        [Authorize]
        [HttpPost]
       
        public async Task< IHttpActionResult >   Post([FromBody] EMPRESASDTOs Value)
        {
            try
            {
                if (Value == null)
                {
                    var Information = new
                    {
                        informacion = "configuration error the entity cannot be downloaded please complete the information "
                    };
                    return Ok(Information.ToString());

                }
                else
                {
                    var map = mapper.Map<EMPRESAS>(Value);
                    db.EMPRESAS.InsertOnSubmit(map);
                    db.SubmitChanges();
                    return Ok(" EMPRESAS Insertada".ToUpper ());
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }

        }
        [Authorize]
        [HttpPut]
        // PUT: api/Empresa/5
        public async Task<HttpResponseMessage> Put([FromUri] string Codigo, [FromBody] EMPRESASDTOs Values)
        {
            try
            {
                var Entidad = db.EMPRESAS.FirstOrDefault(e => e.COD_EMPRESA == Codigo);
                if (Entidad == null || Values == null)
                {       
                           return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                            "field empty please complete the code");
                }
                else
                {
                    var rp = repositorio.EMPRESASUpdate(Entidad, Values);
                    var map = mapper.Map<IEnumerable<EMPRESASDTOs>>(rp);
                    var response = responses.RespUdate(map);
                    return await Task.FromResult(Request.CreateResponse(HttpStatusCode.OK, response));


                }

            }
            catch (Exception e)
            {
                log.Error("error en el proceso PUT: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e);
            }
        }

        // DELETE: api/Empresa/5
        [Authorize]
        [HttpDelete]
        public IHttpActionResult Delete([FromUri] string Codigo)
        {
            try
            {
                if (string.IsNullOrEmpty(Codigo))
                {
                    var Information = new
                    {
                        informacion = "empty field,please complete the code"
                    };
                    return Ok(Information);

                }
                else
                {
                    var empresa = db.EMPRESAS.FirstOrDefault(e => e.COD_EMPRESA.Trim() == Codigo);
                    if (empresa == null)
                    {
                        return Ok("please complete the code or Empresa NO exist:(" + Codigo + ")");
                    }
                    else
                    {

                        db.EMPRESAS.DeleteOnSubmit(empresa);
                        db.SubmitChanges();
                        return Ok("Empresa Eliminado");
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso DELETE: " + e);
                // var f =new EmailClient.SendEmail(" Reporte de error", e.Message, log.Path);

                return NotFound();
            }
        }
    }
}
