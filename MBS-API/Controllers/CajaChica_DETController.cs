﻿using AutoMapper;
using Infraestructura.DTOs;
using MBS_API.AutoMapper;
using MBS_API.DB;
using MBS_API.Models.LogManager;
using MBS_API.Models.Pagination;
using MBS_API.Models.Pagination.Interface;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MBS_API.Controllers
{
    public class CajaChica_DETController : ApiController
    {


        private MBSDataClassesDataContext db;

        // EmailClient nr = new EmailClient();
        private readonly IMapper mapper; 
        private readonly Log log;
        private readonly IResponses responses; 
       
        public CajaChica_DETController()
        {
            db = new MBSDataClassesDataContext();
            mapper = AutoMapperConfig.Mapper;
            log = new Log();
            responses = new Responses<object>();
        }
        [HttpGet]
        // GET: api/  CajaChica_DET
        public async Task<HttpResponseMessage> GET([FromUri] FilterParameter filterParameter)
        {
            try
            {

                var caja_ChicaDET = (from e in db.CajaChica_DET
                                     where (SqlMethods.Like(e.CODIGO_CAJA.ToString(), '%' + filterParameter.Search + '%') ||
                                            SqlMethods.Like(e.cve_doc.ToString(), '%' + filterParameter.Search + '%') ||
                                            SqlMethods.Like(e.RNC_CED.Trim(), '%' + filterParameter.Search + '%') ||
                                            e.fecha_Trans == filterParameter.Date() ||
                                            SqlMethods.Like(e.IMPORTE.ToString(), '%' + filterParameter.Search + '%') ||
                                            SqlMethods.Like(e.IMPUESTO.ToString(), '%' + filterParameter.Search + '%') ||
                                            SqlMethods.Like(e.NCF.Trim(), '%' + filterParameter.Search + '%') ||
                                            SqlMethods.Like(e.TIPO_GASTO.Trim(), '%' + filterParameter.Search + '%') ||
                                            SqlMethods.Like(e.CUENTA_CONTA.ToString(), '%' + filterParameter.Search + '%') ||
                                           SqlMethods.Like(e.Gasto_menor_Cve_Doc.ToString(), '%' + filterParameter.Search + '%'))
                                     orderby (e.CODIGO_CAJA)
                                     select e).DefaultIfEmpty();

                caja_ChicaDET.GroupBy(e => e.CODIGO_CAJA).FirstOrDefault();
                var caja_Chicaspage = await Task.FromResult(PageList<CajaChica_DET>.Create(caja_ChicaDET, filterParameter.pageNumber, filterParameter.PageSize));
                var map = mapper.Map<IEnumerable<CajaChica_DETDTOs>>(caja_Chicaspage);
                var Responses = responses.RespGet(caja_Chicaspage, "Caja chica DET", filterParameter.Search, map, caja_Chicaspage.metadata()); 
                return Request.CreateResponse(HttpStatusCode.OK, Responses);
               
            }

            catch (Exception e)
            {
                log.Error("error en el proceso Get: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);

            }
        }
        [HttpPost]
        [Authorize]
        // POST: api/  CajaChica_DET[{},{},{}]
        public IHttpActionResult Post([FromBody] List<CajaChica_DETDTOs> value)
        {
            try
            {
                if (value == null)
                {
                    var Information = new
                    {
                        informacion = "configuration error the entity cannot be downloaded please complete the information "
                    };

                    return Ok(Information.ToString());

                }
                else
                {

                    var map = mapper.Map<IEnumerable<CajaChica_DET>>(value);              

                    foreach (var e in map)
                    {
                        var NUM_REG = db.CajaChica_DET.Max(x => x.secuencia + 1);
                        e.secuencia = NUM_REG;
                        db.CajaChica_DET.InsertOnSubmit(e);
                        db.SubmitChanges();
                    }
                    return Ok("Caja Chica_DETC Insertado");
                }

            }



            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }
        }
    }
    
}
