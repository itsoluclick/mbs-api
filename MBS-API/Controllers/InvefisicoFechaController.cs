﻿using AutoMapper;
using Infraestructura.DTOs;
using MBS_API.AutoMapper;
using MBS_API.DB;
using MBS_API.Models.LogManager;
using MBS_API.Models.Pagination;
using MBS_API.Models.Pagination.Interface;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MBS_API.Controllers
{
    public class InventariofisicoFechaController : ApiController
    {
        private MBSDataClassesDataContext db;
        private readonly IMapper mapper; 
        private readonly Log log;
        public readonly IResponses responses; 
        
        // EmailClient nr = new EmailClient();       
        public InventariofisicoFechaController()
        {
            db = new MBSDataClassesDataContext();
             mapper = AutoMapperConfig1._Imapper; 
            responses = new Responses<object>();
            log = new Log();
        }
        [HttpGet]
        // GET: api/InvefisicoFecha/5
        public async Task<HttpResponseMessage> GET([FromUri] FilterParameter filterParameter)
        {

            try
            {
                var Invefisico = (from e in db.invefisico_fecha
                                  where (SqlMethods.Like(e.clv_art.Trim(), '%' + filterParameter.Search + '%') ||
                                        SqlMethods.Like(e.exist.ToString(), '%' + filterParameter.Search + '%') ||
                                        SqlMethods.Like(e.IDNUM_REG.ToString(), '%' + filterParameter.Search + '%') ||
                                        SqlMethods.Like(e.almacen.ToString(), '%' + filterParameter.Search + '%') ||
                                         e.fecha == filterParameter.Date())
                                  orderby e.IDNUM_REG
                                  select e);
                Invefisico.GroupBy(e => e.clv_art).FirstOrDefault();
                var PageaInvefisico = await Task.FromResult (PageList<invefisico_fecha>.Create(Invefisico, filterParameter.pageNumber, filterParameter.PageSize));
                var map = mapper.Map<IEnumerable<InventariofisicoFechaDTOs>>(PageaInvefisico);
                var Responses = responses.RespGet(PageaInvefisico, "Inventario fisico", filterParameter.Search, map, PageaInvefisico.metadata()); return Request.CreateResponse(HttpStatusCode.OK, Responses);
                

            }
            catch (Exception e)
            {

                log.Error("error en el proceso GET: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }



        }


    }



   
       
    
}
