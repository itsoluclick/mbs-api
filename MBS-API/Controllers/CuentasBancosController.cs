﻿using AutoMapper;
using Infraestructura.DTOs;
using MBS_API.Areas.Repositorio;
using MBS_API.AutoMapper;
using MBS_API.DB;
using MBS_API.Models.LogManager;
using MBS_API.Models.Pagination;
using MBS_API.Models.Pagination.Interface;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MBS_API.Controllers
{
    public class CuentasBancosController : ApiController
    {
        private MBSDataClassesDataContext db;

        // EmailClient nr = new EmailClient();
        private readonly IMapper mapper; 
        private readonly Log log;
        private readonly IResponses responses;
        private readonly Repositorio repositorio;
        public CuentasBancosController()
        {
            db = new MBSDataClassesDataContext();
            mapper = AutoMapperConfig.Mapper;
            log = new Log();
            responses = new Responses<object>();
            repositorio = new Repositorio();
        }
        [HttpGet]
        // GET: api/ CuentasBancos
        public async Task<HttpResponseMessage> GET([FromUri] FilterParameter filterParameter)
        {
            try
            {
             var cajabanco = (from e in db.CTAS01
                                 where (SqlMethods.Like(e.CVE_CTA.ToString(), '%' + filterParameter.Search + '%') ||
                                        SqlMethods.Like(e.STATUS, '%' + filterParameter.Search + '%') ||
                                        SqlMethods.Like(e.NUM_CTA.Trim(), '%' + filterParameter.Search + '%') ||
                                        SqlMethods.Like(e.BANCO.ToString(), '%' + filterParameter.Search + '%') ||                                       
                                        SqlMethods.Like(e.TIPO_CTA.ToString(), '%' + filterParameter.Search + '%') ||
                                        SqlMethods.Like(e.SUCURSAL.Trim(), '%' + filterParameter.Search + '%') ||
                                        SqlMethods.Like(e.CONTACTO.Trim(), '%' + filterParameter.Search + '%') ||
                                        SqlMethods.Like(e.TELEFONO.Trim(), '%' + filterParameter.Search + '%') ||
                                        SqlMethods.Like(e.CTA_CONTAB.Trim(), '%' + filterParameter.Search + '%') ||
                                        SqlMethods.Like(e.NUM_REG.ToString(), '%' + filterParameter.Search + '%') ||
                                        SqlMethods.Like(e.LEYEN_PST.Trim(), '%' + filterParameter.Search + '%'))
                                 orderby (e.NUM_REG)
                                 select e).DefaultIfEmpty();
                cajabanco.GroupBy(e => e.CVE_CTA).FirstOrDefault();
                var cajapage = await Task.FromResult(PageList<CTAS01>.Create(cajabanco, filterParameter.pageNumber, filterParameter.PageSize));
                var map = mapper.Map<IEnumerable<CTAS01DTOs>>(cajapage);
                var Responses = responses.RespGet(cajapage, "Cuenta de Banco", filterParameter.Search, map, cajapage.metadata());
                return Request.CreateResponse(HttpStatusCode.OK, Responses);


        }

            catch (Exception e)
            {
                log.Error("error en el proceso Get: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);

            }
}
        [HttpPost]
        [Authorize]
        // POST: api/ CuentasBancos
        public IHttpActionResult Post([FromBody] CTAS01DTOs value)
        {
            try
            {
                if (value == null)
                {
                    var Information = new
                    {
                        informacion = "configuration error the entity cannot be downloaded please complete the information "
                    };

                    return Ok(Information.ToString());

                }
                else
                {
                    var map = mapper.Map<CTAS01>(value);

                    db.CTAS01.InsertOnSubmit(map);
                    db.SubmitChanges();

                    return Ok("Cuenta de Banco Insertado");
                }

            }



            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }
        }

        [HttpPut]
        [Authorize]
        // PUT: api/CuentasBancos
        public async Task<HttpResponseMessage> Put([FromUri] string codigo,[FromBody] CTAS01DTOs Values)
        {

            try
            {
                var Entidad = db.CTAS01.FirstOrDefault(e => e.CVE_CTA.ToString() == codigo|| e.NUM_CTA ==codigo);
                if (Entidad == null || Values == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                                "field empty please complete the code or code no exist");
                }
                else
                {

                    var rp = repositorio.CuentasBancosUpdate(Entidad, Values);
                    var map = mapper.Map<CTAS01DTOs>(rp);
                    var response = responses.RespUdate(map);
                    return await Task.FromResult(Request.CreateResponse(HttpStatusCode.OK, response));


                }


            }
            catch (Exception e)
            {
                log.Error("error en el proceso PUT: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e);

            }


        }

        // DELETE: api/ CuentasBancos/5

        [HttpDelete]
       [Authorize]
        public IHttpActionResult Delete([FromUri] string Codigo)
        {
            try
            {


                if (string.IsNullOrEmpty(Codigo))
                {
                    var Information = new
                    {
                        informacion = "empty field,please complete the code"
                    };

                    return Ok(Information);

                }



                var banco = db.CTAS01.FirstOrDefault(e => e.CVE_CTA.ToString() == Codigo || e.NUM_CTA == Codigo);
                if (banco == null)
                {
                    return Ok("please complete the code or cuenta banco NO exist:(" + Codigo + ")");
                }
                else
                {

                    db.CTAS01.DeleteOnSubmit(banco);
                    db.SubmitChanges();
                    return Ok("cuenta banco  Eliminado");
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso DELETE: " + e);
                // var f =new EmailClient.SendEmail(" Reporte de error", e.Message, log.Path);

                return NotFound();
            }
        }
    }

}


