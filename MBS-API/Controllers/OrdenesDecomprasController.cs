﻿using AutoMapper;
using Infraestructura.DTOs;
using MBS_API.AutoMapper;
using MBS_API.DB;
using MBS_API.Models.LogManager;
using MBS_API.Models.Pagination;
using MBS_API.Models.Pagination.Interface;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MBS_API.Controllers
{
    public class OrdenesDecomprasController:ApiController
    {

        private MBSDataClassesDataContext db;

        // EmailClient nr = new EmailClient();
        private readonly IMapper mapper; 
        private readonly Log log; 
        public readonly IResponses responses; 
       
        public OrdenesDecomprasController()
        {
            db = new MBSDataClassesDataContext();
            mapper = AutoMapperConfig.Mapper;
            log = new Log();
            responses = new Responses<object>();
        }
        [HttpGet]
        //get :api/OrdenesDecomprasController?{}
        public async Task<HttpResponseMessage> GET([FromUri] FilterParameter filterParameter)
        {
            try
            {
                var OrdenesDecompra = (from Ordenes in db.COMP01
                          where SqlMethods.Like(Ordenes.NUM_REG.ToString().Trim(), '%' + filterParameter.Search + '%') ||
                                SqlMethods.Like(Ordenes.CVE_DOC.Trim(), '%' + filterParameter.Search + '%') ||
                                SqlMethods.Like(Ordenes.TIP_DOC.Trim(), '%' + filterParameter.Search + '%') ||
                                SqlMethods.Like(Ordenes.CVE_REF.Trim(), '%' + filterParameter.Search + '%') ||
                                SqlMethods.Like(Ordenes.TIP_REG.Trim(), '%' + filterParameter.Search + '%') ||
                                SqlMethods.Like(Ordenes.SU_REFER.Trim(), '%' + filterParameter.Search + '%') ||
                                SqlMethods.Like(Ordenes.TIP_REF.Trim(), '%' + filterParameter.Search + '%') ||
                                SqlMethods.Like(Ordenes.CVE_CLPV.Trim(), '%' + filterParameter.Search + '%') ||
                                SqlMethods.Like(Ordenes.TIPODOCUMENTO.Trim(), '%' + filterParameter.Search + '%') ||
                                SqlMethods.Like(Ordenes.USUARIO.ToString(), '%' + filterParameter.Search + '%') ||
                                SqlMethods.Like(Ordenes.TIP_DOC.Trim(), '%' + filterParameter.Search + '%') ||
                                SqlMethods.Like(Ordenes.TIPOPROVEEDOR.Trim(), '%' + filterParameter.Search + '%') ||
                                SqlMethods.Like(Ordenes.almacen_ent.ToString().Trim(), '%' + filterParameter.Search + '%') ||
                                SqlMethods.Like(Ordenes.TIP_DOC.Trim(), '%' + filterParameter.Search + '%') ||
                                SqlMethods.Like(Ordenes.CVE_CLPV.Trim(), '%' + filterParameter.Search + '%') ||
                                SqlMethods.Like(Ordenes.STATUS.Trim(), '%' + filterParameter.Search + '%') ||
                                SqlMethods.Like(Ordenes.TIP_DOC.Trim(), '%' + filterParameter.Search + '%') ||
                                Ordenes.FECHA_DOC == filterParameter.Date()||
                                Ordenes.FECHA_REC == filterParameter.Date()
                         select Ordenes).DefaultIfEmpty();
                OrdenesDecompra.GroupBy(e => e.TIP_DOC).FirstOrDefault();
                var OrdenesDecomprapage = await Task.FromResult(PageList<COMP01>.Create(OrdenesDecompra, filterParameter.pageNumber, filterParameter.PageSize));
                var map = mapper.Map<IEnumerable<COMP01DTOs>>(OrdenesDecomprapage);
                var Responses = responses.RespGet(OrdenesDecomprapage, "Documento", filterParameter.Search, map, OrdenesDecomprapage.metadata());
                return Request.CreateResponse(HttpStatusCode.OK, Responses);
               
            }

            catch (Exception e)
            {
                log.Error("error en el proceso GET: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }

        }

        // insert ordenes de compra
       [Authorize]
       [HttpPost]
        public IHttpActionResult Post([FromBody]  OrdenesCompraDTOs value)
        {
            try
            { 
                if (value == null)
                {
                    var Information = new
                    {
                        informacion = "configuration error the entity cannot be downloaded please complete the information "
                    };
                    return Ok(Information.ToString());

                }
                else
                {

                    db.Sp_Crea_orden_compraretails(value.documento, value.CCLIE, value.FECHA_DOC, value.almacen, value.
                       cajero, value.TIP_DOC, value.NCFMODIFICA, value.SUREFER, value.ALMACEN_ENT, value.FECHA_ENT,
                       value.MONEDA, value.TASA, value.tipodocumento, value.observacion);                    
                    db.SubmitChanges();
                    return Ok("Orden de Compra Insertada");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }
        }
        


    }







}
