﻿using AutoMapper;
using Infraestructura.DTOs;
using MBS_API.AutoMapper;
using MBS_API.DB;
using MBS_API.Models.LogManager;
using MBS_API.Models.Pagination;
using MBS_API.Models.Pagination.Interface;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MBS_API.Controllers
{
    public class DocumentosController : ApiController
    {

        private MBSDataClassesDataContext db;

        // EmailClient nr = new EmailClient();
        private readonly IMapper mapper; 
        private readonly Log log;
        private readonly IResponses responses;        
        public DocumentosController()
        {
            db = new MBSDataClassesDataContext();
            mapper = AutoMapperConfig.Mapper;
            log = new Log();
            responses = new Responses<object>();
        }
        [HttpGet]
        public async Task<HttpResponseMessage> GET([FromUri] FilterParameter filterParameter)
        {

            try
            {
                var Documentos = (from Documento in db.FACT01
                                  where SqlMethods.Like(Documento.NUM_REG.ToString().Trim(), '%' + filterParameter.Search + '%') ||
                                SqlMethods.Like(Documento.CVE_DOC.Trim(), '%' + filterParameter.Search + '%') ||
                                SqlMethods.Like(Documento.TIP_DOC.Trim(), '%' + filterParameter.Search + '%') ||
                                SqlMethods.Like(Documento.CVE_CLPV.Trim(), '%' + filterParameter.Search + '%') ||
                                SqlMethods.Like(Documento.STATUS.Trim(), '%' + filterParameter.Search + '%') ||
                                SqlMethods.Like(Documento.TIP_DOC.Trim(), '%' + filterParameter.Search + '%') ||
                                Documento.FECHA_DOC == filterParameter.Date()
                         select Documento).DefaultIfEmpty();

                Documentos.GroupBy(e => e.CVE_DOC).FirstOrDefault();
                var Documentopage = await Task.FromResult(PageList<FACT01>.Create(Documentos, filterParameter.pageNumber, filterParameter.PageSize));
                var map = mapper.Map<IEnumerable<FACT01DTOs>>(Documentopage);
                var Responses = responses.RespGet(Documentopage, "Documentos", filterParameter.Search, map, Documentopage.metadata()); return Request.CreateResponse(HttpStatusCode.OK, Responses);

            }

            catch (Exception e)
            {
                log.Error("error en el proceso GET: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }



        }

        // insert factura
        [Authorize]
        [HttpPost]
        public IHttpActionResult PostFacturas([FromBody]  DocumentosDTOs value)
        {
            try
            {

                if (value == null)
                {
                    var Information = new
                    {
                        informacion = "configuration error the entity cannot be downloaded please complete the information "
                    };

                    return Ok(Information.ToString());

                }
                else
                {

                    db.Sp_Crea_factpuntoventa4(value.NumFact, value.IdCliente, value.fecha, value.vend
                        , value.NumAlma, value.efectivo, value.tarjeta1, value.tarjeta2, value.cheque,
                        value.nota, value.credito, value.devolver, value.UID, "F", value.Observacion,
                        value.Condicion, value.DocClie, 0, "F", value.DocClie, value.IdPc, 0, "", "",
                        value.CodCliPref, value.acobrar, value.aprobacion1, value.aprobacion2, value.aprobacionck, value.RNC, value.aprobacionNota, value.Rbono,
                        value.aprobacionpuntos, value.aprobacionpuntos2, value.puntos, value.puntos2, value.bono, value.otrospagos,
                        value.OtroDoc, value.transferencia, value.RTransf, value.RetencionItbis, value.aprobRetencionitbis);
                    db.SubmitChanges();
                    return Ok("Factura Insertada");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }
        }
        //insert pedido
        [Authorize]
        [HttpPost]
        public IHttpActionResult PostPedidos([FromUri] string ID_Pedido, [FromBody] PedidoDTOs value)
        {
            try
            {
                if (value == null || string.IsNullOrEmpty(ID_Pedido))
                {
                    var Information = new
                    {
                        informacion = "configuration error the entity cannot be downloaded please complete the information "
                    };

                    return Ok(Information.ToString());

                }
                else
                {
                    db.Sp_Crea_pedido(ID_Pedido, value.Cliente, value.Fecha_Doc, value.cve_vend, value.Almacen, value.Condicion);
                    db.SubmitChanges();


                    return Ok("Pedido Insertado");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }
        }



    }
}








