﻿using AutoMapper;
using Infraestructura.DTOs;
using MBS_API.Areas.Repositorio;
using MBS_API.AutoMapper;
using MBS_API.DB;
using MBS_API.Models.LogManager;
using MBS_API.Models.Pagination;
using MBS_API.Models.Pagination.Interface;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MBS_API.Controllers
{
    //[Authorize] //Necesita token para aceder a este recurso
    public class ProductosController : ApiController
    {
        private MBSDataClassesDataContext db;

        // EmailClient nr = new EmailClient();
        private readonly IMapper mapper; 
        private readonly Log log; 
        public readonly IResponses responses;
        private readonly Repositorio repositorio;

        public ProductosController()
        {
            db = new MBSDataClassesDataContext();
            mapper = AutoMapperConfig.Mapper;
            log = new Log();
            responses = new Responses<object>();
            repositorio = new Repositorio();
        }

        [HttpGet]
        //Get/api/Productos
        
        public async Task<HttpResponseMessage> GET([FromUri] FilterParameter filterParameter)
        {
            try
            {

                var productos = (from producto in db.INVE01
                                 where (SqlMethods.Like(producto.COD_SUPLIDOR.Trim(), '%' + filterParameter.Search + '%') ||
                                        SqlMethods.Like(producto.DESCR.Trim(), '%' + filterParameter.Search + '%') ||
                                        SqlMethods.Like(producto.LIN_PROD.Trim(), '%' + filterParameter.Search + '%') ||
                                        SqlMethods.Like(producto.CLV_ART.Trim(), '%' + filterParameter.Search + '%'))
                                 orderby (producto.NUM_REG)                                 
                                 select producto).DefaultIfEmpty();


                productos.GroupBy(e => e.CLV_ART).FirstOrDefault();
                var Productopage = await Task.FromResult(PageList<INVE01>.Create(productos, filterParameter.pageNumber, filterParameter.PageSize));
                var map = mapper.Map<IEnumerable<INVE01DTOs>>(Productopage);
                var Responses = responses.RespGet(Productopage, "product", filterParameter.Search, map, Productopage.metadata()); 
                return Request.CreateResponse(HttpStatusCode.OK, Responses);
               

            }
            catch (Exception e)
            {
                log.Error("error en el proceso Get: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);

            }

        }
        [HttpPost]
        // POST: api/Productos
        [Authorize]
        public IHttpActionResult Post([FromBody] INVE01DTOs value)
        {
            try
            {
                if (value == null)
                {
                    var Information = new
                    {
                        informacion = "configuration error the entity cannot be downloaded please complete the information "
                    };

                    return Ok(Information.ToString());

                }
                else
                {
                    var map = mapper.Map<INVE01>(value);
                    db.INVE01.InsertOnSubmit(map);
                    db.SubmitChanges();

                    return Ok("Producto Insertado");
                }
            }



            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }

        }

        // PUT: api/Productos
        [HttpPut]
        [Authorize]
        public async Task< HttpResponseMessage> Put([FromUri] string codigo, [FromBody] INVE01DTOs Values)
        {
            try
            {
                var Entidad = db.INVE01.FirstOrDefault(e => e.CLV_ART == codigo);
                if (Entidad == null || Values == null)
                {


                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                            "field empty please complete the code");
                }
                else
                {
                    var rp = repositorio.ProductoUpdate(Entidad, Values);                   
                    var map = mapper.Map<IEnumerable<INVE01DTOs>>(rp);
                    var response = responses.RespUdate(map) ;
                    return await Task.FromResult(   Request.CreateResponse(HttpStatusCode.OK, response));


                }

            }
            catch (Exception e)
            {
                log.Error("error en el proceso PUT: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e);
            }
        }


        // DELETE: api/Productos?codigo=PRISD
        [Authorize]
        [HttpDelete]
        public IHttpActionResult Delete( [FromUri]string Codigo)
        {
            try
            {
                if (string.IsNullOrEmpty(Codigo))
                {
                    var Information = new
                    {
                        informacion = "empty field,please complete the code"
                    };

                    return Ok(Information);

                }
                else
                {
                    var productos = db.INVE01.FirstOrDefault(e => e.CLV_ART.Trim() == Codigo);                                   
                    if (productos == null)
                    {
                        return Ok("please complete the code or producto NO exist:(" + Codigo + ")");
                    }
                    else
                    {

                        db.INVE01.DeleteOnSubmit(productos);
                        db.SubmitChanges();
                        return Ok("Producto Eliminado");
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso DELETE: " + e);
                // var f =new EmailClient.SendEmail(" Reporte de error", e.Message, log.Path);

                return NotFound();
            }
        }
    }
}
