﻿using AutoMapper;
using Infraestructura.DTOs;
using MBS_API.Areas.Repositorio;
using MBS_API.AutoMapper;
using MBS_API.DB;
using MBS_API.Models.LogManager;
using MBS_API.Models.Pagination;
using MBS_API.Models.Pagination.Interface;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MBS_API.Controllers
{
    public class ContactosClientesController:ApiController
    {
        private MBSDataClassesDataContext db;

        // EmailClient nr = new EmailClient();
        private readonly IMapper mapper; 
        private readonly Log log;
        private readonly IResponses responses;
        private readonly Repositorio repositorio;
        public ContactosClientesController()
        {
            db = new MBSDataClassesDataContext();
            mapper = AutoMapperConfig.Mapper;
            log = new Log();
            responses = new Responses<object>();
            repositorio = new Repositorio();
        }
        // Get: api/ ContactosClientesController?{id}
        [HttpGet]
        public async Task<HttpResponseMessage> GET([FromUri] FilterParameter filterParameter)
        {

            try
            {
                var Impuesto = (from e in db.CONTAC01
                                where (SqlMethods.Like(e.NUM_REG.ToString().Trim(), '%' + filterParameter.Search + '%') ||
                                       SqlMethods.Like(e.USRNAMESAE.Trim(), '%' + filterParameter.Search + '%') ||
                                       SqlMethods.Like(e.CCLIE.Trim(), '%' + filterParameter.Search + '%') ||
                                        SqlMethods.Like(e.NOMBRE.Trim(), '%' + filterParameter.Search + '%') ||
                                         SqlMethods.Like(e.DIRECCION.Trim(), '%' + filterParameter.Search + '%') ||
                                          SqlMethods.Like(e.EMAIL.Trim(), '%' + filterParameter.Search + '%') ||
                                          SqlMethods.Like(e.TELEFONO.Trim(), '%' + filterParameter.Search + '%') ||
                                          SqlMethods.Like(e.CEDULA.Trim(), '%' + filterParameter.Search + '%') ||
                                          SqlMethods.Like(e.CLV_TIPO.ToString(), '%' + filterParameter.Search + '%') ||
                                           SqlMethods.Like(e.NO_SOCIO.Trim(), '%' + filterParameter.Search + '%'))
                                      
                                orderby (e.NUM_REG)
                                select e).DefaultIfEmpty();

                Impuesto.GroupBy(e => e.CCLIE).FirstOrDefault();
                var paageCONTAC01 = await Task.FromResult(PageList<CONTAC01>.Create(Impuesto, filterParameter.pageNumber, filterParameter.PageSize));
                var map = mapper.Map<IEnumerable<CONTAC01DTOs>>(paageCONTAC01);
                var Responses = responses.RespGet(paageCONTAC01, "Contracto clientes", filterParameter.Search, map, paageCONTAC01.metadata()); 
                return Request.CreateResponse(HttpStatusCode.OK, Responses);
                
            }
            catch (Exception e)
            {

                log.Error("error en el proceso GET: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }
        // Post: api/ ContactosClientesController
        [HttpPost]
       [Authorize]
        public IHttpActionResult Post([FromBody] CONTAC01DTOs Value)
        {

            try
            {
                if (Value == null)
                {
                    var Information = new
                    {
                        informacion = "configuration error the entity cannot be downloaded please complete the information "
                    };

                    return Ok(Information.ToString());
                }
                else
                {
                    var map = mapper.Map<CONTAC01>(Value);
                    db.CONTAC01.InsertOnSubmit(map);
                    db.SubmitChanges();
                    return Ok(" Contacto Cliente Insertado");
                }
            }

            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();

            }
        }
        [HttpPut]
        [Authorize]
        // PUT: api/ ContactosClientesController?{ID}
        public async Task< HttpResponseMessage> Put([FromUri] int codigo, [FromBody] CONTAC01DTOs Values)
        {

            try
            {
               var Entidad = db.CONTAC01.FirstOrDefault(e => e.NUM_REG == codigo);
                if (Entidad == null || Values == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                                "field empty please complete the code or code no exist");
                }
                else
                {


                    var rp = repositorio.ClienteCont(Entidad, Values);
                    var map = mapper.Map<CONTAC01DTOs>(rp);
                    var response = responses.RespUdate(map);
                    return await Task.FromResult(   Request.CreateResponse(HttpStatusCode.OK, response));


                }


            }
            catch (Exception e)
            {
               log.Error("error en el proceso PUT: " + e);
               return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e);

            }


        }

        [HttpDelete]
        [Authorize]
        // Delete: api/ ContactosClientesController?{ID}
        public IHttpActionResult Delete([FromUri] int Codigo)
        {
            try
            {
                var paageCONTAC01 = db.CONTAC01.FirstOrDefault(e => e.NUM_REG == Codigo);
                if (paageCONTAC01 == null)
                {
                    return Ok("please complete the code or NUM_CPTO NO exist:(" + Codigo + ")");
                }
                else
                {
                    db.CONTAC01.DeleteOnSubmit(paageCONTAC01);
                    db.SubmitChanges();
                    return Ok(" Contacto Cliente  Eliminado");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }

        }


    }
}