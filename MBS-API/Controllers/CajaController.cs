﻿using AutoMapper;
using Infraestructura.DTOs;
using MBS_API.Areas.Repositorio;
using MBS_API.AutoMapper;
using MBS_API.DB;
using MBS_API.Models.LogManager;
using MBS_API.Models.Pagination;
using MBS_API.Models.Pagination.Interface;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MBS_API.Controllers
{
    public class CajaController:ApiController
    {

        private MBSDataClassesDataContext db;
        private readonly Repositorio repositorio;
        // EmailClient nr = new EmailClient();
        private readonly IMapper mapper;  
        private readonly Log log;
        private readonly IResponses responses; 
        public CajaController()
        {
            db = new MBSDataClassesDataContext();
            mapper = AutoMapperConfig.Mapper;
            log = new Log();
            responses = new Responses<object>();
            repositorio = new Repositorio();
        }
        // Get: api/api/caja
        [HttpGet]
        public async Task<HttpResponseMessage> GET([FromUri] FilterParameter filterParameter)
        {

            try
            {
                var Caja = (from e in db.CAJA01
                                      where (SqlMethods.Like(e.ID_CAJA.ToString(), '%' + filterParameter.Search + '%') ||
                                             SqlMethods.Like(e.ALMACEN.ToString(), '%' + filterParameter.Search + '%') ||
                                             SqlMethods.Like(e.DESCR_CAJA.Trim(), '%' + filterParameter.Search + '%') ||
                                             SqlMethods.Like(e.EQUIPO.Trim(), '%' + filterParameter.Search + '%') ||
                                             SqlMethods.Like(e.tienda.Trim(), '%' + filterParameter.Search + '%')||
                                             SqlMethods.Like(e.TIPOTARJETA.ToString(), '%' + filterParameter.Search + '%')||
                                             SqlMethods.Like(e.TIPOTARJETA2.ToString(), '%' + filterParameter.Search + '%'))                                            
                                      orderby (e.ID_CAJA)
                                      select e).DefaultIfEmpty();

                Caja.GroupBy(e => e.ID_CAJA).FirstOrDefault();
                var paagecaja = await Task.FromResult(PageList<CAJA01>.Create(Caja, filterParameter.pageNumber, filterParameter.PageSize));
                var map = mapper.Map<IEnumerable<CAJA01DTOs>>(paagecaja);
                var Responses = responses.RespGet(paagecaja, "caja", filterParameter.Search, map, paagecaja.metadata());
                return Request.CreateResponse(HttpStatusCode.OK, Responses);

               
            }
            catch (Exception e)
            {

                log.Error("error en el proceso GET: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }
        // Post: api/api/caja
        [HttpPost]
        [Authorize]
        public IHttpActionResult Post([FromBody] CAJA01DTOs Value)
        {

            try
            {
                if (Value == null)
                {
                    var Information = new
                    {
                        informacion = "configuration error the entity cannot be downloaded please complete the information "
                    };

                    return Ok(Information.ToString());

                }
                else
                {
                    var map = mapper.Map<CAJA01>(Value);
                    db.CAJA01.InsertOnSubmit(map);
                    db.SubmitChanges();
                    return Ok(" Caja Insertado");
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }





        }
        [HttpPut]
        [Authorize]
        // PUT: api/api/caja
        public async Task< HttpResponseMessage> Put([FromUri]int codigo, [FromBody] CAJA01DTOs values)
        {

            try
            {
                var Entidad = db.CAJA01.FirstOrDefault(e => e.ID_CAJA == codigo);
                if (Entidad == null || values == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                                "field empty please complete the code or code no exist");
                }
                else
                {
                    var rp = repositorio.CajaUpdate(Entidad, values);
                    var map = mapper.Map<CAJA01DTOs>(rp);
                    var response =  responses.RespUdate(map) ;
                    return await Task.FromResult(   Request.CreateResponse(HttpStatusCode.OK, response));


                }


            }
            catch (Exception e)
            {
                log.Error("error en el proceso PUT: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e);

            }


        }

        [HttpDelete]
        [Authorize]
        // Delete: api/api/caja
        public IHttpActionResult Delete([FromUri]int Codigo)
        {
            try
            {
                var Caja = db.CAJA01.FirstOrDefault(e => e.ID_CAJA == Codigo);
                if (Caja == null)
                {
                    return Ok("please complete the code or NUM_CPTO NO exist:(" + Codigo + ")");
                }
                else
                {
                    db.CAJA01.DeleteOnSubmit(Caja);
                    db.SubmitChanges();
                    return Ok(" Caja Eliminado");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }

        }




    }
}