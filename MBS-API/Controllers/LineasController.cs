﻿
using AutoMapper;
using Infraestructura.DTOs;
using MBS_API.Areas.Repositorio;
using MBS_API.AutoMapper;
using MBS_API.DB;
using MBS_API.Models.LogManager;
using MBS_API.Models.Pagination;
using MBS_API.Models.Pagination.Interface;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MBS_API.Controllers
{
    public class LineasController : ApiController
    {
        private MBSDataClassesDataContext db;

        // EmailClient nr = new EmailClient();
        private readonly IMapper mapper; 
        private readonly Log log;
        public readonly IResponses responses;
        private readonly Repositorio repositorio;

        public LineasController()
        {
            db = new MBSDataClassesDataContext();
            mapper = AutoMapperConfig.Mapper;
            log = new Log();
            responses = new Responses<object>();
            repositorio = new Repositorio();
        }
        [HttpGet]
        // GET: api/LINEA
        public async Task<HttpResponseMessage> GET([FromUri] FilterParameter filterParameter)
        {
            try
            {
                var Lineas = (from linea in db.CLIN01
                              where (SqlMethods.Like(linea.CLV_LIN.Trim(), '%' + filterParameter.Search + '%') ||
                                    SqlMethods.Like(linea.DESC_LIN.Trim(), '%' + filterParameter.Search + '%') ||
                                    SqlMethods.Like(linea.CUENTA_COI.Trim(), '%' + filterParameter.Search + '%') ||
                                    SqlMethods.Like(linea.cuenta_coi_costo.Trim(), '%' + filterParameter.Search + '%') ||
                                    SqlMethods.Like(linea.CUENTA_COI_VENTA.Trim(), '%' + filterParameter.Search + '%'))
                              orderby (linea.NUM_REG)
                              select linea).DefaultIfEmpty();

                Lineas.GroupBy(e => e.CLV_LIN).FirstOrDefault();
                var Lineaspage = await Task.FromResult(PageList<CLIN01>.Create(Lineas, filterParameter.pageNumber, filterParameter.PageSize));
                var map = mapper.Map<IEnumerable<CLIN01DTOs>>(Lineaspage);
                var Responses = responses.RespGet(Lineaspage, "Lineas", filterParameter.Search, map, Lineaspage.metadata()); return Request.CreateResponse(HttpStatusCode.OK, Responses);
                
            }
            catch (Exception e)
            {
                log.Error("error en el proceso Get: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);

            }
        }
        [HttpPost]
        [Authorize]
        // POST: api/LINEA
        public IHttpActionResult Post([FromBody] CLIN01DTOs value)
        {
            try
            {
                if (value == null)
                {
                    var Information = new
                    {
                        informacion = "configuration error the entity cannot be downloaded please complete the information "
                    };

                    return Ok(Information.ToString());

                }
                else
                {
                    var map = mapper.Map<CLIN01>(value);
                    db.CLIN01.InsertOnSubmit(map);
                    db.SubmitChanges();

                    return Ok("Linea Insertado");
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }
        }

        [HttpPut]
       [Authorize]
        // PUT: api/LINEA
        public async Task< HttpResponseMessage> Put([FromUri]string codigo, [FromBody] CLIN01DTOs Values)
        {

            try
            {
                var Entidad = db.CLIN01.FirstOrDefault(e => e.CLV_LIN.Trim() == codigo);
                if (Entidad == null || Values == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                                "field empty please complete the code or code no exist");
                }
                else
                {

                    var rp = repositorio.lineaUpdate(Entidad, Values);
                    var map = mapper.Map<CLIN01DTOs>(rp);
                    var response = responses.RespUdate(map) ;
                    return await Task.FromResult(   Request.CreateResponse(HttpStatusCode.OK, response));


                }
            

            }
            catch (Exception e)
            {
                log.Error("error en el proceso PUT: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e);
              
            }


        }

        // DELETE: api/LINEA/5

       [HttpDelete]
       [Authorize]
        public IHttpActionResult Delete([FromUri]string Codigo)
        {
            try
            {
               
                if (string.IsNullOrEmpty(Codigo))
                {
                    var Information = new
                    {
                        informacion = "empty field,please complete the code"
                    };

                    return Ok(Information);

                }
                else
                {
                    var lineas = db.CLIN01.FirstOrDefault(e => e.CLV_LIN.Trim() == Codigo);                    
                    if (lineas==null)
                    {
                        return Ok(" please complete the code or lineas NO exist:("+ Codigo + ")");
                    }
                    else
                    {
                        db.CLIN01.DeleteOnSubmit(lineas);
                        db.SubmitChanges();
                        return Ok("lineas Eliminada");
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso DELETE: " + e);
                // var f =new EmailClient.SendEmail(" Reporte de error", e.Message, log.Path);

                return NotFound();
            }
        }
    }

}
