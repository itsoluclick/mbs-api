﻿using AutoMapper;
using Infraestructura.DTOs;
using MBS_API.Areas.Repositorio;
using MBS_API.AutoMapper;
using MBS_API.DB;
using MBS_API.Models.LogManager;
using MBS_API.Models.Pagination;
using MBS_API.Models.Pagination.Interface;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MBS_API.Controllers
{
    public class FabricantesController : ApiController
    {
        private MBSDataClassesDataContext db;
        private readonly Repositorio repositorio;
        // EmailClient nr = new EmailClient();
        private readonly IMapper mapper; 
        private readonly Log log;
        private readonly IResponses responses; 
       
        public FabricantesController()
        {
            db = new MBSDataClassesDataContext();
            mapper = AutoMapperConfig.Mapper;
            log = new Log();
            responses = new Responses<object>();
            repositorio = new Repositorio();
        }
        // Get: api/api/FABRICANTE
        [HttpGet]
        public async Task<HttpResponseMessage> GET([FromUri] FilterParameter filterParameter)
        {

            try
            {
                var fabricantes = (from e in db.FABRICANTE
                                where (SqlMethods.Like(e.CODIGO_FAB.ToString().Trim(), '%' + filterParameter.Search + '%') ||
                                       SqlMethods.Like(e.DESCR_FAB.Trim(), '%' + filterParameter.Search + '%') ||
                                       SqlMethods.Like(e.ESTADO.Trim(), '%' + filterParameter.Search + '%') ||
                                       SqlMethods.Like(e.PAIS.ToString().Trim(), '%' + filterParameter.Search + '%'))
                                orderby (e.CODIGO_FAB)
                                select e).DefaultIfEmpty();

                fabricantes.GroupBy(e => e.CODIGO_FAB).FirstOrDefault();
                var fabrigecaja = await Task.FromResult(PageList<FABRICANTE>.Create(fabricantes, filterParameter.pageNumber, filterParameter.PageSize));
                var map = mapper.Map<IEnumerable<FABRICANTEDTOs>>(fabrigecaja);
                var Responses = responses.RespGet(fabrigecaja, "FABRICANTE", filterParameter.Search, map, fabrigecaja.metadata());
                return Request.CreateResponse(HttpStatusCode.OK, Responses);
                
            }
            catch (Exception e)
            {
                log.Error("error en el proceso GET: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }
        // Post: api/api/fabricante
        [HttpPost]
         [Authorize]
        public IHttpActionResult Post([FromBody] FABRICANTEDTOs Value)
        {

            try
            {
                if (Value == null)
                {
                    var Information = new
                    {
                        informacion = "configuration error the entity cannot be downloaded please complete the information "
                    };

                    return Ok(Information.ToString());

                }
                else
                {
                    var map = mapper.Map<FABRICANTE>(Value);
                    db.FABRICANTE.InsertOnSubmit(map);
                    db.SubmitChanges();
                    return Ok(" Fabricnate Insertado");
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }





        }
        [HttpPut]
        [Authorize]
        // PUT: api/api/fabricante
        public async Task< HttpResponseMessage> Put(int codigo, [FromBody] FABRICANTEDTOs values)
        {

            try
            {
                var Entidad = db.FABRICANTE.FirstOrDefault(e => e.CODIGO_FAB == codigo);
                if (Entidad == null || values == null)
                {


                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                            "field empty please complete the code");
                }
                else
                {
                    var rp = repositorio.FabricanteUpdate(Entidad,values);               
                    var map = mapper.Map<FABRICANTEDTOs>(rp);
                    var response = responses.RespUdate(map);
                    return await Task.FromResult(   Request.CreateResponse(HttpStatusCode.OK, response));


                }


            }
            catch (Exception e)
            {
                log.Error("error en el proceso PUT: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e);

            }


        }

        [HttpDelete]
         [Authorize]
        // Delete: api/api/fabricante
        public IHttpActionResult Delete(int Codigo)
        {
            try
            {
                var Fabricante = db.FABRICANTE.FirstOrDefault(e => e.CODIGO_FAB == Codigo);
                if (Fabricante == null)
                {
                    return Ok("please complete the code or NUM_CPTO NO exist:(" + Codigo + ")");
                }
                else
                {
                    db.FABRICANTE.DeleteOnSubmit(Fabricante);
                    db.SubmitChanges();
                    return Ok(" Fabricante Eliminado");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }

        }
    }
    }