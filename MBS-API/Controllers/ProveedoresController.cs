﻿using AutoMapper;
using Infraestructura.DTOs;
using MBS_API.Areas.Repositorio;
using MBS_API.AutoMapper;
using MBS_API.DB;
using MBS_API.Models.LogManager;
using MBS_API.Models.Pagination;
using MBS_API.Models.Pagination.Interface;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MBS_API.Controllers
{
    public class ProveedoresController: ApiController
    {
        private MBSDataClassesDataContext db;

        // EmailClient nr = new EmailClient();
        private readonly IMapper mapper; 
        private readonly Log log;
        private readonly IResponses responses;
        private readonly Repositorio repositorio;
       
        public ProveedoresController()
        {
            db = new MBSDataClassesDataContext();
            mapper = AutoMapperConfig.Mapper;
            log = new Log();
            responses = new Responses<object>();
        }
        [HttpGet]
        //Get/api/proveedores
        public async Task<HttpResponseMessage> GET([FromUri] FilterParameter filterParameter)
        {
            try
            {
                var Proveedores = (from proveedore in db.PROV01
                                 where (SqlMethods.Like(proveedore.TELEFONOS.Trim(), '%' + filterParameter.Search + '%') ||
                                        SqlMethods.Like(proveedore.CPROV.Trim(), '%' + filterParameter.Search + '%') ||
                                        SqlMethods.Like(proveedore.NOMBRE.Trim(), '%' + filterParameter.Search + '%') ||
                                        SqlMethods.Like(proveedore.STATUS.Trim(), '%' + filterParameter.Search + '%')||
                                        SqlMethods.Like(proveedore.CODIGO.Trim(), '%' + filterParameter.Search + '%')||
                                        SqlMethods.Like(proveedore.RFC.Trim(), '%' + filterParameter.Search + '%'))
                                 orderby (proveedore.NUM_REG)
                                 select proveedore).DefaultIfEmpty();
                Proveedores.GroupBy(e => e.CODIGO).FirstOrDefault();
                
                var Proveepage = await Task.FromResult(PageList<PROV01>.Create(Proveedores, filterParameter.pageNumber, filterParameter.PageSize));
                var map = mapper.Map<IEnumerable<PROV01DTOs>>(Proveepage);
                var Responses = responses.RespGet(Proveepage, "vendors", filterParameter.Search, map, Proveepage.metadata());
                return Request.CreateResponse(HttpStatusCode.OK, Responses);
              

            }
            catch (Exception e)
            {
                log.Error("error en el proceso Get: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);

            }

        }

        //POST: api/proveedores
        [Authorize]
        [HttpPost]
        public IHttpActionResult Post([FromBody] PROV01DTOs value)
        {
            try
            {
                if (value == null)
                {
                    var Information = new
                    {
                        informacion = "configuration error the entity cannot be downloaded please complete the information "
                    };

                    return Ok(Information.ToString());

                }
                else
                {
                    var map = mapper.Map<PROV01>(value);
                    db.PROV01.InsertOnSubmit(map);
                    db.SubmitChanges();

                    return Ok("Proveedor Insertado");
                }
            }



            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }

        }

        // PUT: api/Productos
        [HttpPut]
        [Authorize]
        public async Task< HttpResponseMessage> Put([FromUri]string codigo, [FromBody] PROV01DTOs Values)
        {
            try
            {
                var Entidad = db.PROV01.FirstOrDefault(e => e.CPROV == codigo);
                if (Entidad == null || Values == null)
                {


                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                            "field empty please complete the code");
                }
                else
                {


                    var rp = repositorio.ProveedoresUpdate(Entidad, Values);
                    var map = mapper.Map<IEnumerable<PROV01DTOs>>(Entidad);
                    var response = responses.RespUdate(map) ;
                    return await Task.FromResult(   Request.CreateResponse(HttpStatusCode.OK, response));


                }

            }
            catch (Exception e)
            {
                log.Error("error en el proceso PUT: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e);
            }
        }


        // DELETE: api/Productos?codigo=PRISD
        [Authorize]
        [HttpDelete]
        public IHttpActionResult Delete([FromUri]string Codigo)
        {
            try
            {
                if (string.IsNullOrEmpty(Codigo))
                {
                    var Information = new
                    {
                        informacion = "empty field,please complete the code"
                    };

                    return Ok(Information);

                }
                else
                {
                    var Proveedores = db.PROV01.FirstOrDefault(e => e.CPROV.Trim() == Codigo);
                    if (Proveedores == null)
                    {
                        return Ok("please complete the code or Proveedor NO exist:(" + Codigo + ")");
                    }
                    else
                    {

                        db.PROV01.DeleteOnSubmit(Proveedores);
                        db.SubmitChanges();
                        return Ok("Proveedor Eliminado");
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso DELETE: " + e);
                // var f =new EmailClient.SendEmail(" Reporte de error", e.Message, log.Path);
                return NotFound();
            }
        }
    }
}

   
