﻿using AutoMapper;
using Infraestructura.DTOs;
using MBS_API.AutoMapper;
using MBS_API.DB;
using MBS_API.Models.LogManager;
using MBS_API.Models.Pagination;
using MBS_API.Models.Pagination.Interface;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MBS_API.Controllers
{
    public class CuentasxCobarController : ApiController
    {
        private MBSDataClassesDataContext db;
        // EmailClient nr = new EmailClient();
        private readonly IMapper mapper; 
        private readonly Log log;
        private readonly IResponses responses;       
       
        public CuentasxCobarController()
        {
            db = new MBSDataClassesDataContext();
            mapper = AutoMapperConfig.Mapper;
            log = new Log();
            responses = new Responses<object>();
        }
        // EmailClient nr = new EmailClient();
        [HttpGet]
        // GET: api/  CuentasXCobar
        public async Task<HttpResponseMessage> GET([FromUri] FilterParameter filterParameter)
        {
            try
            { 
            
                var CXC = (from e in db.CUEN01
                                       where (SqlMethods.Like(e.NUM_REG.ToString(), '%' + filterParameter.Search + '%') ||
                                              SqlMethods.Like(e.CCLIE.ToString(), '%' + filterParameter.Search + '%') ||
                                              SqlMethods.Like(e.STATUS.Trim(), '%' + filterParameter.Search + '%') ||
                                              e.FECHA_APLI == filterParameter.Date() ||
                                              SqlMethods.Like(e.NO_FACTURA.Trim(), '%' + filterParameter.Search + '%') ||
                                              SqlMethods.Like(e.TIPO_MOV.ToString(), '%' + filterParameter.Search + '%') ||
                                              SqlMethods.Like(e.BANK_COM.ToString(), '%' + filterParameter.Search + '%') ||
                                              SqlMethods.Like(e.DOCUMENTO.Trim(), '%' + filterParameter.Search + '%') ||
                                              SqlMethods.Like(e.CVE_CTA.ToString(), '%' + filterParameter.Search + '%') ||
                                              SqlMethods.Like(e.cod_dev.ToString(), '%' + filterParameter.Search + '%') ||
                                              SqlMethods.Like(e.AFEC_COI.Trim(), '%' + filterParameter.Search + '%') ||
                                              SqlMethods.Like(e.STRCVEVEND.Trim(), '%' + filterParameter.Search + '%') ||
                                             SqlMethods.Like(e.DOCTO.Trim(), '%' + filterParameter.Search + '%') ||
                                              SqlMethods.Like(e.OBS_CXCCXP.ToString(), '%' + filterParameter.Search + '%'
                                             ))
                                       orderby (e.NUM_REG)
                                       select e).DefaultIfEmpty();
                CXC.GroupBy(e => e.STATUS).FirstOrDefault();
                var CXCpage = await Task.FromResult(PageList<CUEN01>.Create(CXC, filterParameter.pageNumber, filterParameter.PageSize));
                var map = mapper.Map<IEnumerable<CUEN01DTOs>>(CXCpage);
                var Responses = responses.RespGet(CXCpage, "CXC", filterParameter.Search, map, CXCpage.metadata()); 
                return Request.CreateResponse(HttpStatusCode.OK, Responses);               
            }

            catch (Exception e)
            {
                log.Error("error en el proceso Get: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);

            }
        }
        [HttpPost]
        [Authorize]
        // POST: api/  CuentasXCobar{}
        public IHttpActionResult Post([FromUri] int?   rec_auto , [FromUri] string   almacen,[FromUri] string CONDICION_PAGO,[FromUri]  string  cobrador  )
        {
            try
            {
                  
                if (rec_auto == null || string.IsNullOrEmpty(almacen) || string.IsNullOrEmpty(CONDICION_PAGO)|| string.IsNullOrEmpty(cobrador)  )
                {
                    
                    var Information = new
                    {
                        informacion = "please complete the fields to continue with the transaction"
                    };

                    return Ok(Information.ToString());

                }
                else
                {
                    var confiracion = db.TB_REC_DETALLE.Where(e => e.REC_AUTO == rec_auto && e.ALMACEN == almacen);
                    if (confiracion == null)
                    {
                        var Information = new
                        {
                            informacion = "please verify registration"
                        };
                        return Ok(Information.ToString());
                    }
                    else
                    {
                        db.Stp_ins_cuenta_rec(rec_auto, almacen, CONDICION_PAGO, cobrador);
                        db.SubmitChanges();

                    }
                    return Ok("CxC Insertado");

                }

            }

            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }
        }
    }
}

