﻿using AutoMapper;
using Infraestructura.DTOs;
using MBS_API.AutoMapper;
using MBS_API.DB;
using MBS_API.Models.LogManager;
using MBS_API.Models.Pagination;
using MBS_API.Models.Pagination.Interface;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MBS_API.Controllers
{
    public class GruposController : ApiController
    {
        private MBSDataClassesDataContext db;

        // EmailClient nr = new EmailClient();
        private readonly IMapper mapper; 
        private readonly Log log;
        private readonly IResponses responses; 
       
        public GruposController()
        {
            db = new MBSDataClassesDataContext();
            mapper = AutoMapperConfig.Mapper;
            log = new Log();
            responses = new Responses<object>();


        }
        [HttpGet]
        public async Task<HttpResponseMessage> GET([FromUri] FilterParameter filterParameter)
        {
            try
            {
                var Grupos = (from Grupo in db.INVGRUM
                              where (SqlMethods.Like(Grupo.ESTADO, '%' + filterParameter .Search+ '%') ||
                              SqlMethods.Like(Grupo.INVGRU_DESCRIPCION, '%' +filterParameter.Search + '%')||
                              SqlMethods.Like(Grupo.INVGRU_CODIGO.ToString(), '%' + filterParameter.Search + '%'))
                              orderby Grupo.INVGRU_CODIGO
                              select Grupo).DefaultIfEmpty();
                Grupos.GroupBy(e => e.ESTADO).FirstOrDefault();
                var Grupospage = await Task.FromResult(PageList<INVGRUM>.Create(Grupos, filterParameter.pageNumber, filterParameter.PageSize));
                var map = mapper.Map<IEnumerable<INVGRUMDTOs>>(Grupospage);
                var Responses = responses.RespGet(Grupospage, "Group", filterParameter.Search, map, Grupospage.metadata()); 
                return Request.CreateResponse(HttpStatusCode.OK, Responses);
               

            }

            catch (Exception e)
            {
                log.Error("error en el proceso Get: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);

            }



        }
        [HttpPost]
        [Authorize]
        // POST: api/GROUP
        public IHttpActionResult Post([FromBody] INVGRUMDTOs value)
        {
            try
            {
                if (value == null)
                {
                    var Information = new
                    {
                        informacion = "configuration error the entity cannot be downloaded please complete the information "
                    };

                    return Ok(Information.ToString());

                }
                else
                {
                    var map = mapper.Map<INVGRUM>(value);
                    db.INVGRUM.InsertOnSubmit(map);
                    db.SubmitChanges();
                    return Ok("Grupo Insertado");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }
        }
       


        [HttpPut]
        [Authorize]
        // PUT: api/api/GROUP
        public async Task< HttpResponseMessage> Put(int codigo, [FromBody] INVGRUMDTOs Values)
        {

            try
            {
                var Entidad = db.INVGRUM.FirstOrDefault(e => e.INVGRU_CODIGO == codigo);
                if (Entidad == null || Values == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                                "field empty please complete the code or code no exist");
                }
                else
                {

                    //Entidad.INVGRU_CODIGO = Values.INVGRU_CODIGO;
                    Entidad.INVGRU_DESCRIPCION = Values.INVGRU_DESCRIPCION;
                    Entidad.ESTADO = Values.ESTADO;
                    db.SubmitChanges();
                    var map = mapper.Map<IEnumerable<INVGRUMDTOs>>(Entidad);
                    var response = responses.RespUdate(map);
                    return await Task.FromResult(   Request.CreateResponse(HttpStatusCode.OK, response));


                }


            }
            catch (Exception e)
            {
                log.Error("error en el proceso PUT: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e);

            }


        }
        [HttpDelete]
        [Authorize]
        // DELETE: api/api/GROUP
        public IHttpActionResult Delete(int Codigo)
        {
            try
            {
               var Grupos = db.INVGRUM.FirstOrDefault(e => e.INVGRU_CODIGO == Codigo);
                    if (Grupos == null)
                    {
                        return Ok("please complete the code or Grupo NO exist:(" + Codigo + ")");
                    }
                    else
                    {
                        db.INVGRUM.DeleteOnSubmit(Grupos);
                        db.SubmitChanges();
                        return Ok("Grupo Eliminado");
                    }
                
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso DELETE: " + e);
                // var f =new EmailClient.SendEmail(" Reporte de error", e.Message, log.Path);

                return NotFound();
            }
        }
    }

    }