﻿using AutoMapper;
using Infraestructura.DTOs;
using MBS_API.Areas.Repositorio;
using MBS_API.AutoMapper;
using MBS_API.DB;
using MBS_API.Models.LogManager;
using MBS_API.Models.Pagination;
using MBS_API.Models.Pagination.Interface;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MBS_API.Controllers
{
    public class MovimientoBancarioController : ApiController
    {
        private MBSDataClassesDataContext db;

        // EmailClient nr = new EmailClient();
        private readonly IMapper mapper; 
        private readonly Log log;
        public readonly IResponses responses;
        public readonly Repositorio repositorio;
      public MovimientoBancarioController()
        {
            db = new MBSDataClassesDataContext();
             mapper = AutoMapperConfig1._Imapper;
            responses = new Responses<object>();
            log = new Log();
            repositorio = new Repositorio();
        }

        [HttpGet]
        // GET: api/MovimientoBancario/5
        public async Task<HttpResponseMessage> Get([FromUri] FilterParameter filterParameter)
        {
            try
            {
                var MB = (from e in db.MOVS0101
                          where (SqlMethods.Like(e.BAND_CONC, '%' + filterParameter.Search + '%')) ||
                     SqlMethods.Like(e.BAND_CONC.Trim(), '%' + filterParameter.Search + '%') ||
                     SqlMethods.Like(e.TIPO.Trim(), '%' + filterParameter.Search + '%') ||
                     SqlMethods.Like(e.TIP_REG.ToString(), '%' + filterParameter.Search + '%') ||
                     SqlMethods.Like(e.CON_PART.ToString(), '%' + filterParameter.Search + '%') ||
                     SqlMethods.Like(e.CTA_BAN.ToString(), '%' + filterParameter.Search + '%') ||
                     SqlMethods.Like(e.NUM_CHEQUE.ToString(), '%' + filterParameter.Search + '%') ||
                     SqlMethods.Like(e.STATUS.Trim(), '%' + filterParameter.Search + '%') ||
                     SqlMethods.Like(e.FACT.Trim(), '%' + filterParameter.Search + '%') ||
                     SqlMethods.Like(e.TRANSFER.Trim(), '%' + filterParameter.Search + '%') ||
                      SqlMethods.Like(e.BENEF.Trim(), '%' + filterParameter.Search + '%') ||
                      SqlMethods.Like(e.CVE_INST.ToString(), '%' + filterParameter.Search + '%') ||
                      SqlMethods.Like(e.CTA_TRANSF.ToString(), '%' + filterParameter.Search + '%') ||
                      SqlMethods.Like(e.NUM_POL.Trim(), '%' + filterParameter.Search + '%') ||
                      SqlMethods.Like(e.id_projecto.ToString(), '%' + filterParameter.Search + '%') ||
                      SqlMethods.Like(e.REF.Trim(), '%' + filterParameter.Search + '%') ||
                      SqlMethods.Like(e.FORMA_PAGO.Trim(), '%' + filterParameter.Search + '%') ||
                      e.FECHA == filterParameter.Date() || e.FECHA_LIQ == filterParameter.Date()
                          orderby e.CTA_BAN
                          select e).DefaultIfEmpty();
                MB.GroupBy(e => e.TIP_REG).FirstOrDefault();
                var mbanco = await Task.FromResult(PageList<MOVS0101>.Create(MB, filterParameter.pageNumber, filterParameter.PageSize));
                var map = mapper.Map<IEnumerable<MOVS0101DTOs>>(mbanco);
                var Responses = responses.RespGet(mbanco, "Movimientos Bancarios", filterParameter.Search, map, mbanco.metadata());
                return Request.CreateResponse(HttpStatusCode.OK, Responses);

            }

            catch (Exception e)
            {
                log.Error("error en el proceso Get: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);

            }



        }
        [HttpPost]
        [Authorize]
        // POST: api/MovimientoBancario
        public IHttpActionResult Post([FromBody] MOVS0101DTOs value)
        {
            try
            {
                if (value == null)
                {
                    var Information = new
                    {
                        informacion = "configuration error the entity cannot be downloaded please complete the information "
                    };

                    return Ok(Information.ToString());

                }
                else
                {

                    var map = mapper.Map<IEnumerable<MOVS0101>>(value);
                    foreach (MOVS0101 e in map)
                    {
                        db.MOVS0101.InsertOnSubmit(e);
                        db.SubmitChanges();
                    }

                    return Ok("Movimiento Bancario Insertado");
                }
            }

            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }

        }
        [Authorize]
        [HttpPut]
        // PUT: api/MovimientoBancario/5
        public async Task<HttpResponseMessage> Put([FromUri] int CTA_BAN, [FromUri] short TIP_REG, [FromBody] MOVS0101DTOs Values)
        {
            try
            {
                var Entidad = db.MOVS0101.FirstOrDefault(e => e.CTA_BAN == CTA_BAN && e.TIP_REG ==TIP_REG);
                if (Entidad == null || Values == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                                "field empty please complete the code or code no exist");
                }
                else
                {
                    var rp = repositorio.MovimientoBancarioUpdate(Entidad, Values);
                    var map = mapper.Map<MOVS0101DTOs>(rp);
                    var response = responses.RespUdate(map);             
                    return await Task.FromResult(Request.CreateResponse(HttpStatusCode.OK, response));


                }


            }
            catch (Exception e)
            {
                log.Error("error en el proceso PUT: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e);

            }
        }
        [HttpDelete]
        [Authorize]
        // DELETE: api/MovimientoBancario/5
        public IHttpActionResult Delete([FromUri] int Codigo)
        {
            try
            {
                var MB = db.MOVS0101.FirstOrDefault(e => e.CTA_BAN == Codigo);
                if (MB == null)
                {
                    return Ok("please complete the code or SubGrupo NO exist:(" + Codigo + ")");
                }
                else
                {
                    db.MOVS0101.DeleteOnSubmit(MB);
                    db.SubmitChanges();
                    return Ok("Movimiento Bancario Eliminado");
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso DELETE: " + e);
                // var f =new EmailClient.SendEmail(" Reporte de error", e.Message, log.Path);
                return NotFound();
            }

        }
    }
}
