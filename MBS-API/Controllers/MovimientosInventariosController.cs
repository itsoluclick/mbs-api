﻿using AutoMapper;
using Infraestructura.DTOs;
using MBS_API.Areas.Repositorio;
using MBS_API.AutoMapper;
using MBS_API.DB;
using MBS_API.Models.LogManager;
using MBS_API.Models.Pagination;
using MBS_API.Models.Pagination.Interface;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MBS_API.Controllers
{
    public class MovimientosInventariosController : ApiController
    {
        private MBSDataClassesDataContext db;

        // EmailClient nr = new EmailClient();
        private readonly IMapper mapper;
        private readonly Log log;
        private readonly IResponses responses;
        private readonly Repositorio repositorio;

        public MovimientosInventariosController()
        {
            db = new MBSDataClassesDataContext();
            mapper = AutoMapperConfig.Mapper;
            log = new Log();
            responses = new Responses<object>();
            repositorio = new Repositorio();
        }

        [HttpGet]
        //Get/api/movimientoinventario

        public async Task<HttpResponseMessage> GET([FromUri] FilterParameter filterParameter)
        {
            try
            {
                var movimiento = (from e in db.MINV01
                                  where (SqlMethods.Like(e.TIPO_MOV.ToString(), '%' + filterParameter.Search + '%') ||
                                         SqlMethods.Like(e.FECHA_DOCU.ToString(), '%' + filterParameter.Search + '%') ||
                                         SqlMethods.Like(e.REFER.Trim(), '%' + filterParameter.Search + '%') ||
                                         SqlMethods.Like(e.CLAVE_CLPV.Trim(), '%' + filterParameter.Search + '%') ||
                                         SqlMethods.Like(e.VEND.Trim(), '%' + filterParameter.Search + '%') ||
                                         SqlMethods.Like(e.ALMACEN.ToString(), '%' + filterParameter.Search + '%') ||
                                         SqlMethods.Like(e.TIPO_PROD.ToString(), '%' + filterParameter.Search + '%') ||
                                         SqlMethods.Like(e.FACTOR_CON.ToString(), '%' + filterParameter.Search + '%') ||
                                         /*
                                          SqlMethods.Like(e.ALMACEN_DESTINO.ToString(), '%' + filterParameter.Search + '%') ||
                                          SqlMethods.Like(e.ALMACEN_ORIGEN.ToString(), '%' + filterParameter.Search + '%') ||
                                          SqlMethods.Like(e.TELEFONO.Trim(), '%' + filterParameter.Search + '%') ||
                                          SqlMethods.Like(e.documento2.Trim(), '%' + filterParameter.Search + '%') ||
                                          SqlMethods.Like(e.NOMBRE.ToString(), '%' + filterParameter.Search + '%') ||
                                          */
                                         SqlMethods.Like(e.DOCUMENTO.ToString(), '%' + filterParameter.Search + '%') ||
                                         SqlMethods.Like(e.NUM_REG.ToString(), '%' + filterParameter.Search + '%') ||
                                         SqlMethods.Like(e.CLV_ART.Trim(), '%' + filterParameter.Search + '%'))
                                  orderby (e.NUM_REG)
                                  select e).DefaultIfEmpty();
                movimiento.GroupBy(e => e.TIPO_MOV).FirstOrDefault();
                var movimientopage = await Task.FromResult(PageList<MINV01>.Create(movimiento, filterParameter.pageNumber, filterParameter.PageSize));
                var map = mapper.Map<IEnumerable<MINV01DTOs>>(movimientopage);
                var Responses = responses.RespGet(movimientopage, "Movimiento inventario", filterParameter.Search, map, movimientopage.metadata());
                return Request.CreateResponse(HttpStatusCode.OK, Responses);
            }
            catch (Exception e)
            {
                log.Error("error en el proceso Get: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);

            }

        }
    
        [HttpPost]
        // POST: api/movimiento inventario[{},{}]
       [Authorize]
        public IHttpActionResult Post([FromBody] List< MINV01DTOs> value)
        {
            try
            {
                if (value == null)
                {
                    var Information = new
                    {
                        informacion = "configuration error the entity cannot be downloaded please complete the information "
                    };

                    return Ok(Information.ToString());

                }
                else
                {

                    var map = mapper.Map<IEnumerable<MINV01>>(value);
                    foreach (MINV01 e in map)
                    {
                        db.MINV01.InsertOnSubmit(e);
                        db.SubmitChanges(); 
                    }

                    return Ok("Movimiento Inventario Insertado");
                }
            }



            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }

        }

        // PUT: api/movimientoinventario?id
        [HttpPut]
        [Authorize]
        public async Task<HttpResponseMessage> Put([FromUri]int codigo, [FromBody] MINV01DTOs Values)
        {
            try
            {
                var Entidad = db.MINV01.FirstOrDefault(e => e.NUM_REG == codigo);
                if (Entidad == null || Values == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                                "field empty please complete the code or code no exist");
                }
                else

                {
                    var rp = repositorio.MovimientoInventarioUpdate(Entidad, Values);
                    var map = mapper.Map<MINV01DTOs>(rp);
                    var response = responses.RespUdate(map) ;
                    return await Task.FromResult( Request.CreateResponse(HttpStatusCode.OK, response));


                }

            }
            catch (Exception e)
            {
                log.Error("error en el proceso PUT: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e);
            }
        }


        // DELETE: api/Productos?codigo=PRISD
        [Authorize]
       [HttpDelete]
        public IHttpActionResult Delete([FromUri]string Codigo)
        {
            try
            {
                if (string.IsNullOrEmpty(Codigo))
                {
                    var Information = new
                    {
                        informacion = "empty field,please complete the code"
                    };

                    return Ok(Information);

                }
                else
                {


                    var movimiento = db.MINV01.FirstOrDefault(e => e.CLV_ART.Trim()== Codigo);
                    if (movimiento == null)
                    {
                        return Ok("please complete the code or Movimiento inventario NO exist:(" + Codigo + ")");
                    }
                    else
                    {

                        db.MINV01.DeleteOnSubmit(movimiento);
                        db.SubmitChanges();
                        return Ok("Movimiento al inventario eliminado Eliminado");
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso DELETE: " + e);
                // var f =new EmailClient.SendEmail(" Reporte de error", e.Message, log.Path);

                return NotFound();
            }
        }
    }
}

