﻿using AutoMapper;
using Infraestructura.DTOs;
using MBS_API.AutoMapper;
using MBS_API.DB;
using MBS_API.Models.LogManager;
using MBS_API.Models.Pagination;
using MBS_API.Models.Pagination.Interface;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MBS_API.Controllers
{
    public class DepartamentosController : ApiController
    {

        private MBSDataClassesDataContext db;

        // EmailClient nr = new EmailClient();
        private readonly IMapper mapper; 
        private readonly Log log;
        private readonly IResponses responses; 
       
        public DepartamentosController()
        {
            db = new MBSDataClassesDataContext();
            mapper = AutoMapperConfig.Mapper;
            log = new Log();
            responses = new Responses<object>();
        }
        [HttpGet]
        // GET: api/Departamentos
        public async Task<HttpResponseMessage> GET([FromUri] FilterParameter filterParameter)
        {
            try
            {
                var Departamentos = (from e in db.DEPTO
                              where (SqlMethods.Like(e.DEPTO1.Trim(), '%' + filterParameter.Search + '%') ||
                                    SqlMethods.Like(e.IDDEPTO.ToString(), '%' + filterParameter.Search + '%') ||
                                    SqlMethods.Like(e.DESCR.ToString(), '%' + filterParameter.Search + '%') )
                                   
                              orderby (e.IDDEPTO)
                              select e).DefaultIfEmpty();

                Departamentos.GroupBy(e => e.DEPTO1).FirstOrDefault();
                var Departamentospage = await Task.FromResult(PageList<DEPTO>.Create(Departamentos, filterParameter.pageNumber, filterParameter.PageSize));
                var map = mapper.Map<IEnumerable<DEPTODTOs>>(Departamentospage);
                var Responses = responses.RespGet(Departamentospage, "Departamento", filterParameter.Search, map, Departamentospage.metadata()); return Request.CreateResponse(HttpStatusCode.OK, Responses);

            }

            catch (Exception e)
            {
                log.Error("error en el proceso Get: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);

            }
        }
        [HttpPost]
       [Authorize]
        // POST: api/Departamentos
        public IHttpActionResult Post([FromBody] DEPTODTOs value)
        {
            try
            {
                if (value == null)
                {
                    var Information = new
                    {
                        informacion = "configuration error the entity cannot be downloaded please complete the information "
                    };

                    return Ok(Information.ToString());

                }
                else
                {
                    var map = mapper.Map<DEPTO>(value);
                    db.DEPTO.InsertOnSubmit(map);
                    db.SubmitChanges();                                     

                    return Ok("Departamento Insertado");
                }

            }



            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }
        }

        [HttpPut]
       [Authorize]
        // PUT: api/Departamentos
        public async Task< HttpResponseMessage> Put([FromUri] int codigo, [FromBody] DEPTODTOs Values)
        {

            try
            {
                var Entidad = db.DEPTO.FirstOrDefault(e => e.IDDEPTO== codigo);
                if (Entidad == null || Values == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                                "field empty please complete the code or code no exist");
                }
                else
                {

                    Entidad.DEPTO1 = Values.DEPTO;
                    Entidad.DESCR = Values.DESCR;                

                    db.SubmitChanges();
                    var map = mapper.Map<DEPTODTOs>(Entidad);
                    var response = responses.RespUdate(map);
                    return await Task.FromResult(   Request.CreateResponse(HttpStatusCode.OK, response));


                }


            }
            catch (Exception e)
            {
                log.Error("error en el proceso PUT: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e);

            }


        }

        // DELETE: api/Departamentos/5

        [HttpDelete]
       [Authorize]
        public IHttpActionResult Delete([FromUri] int Codigo)
        {
            try
            {
                var Departamento = db.DEPTO.FirstOrDefault(e => e.IDDEPTO == Codigo);
                if (Departamento == null)
                {
                    return Ok("please complete the code or SubGrupo NO exist:(" + Codigo + ")");
                }
                else
                {
                    db.DEPTO.DeleteOnSubmit(Departamento);
                    db.SubmitChanges();
                    return Ok("Departamento Eliminado");
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso DELETE: " + e);
                // var f =new EmailClient.SendEmail(" Reporte de error", e.Message, log.Path);

                return NotFound();
            }
        }
    }





}

