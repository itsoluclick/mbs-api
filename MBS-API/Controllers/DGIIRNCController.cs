﻿using AutoMapper;
using Infraestructura.DTOs;
using MBS_API.Areas.Repositorio;
using MBS_API.AutoMapper;
using MBS_API.DB;
using MBS_API.Models.LogManager;
using MBS_API.Models.Pagination;
using MBS_API.Models.Pagination.Interface;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MBS_API.Controllers
{
    public class DGIIRNCController : ApiController
    {
        private MBSDataClassesDataContext db;

        // EmailClient nr = new EmailClient();
        private readonly IMapper mapper;  
        private readonly Log log;
        private readonly IResponses responses;
        private readonly Repositorio repositorio;
        
       
        public DGIIRNCController()
        {
            db = new MBSDataClassesDataContext();
             mapper = AutoMapperConfig1._Imapper;
            responses = new Responses<object>();
            log = new Log();
            repositorio = new Repositorio();
        }
        // GET: api/DGIIRNC/5
        [HttpGet]
        public async Task<HttpResponseMessage> GET([FromUri] FilterParameter filterParameter)
        {
            try
            {
                var RNC = (from e in db.DGII_RNC
                               where (SqlMethods.Like(e.ACTIVIDAD.Trim(), '%' + filterParameter.Search + '%') ||
                                      SqlMethods.Like(e.EMPRESA.ToString(), '%' + filterParameter.Search + '%') ||
                                      SqlMethods.Like(e.EMPRESA2.ToString(), '%' + filterParameter.Search + '%') ||
                                      SqlMethods.Like(e.ESTATUS.ToString(), '%' + filterParameter.Search + '%') ||
                                      SqlMethods.Like(e.NORMAL.ToString(), '%' + filterParameter.Search + '%') ||
                                      SqlMethods.Like(e.RNC.Trim(), '%' + filterParameter.Search + '%') ||
                                      SqlMethods.Like(e.TIPO_NCF.Trim(), '%' + filterParameter.Search + '%'))
                               orderby (e.RNC)
                               select e).DefaultIfEmpty();


                RNC.GroupBy(e => e.EMPRESA).FirstOrDefault();
                var RNCpage = await Task.FromResult(PageList<DGII_RNC>.Create(RNC, filterParameter.pageNumber, filterParameter.PageSize));
                var map = mapper.Map<IEnumerable<DGII_RNCDTOs>>(RNCpage);
                var Responses = responses.RespGet(RNCpage, "DGII RNC", filterParameter.Search, map, RNCpage.metadata());
                return Request.CreateResponse(HttpStatusCode.OK, Responses);


                

            }
            catch (Exception e)
            {
                log.Error("error en el proceso Get: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);

            }

        }

        // POST: api/DGIIRNC
        [HttpPost]
        [Authorize]
        public IHttpActionResult Post([FromBody] DGII_RNCDTOs  value)
        {
           try
            {
                if (value == null)
                {
                    var Information = new
                    {
                        informacion = "configuration error the entity cannot be downloaded please complete the information "
                    };
                    return Ok(Information.ToString());

                }
                else
                {
                    var map = mapper.Map<DGII_RNC>(value);
                    db.DGII_RNC.InsertOnSubmit(map);
                    db.SubmitChanges();
                    return Ok("DGII_RNC Insertado");
                }

            }

            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }
        }

        // PUT: api/DGIIRNC/5
        [Authorize]
        [HttpPut]
        public async Task<HttpResponseMessage> Put([FromUri] string codigo, [FromBody] DGII_RNCDTOs Values)
        {
            try
            {
                var Entidad = db.DGII_RNC.FirstOrDefault(e => e.RNC == codigo);
                if (Entidad == null || Values == null)
                {


                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                            "field empty please complete the code");
                }
                else
                {



                    var rp = repositorio.DGII_RNCUpdate(Entidad,Values);
                    var map = mapper.Map<IEnumerable<DGII_RNCDTOs>>(Entidad);
                    var response = responses.RespUdate(map);
                    return await Task.FromResult(Request.CreateResponse(HttpStatusCode.OK, response));


                }

            }
            catch (Exception e)
            {
                log.Error("error en el proceso PUT: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e);
            }
        }


        // DELETE: api/DGIIRNC/5

        [Authorize]
        [HttpDelete]
        public IHttpActionResult Delete( [FromUri]  string Codigo)
        {
            try
            {
                if (string.IsNullOrEmpty(Codigo))
                {
                    var Information = new
                    {
                        informacion = "empty field,please complete the code"
                    };

                    return Ok(Information);

                }
                else
                {
                    var rnc = db.DGII_RNC.FirstOrDefault(e => e.RNC.Trim() == Codigo);
                    if (rnc == null)
                    {
                        return Ok("please complete the code or producto NO exist:(" + Codigo + ")");
                    }
                    else
                    {

                        db.DGII_RNC.DeleteOnSubmit(rnc);
                        db.SubmitChanges();
                        return Ok("Producto Eliminado");
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso DELETE: " + e);
                // var f =new EmailClient.SendEmail(" Reporte de error", e.Message, log.Path);
                return NotFound();
            }
        }
    }
}
