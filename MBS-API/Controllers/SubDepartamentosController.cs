﻿using AutoMapper;
using Infraestructura.DTOs;
using MBS_API.AutoMapper;
using MBS_API.DB;
using MBS_API.Models.LogManager;
using MBS_API.Models.Pagination;
using MBS_API.Models.Pagination.Interface;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MBS_API.Controllers
{
    public class SubDepartamentosController : ApiController
    {


        private MBSDataClassesDataContext db;

        // EmailClient nr = new EmailClient();
        private readonly IMapper mapper; 
        private readonly Log log;
        public readonly IResponses responses; 
       
        public SubDepartamentosController()
        {
            db = new MBSDataClassesDataContext();
            mapper = AutoMapperConfig.Mapper;
            log = new Log();
            responses = new Responses<object>();
        }
        [HttpGet]
        // GET: api/SubDepartamentos
        public async Task<HttpResponseMessage> GET([FromUri] FilterParameter filterParameter)
        {
            try
            {
                var SubDepartamentos = (from e in db.SUBDEPTO
                                     where (SqlMethods.Like(e.CODIGO_SUBDEPTO.ToString(), '%' + filterParameter.Search + '%') ||
                                           SqlMethods.Like(e.DESCR_SUBDEPTO.Trim(), '%' + filterParameter.Search + '%') ||
                                           SqlMethods.Like(e.IDDEPTO.ToString(), '%' + filterParameter.Search + '%'))

                                     orderby (e.IDDEPTO)
                                     select e).DefaultIfEmpty();
                SubDepartamentos.GroupBy(e => e.CODIGO_SUBDEPTO).FirstOrDefault();
                var SubDepartamentospage = await Task.FromResult(PageList<SUBDEPTO>.Create(SubDepartamentos, filterParameter.pageNumber, filterParameter.PageSize));
                var map = mapper.Map<IEnumerable<SUBDEPTODTOs>>(SubDepartamentospage);
                var Responses = responses.RespGet(SubDepartamentospage, "SubDepartamento", filterParameter.Search, map, SubDepartamentospage.metadata()); 
                return Request.CreateResponse(HttpStatusCode.OK, Responses);
               
            }

            catch (Exception e)
            {
                log.Error("error en el proceso Get: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);

            }
        }
        [HttpPost]
        [Authorize]
        // POST: api/SubDepartamentos
        public IHttpActionResult Post([FromBody] SUBDEPTODTOs value)
        {
            try
            {
                if (value == null)
                {
                    var Information = new
                    {
                        informacion = "configuration error the entity cannot be downloaded please complete the information "
                    };

                    return Ok(Information.ToString());

                }
                else
                {
                    var subd = mapper.Map<SUBDEPTO>(value);
                    db.SUBDEPTO.InsertOnSubmit(subd);
                    db.SubmitChanges();
                    return Ok("SubDepartamento Insertado");
                }

            }



            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }
        }

        [HttpPut]
        [Authorize]
        // PUT: api/SubDepartamentos
        public async Task< HttpResponseMessage> Put([FromUri]int codigo, [FromBody] SUBDEPTODTOs Values)
        {

            try
            {
                var Entidad = db.SUBDEPTO.FirstOrDefault(e => e.CODIGO_SUBDEPTO == codigo);
                if (Entidad == null || Values == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                                "field empty please complete the code or code no exist");
                }
                else
                {

                    Entidad.DESCR_SUBDEPTO = Values.DESCR_SUBDEPTO;
                    Entidad.IDDEPTO = Values.IDDEPTO;

                    db.SubmitChanges();
                    var map = mapper.Map<IEnumerable<SUBDEPTODTOs>>(Entidad);
                    var response = responses.RespUdate(map) ;
                    return await Task.FromResult(   Request.CreateResponse(HttpStatusCode.OK, response));


                }


            }
            catch (Exception e)
            {
                log.Error("error en el proceso PUT: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e);

            }


        }

        // DELETE: api/SubDepartamentos/5

        [HttpDelete]
       [Authorize]
        public IHttpActionResult Delete([FromUri]int Codigo)
        {
            try
            {
                var Departamento = db.SUBDEPTO.FirstOrDefault(e => e.CODIGO_SUBDEPTO == Codigo);
                if (Departamento == null)
                {
                    return Ok("please complete the code or SubGrupo NO exist:(" + Codigo + ")");
                }
                else
                {
                    db.SUBDEPTO.DeleteOnSubmit(Departamento);
                    db.SubmitChanges();
                    return Ok("SubDepartamento Eliminado");
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso DELETE: " + e);
                // var f =new EmailClient.SendEmail(" Reporte de error", e.Message, log.Path);

                return NotFound();
            }
        }
    }


}

