﻿using AutoMapper;
using Infraestructura.DTOs;
using MBS_API.Areas.Repositorio;
using MBS_API.AutoMapper;
using MBS_API.DB;
using MBS_API.Models.LogManager;
using MBS_API.Models.Pagination;
using MBS_API.Models.Pagination.Interface;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MBS_API.Controllers
{
    public class AlmacenesController : ApiController
    {

        private MBSDataClassesDataContext db;

        // EmailClient nr = new EmailClient();
        private readonly IMapper mapper;  
        private readonly Log log;
        public readonly IResponses responses;
        public readonly Repositorio repositorio;
        public AlmacenesController()
        {
            db = new MBSDataClassesDataContext();
            mapper = AutoMapperConfig.Mapper;
            log = new Log();
            responses = new Responses<object>();
            repositorio = new Repositorio();
    }
        [HttpGet]
        public async Task<HttpResponseMessage> GET([FromUri] FilterParameter filterParameter)
        {

            try
            {
                var almacenes = (from e in db.ALMACENE
                                 where (SqlMethods.Like(e.ALMACEN.ToString(), '%' + filterParameter.Search + '%') ||
                                        SqlMethods.Like(e.COD_TIPO_ALMACEN.ToString(), '%' + filterParameter.Search + '%') ||
                                        SqlMethods.Like(e.TELEFONO.Trim(), '%' + filterParameter.Search + '%') ||
                                        SqlMethods.Like(e.dir.Trim(), '%' + filterParameter.Search + '%') ||
                                        SqlMethods.Like(e.descprefalmacen.Trim(), '%' + filterParameter.Search + '%') ||
                                        SqlMethods.Like(e.DESC_ALMACEN.Trim(), '%' + filterParameter.Search + '%') ||
                                        SqlMethods.Like(e.clv_vend.Trim(), '%' + filterParameter.Search + '%') ||
                                        SqlMethods.Like(e.ESTATUS.ToString(), '%' + filterParameter.Search + '%'))
                                 orderby e.ALMACEN
                                 select e);
                almacenes.GroupBy(e => e.ALMACEN).FirstOrDefault();
                var Pagealmacenes = await Task.FromResult(PageList<ALMACENE>.Create(almacenes, filterParameter.pageNumber, filterParameter.PageSize));             
                var map = mapper.Map<IEnumerable<ALMACENESDTOs>>(Pagealmacenes);
                var Responses = responses.RespGet(Pagealmacenes, "Almacen", filterParameter.Search, map, Pagealmacenes.metadata()); return Request.CreateResponse(HttpStatusCode.OK, Responses);


            }
            catch (Exception e)
            {

                log.Error("error en el proceso GET: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }



        }
        [HttpPost]
        [Authorize]
        public IHttpActionResult Post([FromBody] ALMACENESDTOs Value)
        {

            try
            {
                if (Value == null)
                {
                    var Information = new
                    {
                        informacion = "configuration error the entity cannot be downloaded please complete the information "
                    };

                    return Ok(Information.ToString());

                }
                else
                {

                    var map = mapper.Map<ALMACENE>(Value);
                    db.ALMACENE.InsertOnSubmit(map);
                    db.SubmitChanges();
                    return Ok("ALMACEN  Insertado");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }
        }
        [HttpPut]
        [Authorize]
        // PUT: api/api/GROUP
        public async Task< HttpResponseMessage> Put([FromUri]int codigo, [FromBody] ALMACENESDTOs values)
        {

            try
            {
                var Entidad = db.ALMACENE.FirstOrDefault(e => e.ALMACEN == codigo);
                if (Entidad == null || values == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                                "field empty please complete the code or code no exist");
                }
                else
                {

                    var rp = repositorio.AlmacenUpdate(Entidad, values);

                    var map = mapper.Map<ALMACENESDTOs>(Entidad);
                    responses.RespUdate(map);
                    return await Task.FromResult(   Request.CreateResponse(HttpStatusCode.OK, responses));


                }


            }
            catch (Exception e)
            {
                log.Error("error en el proceso PUT: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e);

            }


        }

        [HttpDelete]
        [Authorize]
        public IHttpActionResult Delete([FromUri]short Codigo)
        {
            try
            {
                var almacenes = db.ALMACENE.FirstOrDefault(e => e.ALMACEN == Codigo);
                if (almacenes == null)
                {
                    return Ok("please complete the code or NUM_ALMACENE exist:(" + Codigo + ")");
                }
                else
                {
                    db.ALMACENE.DeleteOnSubmit(almacenes);
                    db.SubmitChanges();
                    return Ok("ALMACENE Eliminado");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }

        }
    }
}