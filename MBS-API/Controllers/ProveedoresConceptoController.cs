﻿using AutoMapper;
using Infraestructura.DTOs;
using MBS_API.AutoMapper;
using MBS_API.DB;
using MBS_API.Models.LogManager;
using MBS_API.Models.Pagination;
using MBS_API.Models.Pagination.Interface;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MBS_API.Controllers
{



    public class ProveedoresConceptoController : ApiController
    {
        private MBSDataClassesDataContext db;
  
        // EmailClient nr = new EmailClient();
        private readonly IMapper mapper; 
        private readonly Log log;
        public readonly IResponses responses;
    
        public ProveedoresConceptoController()
        {

           
             db = new MBSDataClassesDataContext();
            mapper = AutoMapperConfig.Mapper;
            log = new Log();
            responses = new Responses<object>();
        }
        [HttpGet]
        public async Task<HttpResponseMessage> GET([FromUri] FilterParameter filterParameter)
        {
            try
            {
                var conceptoproveedor = (from  e in db.CONP01
                                         where (SqlMethods.Like(e.NUM_CPTO.ToString(), '%' + filterParameter.Search + '%') ||
                                                SqlMethods.Like(e.TIPO.Trim(), '%' + filterParameter.Search + '%') ||
                                                SqlMethods.Like(e.categoria.Trim(), '%' + filterParameter.Search + '%') ||
                                                SqlMethods.Like(e.CUEN_CONT.Trim(), '%' + filterParameter.Search + '%') ||
                                                SqlMethods.Like(e.CON_REFER.Trim(), '%' + filterParameter.Search + '%'))
                                         orderby e.NUM_CPTO
                                         select e).DefaultIfEmpty();
                conceptoproveedor.GroupBy(e => e.NUM_CPTO).FirstOrDefault();
                var Pageconceptoproveedor = await Task.FromResult(PageList<CONP01>.Create(conceptoproveedor, filterParameter.pageNumber, filterParameter.PageSize));
                var map = mapper.Map<IEnumerable<CONP01DTOs>>(Pageconceptoproveedor);
                var Responses = responses.RespGet(Pageconceptoproveedor, "Concepto clientes", filterParameter.Search, map, Pageconceptoproveedor.metadata()); return Request.CreateResponse(HttpStatusCode.OK, Responses);

               
            }
            catch (Exception e)
            {

                log.Error("error en el proceso GET: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }



        }
        [HttpPost]
        [Authorize]
        public IHttpActionResult Post([FromBody] CONP01DTOs Value)
        {

            try
            {
                if (Value == null)
                {
                    var Information = new
                    {
                        informacion = "configuration error the entity cannot be downloaded please complete the information "
                    };

                    return Ok(Information.ToString());

                }
                else
                {
                    var map = mapper.Map<CONP01>(Value);
                    db.CONP01.InsertOnSubmit(map);
                    db.SubmitChanges();
                    return Ok("Proveedores Concepto Insertado");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }
        }
        [HttpPut]
        [Authorize]
        // PUT: api/api/GROUP
        public async Task< HttpResponseMessage> Put([FromUri]int codigo, [FromBody] CONP01DTOs values)
        {

            try
            {
                var Entidad = db.CONP01.FirstOrDefault(e => e.NUM_CPTO == codigo);
                if (Entidad == null || values == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                                "field empty please complete the code or code no exist");
                }
                else
                {
                    Entidad.NUM_REG = values.NUM_REG;
                    Entidad.DESCR = values.DESCR;
                    Entidad.TIPO = values.TIPO;
                    Entidad.CUEN_CONT = values.CUEN_CONT;
                    Entidad.CON_REFER = values.CON_REFER;
                    Entidad.GEN_CPTO = values.GEN_CPTO;
                    Entidad.T_CRED = values.T_CRED;
                    Entidad.categoria = values.Categoria;

                    db.SubmitChanges();
                    var map = mapper.Map<CONP01DTOs>(Entidad);
                    var response =responses.RespUdate(map) ;
                    return await Task.FromResult(   Request.CreateResponse(HttpStatusCode.OK, response));


                }
            }
            catch (Exception e)
            {
                log.Error("error en el proceso PUT: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e);

            }


        }

        [HttpDelete]
        [Authorize]
        public IHttpActionResult Delete([FromUri]int Codigo)
        {
            try
            {
                var conceptoproveedor = db.CONP01.FirstOrDefault(e => e.NUM_CPTO == Codigo);
                if (conceptoproveedor == null)
                {
                    return Ok("please complete the code or NUM_CPTO NO exist:(" + Codigo + ")");
                }
                else
                {
                    db.CONP01.DeleteOnSubmit(conceptoproveedor);
                    db.SubmitChanges();
                   
                    return Ok("concepto  clientes Eliminado");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }

        }
    }
}
