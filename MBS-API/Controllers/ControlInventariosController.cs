﻿using AutoMapper;
using Infraestructura.DTOs;
using MBS_API.Areas.Repositorio;
using MBS_API.AutoMapper;
using MBS_API.DB;
using MBS_API.Models.LogManager;
using MBS_API.Models.Pagination;
using MBS_API.Models.Pagination.Interface;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;



namespace MBS_API.Controllers
{
    public class ControlInventariosController : ApiController
    {


        private MBSDataClassesDataContext db;

        // EmailClient nr = new EmailClient();
        private readonly IMapper mapper; 
        private readonly Log log;
        private readonly IResponses responses;
        private readonly Repositorio repositorio;

        public ControlInventariosController()
        {
            db = new MBSDataClassesDataContext();
            mapper = AutoMapperConfig.Mapper;
            log = new Log();
            responses = new Responses<object>();
            repositorio = new Repositorio();
        }
        [HttpGet]
        //Get/api/control invetario
        public async Task<HttpResponseMessage> GET([FromUri] FilterParameter filterParameter)
        {
            try
            {
                var control = (from e in db.MULT01
                               where (SqlMethods.Like(e.STATUS.Trim(), '%' + filterParameter.Search + '%') ||
                                      SqlMethods.Like(e.ALMACEN.ToString(), '%' + filterParameter.Search + '%') ||
                                      SqlMethods.Like(e.STOCK_MIN.ToString(), '%' + filterParameter.Search + '%') ||
                                      SqlMethods.Like(e.STOCK_MIN.ToString(), '%' + filterParameter.Search + '%') ||
                                      SqlMethods.Like(e.COMP_X_REC.ToString(), '%' + filterParameter.Search + '%') ||
                                      SqlMethods.Like(e.PASILLOs.Trim(), '%' + filterParameter.Search + '%') ||
                                      SqlMethods.Like(e.lado.Trim(), '%' + filterParameter.Search + '%') ||
                                      SqlMethods.Like(e.apart.ToString(), '%' + filterParameter.Search + '%') ||
                                      SqlMethods.Like(e.BSECAPTIF.ToString(), '%' + filterParameter.Search + '%') ||
                                      SqlMethods.Like(e.NUM_REG.ToString(), '%' + filterParameter.Search + '%') ||
                                      SqlMethods.Like(e.CLV_ART.Trim(), '%' + filterParameter.Search + '%'))
                               orderby (e.NUM_REG)
                               select e).DefaultIfEmpty();


                control.GroupBy(e => e.ALMACEN).FirstOrDefault();
                var controlpage = await Task.FromResult(PageList<MULT01>.Create(control, filterParameter.pageNumber, filterParameter.PageSize));
                var map = mapper.Map<IEnumerable<MULT01DTOs>>(controlpage);
                var Responses = responses.RespGet(controlpage, "Movimiento de inventario", filterParameter.Search, map, controlpage.metadata());
                return Request.CreateResponse(HttpStatusCode.OK, Responses);



            }
            catch (Exception e)
            {
                log.Error("error en el proceso Get: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);

            }

        }
        [HttpPost]
        // POST: api/control inventario
        [Authorize]
        public IHttpActionResult Post([FromBody] MULT01DTOs value)
        {
            try
            {
                if (value == null)
                {
                    var Information = new
                    {
                        informacion = "configuration error the entity cannot be downloaded please complete the information "
                    };

                    return Ok(Information.ToString());

                }
                else
                { var map = mapper.Map<MULT01>(value);
                      db.MULT01.InsertOnSubmit(map);
                      db.SubmitChanges();                   

                    return Ok("control Insertado");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }

        }

        // PUT: api/controlInventario
        [HttpPut]
        [Authorize]
        public async Task<HttpResponseMessage> Put([FromUri] string codigo, [FromBody] MULT01DTOs Values)
        {
            try
            {
                var Entidad = db.MULT01.FirstOrDefault(e => e.CLV_ART == codigo);
                if (Entidad == null || Values == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                            "field empty please complete the code");
                }
                else
                {

                    var rp = repositorio.ControlInvetarioUpdate(Entidad, Values);
                    var map = mapper.Map<MULT01DTOs>(rp);
                    var response = responses.RespUdate(map);
                    return await Task.FromResult( Request.CreateResponse(HttpStatusCode.OK, response));
                    
                }

            }
            catch (Exception e)
            {
                log.Error("error en el proceso PUT: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e);
            }
        }





    }
}

