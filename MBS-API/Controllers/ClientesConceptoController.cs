﻿using AutoMapper;
using Infraestructura.DTOs;
using MBS_API.Areas.Repositorio;
using MBS_API.AutoMapper;
using MBS_API.DB;
using MBS_API.Models.LogManager;
using MBS_API.Models.Pagination;
using MBS_API.Models.Pagination.Interface;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MBS_API.Controllers
{
    public class ClientesConceptoController : ApiController
    {


        private MBSDataClassesDataContext db;

        // EmailClient nr = new EmailClient();
        private readonly IMapper mapper; 
        private readonly Log log;
        private readonly IResponses responses;
        private readonly Repositorio repositorio;
       
        public ClientesConceptoController()
        {
            db = new MBSDataClassesDataContext();
            mapper = AutoMapperConfig.Mapper;
            log = new Log();
            responses = new Responses<object>();
            repositorio = new Repositorio();
        }
        [HttpGet]
        public async Task<HttpResponseMessage> GET([FromUri] FilterParameter filterParameter)
        {

            try
            {
                var clientesconcep = (from e in db.CONC01
                                      where (SqlMethods.Like(e.NUM_CPTO.ToString(), '%' + filterParameter.Search + '%') ||
                                             SqlMethods.Like(e.NC.Trim(), '%' + filterParameter.Search + '%') ||
                                             SqlMethods.Like(e.CATEGORIA.Trim(), '%' + filterParameter.Search + '%') ||
                                             SqlMethods.Like(e.CUEN_CONT.Trim(), '%' + filterParameter.Search + '%')||
                                             SqlMethods.Like(e.DESCR.Trim(), '%' + filterParameter.Search + '%'))
                                      orderby (e.NUM_CPTO)
                                      select e).DefaultIfEmpty();

                clientesconcep.GroupBy(e => e.NUM_CPTO).FirstOrDefault();
                var pageclientesconcep = await Task.FromResult(PageList<CONC01>.Create(clientesconcep, filterParameter.pageNumber, filterParameter.PageSize));
                var map = mapper.Map<IEnumerable<CONC01DTOs>>(pageclientesconcep);
                var Responses = responses.RespGet(pageclientesconcep, " Concepto clientes", filterParameter.Search, map, pageclientesconcep.metadata()); 
                return Request.CreateResponse(HttpStatusCode.OK, Responses);
               
            }
            catch (Exception e)
            {

                log.Error("error en el proceso GET: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }
        [HttpPost]
        [Authorize]
        public IHttpActionResult Post([FromBody] CONC01DTOs Value)
        {

            try
            {
                if (Value == null)
                {
                    var Information = new
                    {
                        informacion = "configuration error the entity cannot be downloaded please complete the information "
                    };

                    return Ok(Information.ToString());

                }
                else
                {
                    var map = mapper.Map<CONC01>(Value);
                    db.CONC01.InsertOnSubmit(map);
                    db.SubmitChanges();
                    return Ok("Cliente Concepto Insertado");
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }





        }
        [HttpPut]
        [Authorize]
        // PUT: api/api/GROUP
        public async Task< HttpResponseMessage> Put([FromUri] int codigo, [FromBody] CONC01DTOs values)
        {

            try
            {
                var Entidad = db.CONC01.FirstOrDefault(e => e.NUM_CPTO == codigo);
                if (Entidad == null || values == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                                "field empty please complete the code or code no exist");
                }
                else
                {


                    var rp = repositorio.ClienteConcepUpdate(Entidad, values);
                    var map = mapper.Map<IEnumerable<CONC01DTOs>>(rp);
                    var response = responses.RespUdate(map);                  
                    return await Task.FromResult(   Request.CreateResponse(HttpStatusCode.OK, response));


                }


            }
            catch (Exception e)
            {
                log.Error("error en el proceso PUT: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e);

            }


        }

        [HttpDelete]
        [Authorize]
        public IHttpActionResult Delete([FromUri] int Codigo)
        {
            try
            {
                var clientesconcep = db.CONC01.FirstOrDefault(e => e.NUM_CPTO == Codigo);
                if (clientesconcep == null)
                {
                    return Ok("please complete the code or NUM_CPTO NO exist:(" + Codigo + ")");
                }
                else
                {
                    db.CONC01.DeleteOnSubmit(clientesconcep);
                    db.SubmitChanges();
                    return Ok("concepto  clientes Eliminado");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }

        }











    }
}