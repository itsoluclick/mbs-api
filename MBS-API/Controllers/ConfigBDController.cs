﻿using MBS_API.DB;
using MBS_API.Models.Config;
using MBS_API.Models.LogManager;
using System;
using System.Web.Http;

namespace MBS_API.Controllers
{
    public class ConfigBDController : ApiController
    {

        private MBSDataClassesDataContext db;

        // EmailClient nr = new EmailClient();

        private readonly Log log;
        private readonly ConfigBD configBD;

        public ConfigBDController()
        {
            db = new MBSDataClassesDataContext();
            configBD = new ConfigBD();
            log = new Log();
        }

        [HttpPost]
       [Authorize]
        // post: api/ ConfigBD(ID)
        public IHttpActionResult Post([FromUri]string IDConfig)
        {

            try
            {
                if (string.IsNullOrEmpty(IDConfig))
                {

                    var Information = new
                    {
                        informacion = "configuration error the entity cannot be downloaded please complete the information............! "
                    };
                    return Ok(Information.ToString());
                }
                else if (IDConfig == "IT@2012".ToUpper())
                {
                    configBD.ConfiguracionTabla();
                    var _BD =   configBD.CONFIGURACIONDB();
                    var _IDEN =  configBD.CONFIGURACIONDBIDEN();                


                    if (_BD == 1 || _IDEN == 1)
                    {
                        return Ok("Configuracion Realizada con exito..........................................!");
                    }
                    else
                    {
                        return Ok("No hay tabla para configurar.....................................................!");
                        
                    }

                }
                else
                { 
                    return Ok("Codigo incorrecto vuelva intental o poner en contacto  con el administrador....................!");

                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e);
                log.Error("error en el proceso Post: " + e);
                return NotFound();
            }




        }
       
    }  
   

    }

    

