﻿using AutoMapper;
using Infraestructura.DTOs;
using MBS_API.Areas.Repositorio;
using MBS_API.AutoMapper;
using MBS_API.DB;
using MBS_API.Models.LogManager;
using MBS_API.Models.Pagination;
using MBS_API.Models.Pagination.Interface;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MBS_API.Controllers
{
    //[Authorize] //Necesita token para aceder a este recurso
    public class ClientesController : ApiController
    {
        private MBSDataClassesDataContext db;

        // EmailClient nr = new EmailClient();
        private readonly IMapper mapper; 
        private readonly Log log;
        private readonly IResponses responses;
        private readonly Repositorio repositorio;
        public ClientesController()
        {
            db = new MBSDataClassesDataContext();
            mapper = AutoMapperConfig.Mapper;
            log = new Log();
            responses = new Responses<object>();
            repositorio = new Repositorio();
        }

        [HttpGet]
        public async Task<HttpResponseMessage> GET([FromUri] FilterParameter filterParameter)
        {

            try
            {
                var Clientes = (from cliente in db.CLIE01
                                where (SqlMethods.Like(cliente.CCLIE.Trim(), '%' + filterParameter.Search + '%') ||
                                SqlMethods.Like(cliente.NOMBRE.Trim(), '%' + filterParameter.Search + '%') ||
                                SqlMethods.Like(cliente.RFC.Trim(), '%' + filterParameter.Search + '%') ||
                                SqlMethods.Like(cliente.DIR.Trim(), '%' + filterParameter.Search + '%') ||
                                SqlMethods.Like(cliente.TELEFONOS.Trim(), '%' + filterParameter.Search + '%') ||
                                SqlMethods.Like(cliente.TELEFONO.Trim(), '%' + filterParameter.Search + '%') ||
                                SqlMethods.Like(cliente.CODIGO.Trim(), '%' + filterParameter.Search + '%'))
                                orderby (cliente.NUM_REG)
                                select cliente).DefaultIfEmpty();

                Clientes.GroupBy(e => e.CCLIE).FirstOrDefault();

                var Clientepage = await Task.FromResult(PageList<CLIE01>.Create(Clientes, filterParameter.pageNumber, filterParameter.PageSize));
                var map = mapper.Map<IEnumerable<CLIE01DTOs>>(Clientepage);
                var Responses = responses.RespGet(Clientepage, "Customers", filterParameter.Search, map, Clientepage.metadata()); 
                return Request.CreateResponse(HttpStatusCode.OK, Responses);

            }

            catch (Exception e)
            {
                log.Error("error en el proceso GET: " + e);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }





        }

        
        [HttpPost]
        [Authorize]
        // POST: api/Productos
        public IHttpActionResult Post([FromBody] CLIE01DTOs values)
        {

            try
            {
                if (values == null)
                {
                    var Information = new
                    {
                        informacion = "configuration error the entity cannot be downloaded please complete the information "
                    };

                    return Ok(Information.ToString());

                }
                else
                {
                    var map = mapper.Map<CLIE01>(values);
                    db.CLIE01.InsertOnSubmit(map);
                    db.SubmitChanges();
                    return Ok("Cliente Insertado");
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                log.Error("error en el proceso POST: " + e);
                return NotFound();

            }





        }
        [HttpPut]
        [Authorize]
        // PUT: api/Productos
        public async Task< HttpResponseMessage> Put([FromUri] string Codigo, [FromBody] CLIE01DTOs Values)
        {
            try
            {
                
                   var Entidad = db.CLIE01.FirstOrDefault(x => x.CCLIE == Codigo);
                if (Entidad == null || Values == null)
                {
                    
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                    "Customers  not found to update");
                    
                }
                else
                {
                    var rp = repositorio.ClienteUpdate(Entidad, Values);
                    var map = mapper.Map<CLIE01DTOs>(rp);
                    var response = responses.RespUdate(map) ;
                    return await Task.FromResult(   Request.CreateResponse(HttpStatusCode.OK, response));

                }
            }
            catch (Exception e)
            {
                log.Error("error en el proceso PUT: " + e.Message);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e);
            }
        }
        // DELETE: api/Clientes/5

        [HttpDelete]
        [Authorize]
        public IHttpActionResult Delete([FromUri] string Codigo)
        {
            try
            {
                if (string.IsNullOrEmpty(Codigo))
                {
                    var Information = new
                    {
                        informacion = "empty field,please complete the code"
                    };

                    return Ok(Information);

                }
                else
                {
                    var Clientes = db.CLIE01.FirstOrDefault(e => e.CCLIE.Trim() == Codigo);                       

                    if (Clientes == null)
                    {
                        return Ok("please complete the code or Cliente NO exist:(" + Codigo + ")");
                    }
                    else
                    {
                        db.CLIE01.DeleteOnSubmit(Clientes);
                        db.SubmitChanges();
                        return Ok("Clientes Eliminado");

                    }
                }
            }
            catch (Exception e)
            {
                log.Error("error en el proceso DELETE: " + e);
                return NotFound();

            }


        }
    }
}
