﻿using Infraestructura.DTOs;
using MBS_API.DB;

namespace MBS_API.Areas.Repositorio
{

    public class Repositorio
    {
        private MBSDataClassesDataContext db;
        public Repositorio()
        {
            db = new MBSDataClassesDataContext();
        }
        public PROV01 ProveedoresUpdate(DB.PROV01 Entidad, PROV01DTOs Values)
            { Entidad.NUM_REG = Values.NUM_REG;
                   // Entidad.CPROV = Values.CPROV;
                    Entidad.STATUS = Values.STATUS;
                    Entidad.NOMBRE = Values.NOMBRE;
                    Entidad.RFC = Values.RFC;
                    Entidad.DIR = Values.DIR;
                    Entidad.POB = Values.POB;
                    Entidad.CODIGO = Values.CODIGO;
                    Entidad.TELEFONO = Values.TELEFONO;
                    Entidad.ATENCION = Values.ATENCION;
                    Entidad.ATEN_COB = Values.ATEN_COB;
                    Entidad.REV_PAG = Values.REV_PAG;
                    Entidad.CLASIFIC = Values.CLASIFIC;
                    Entidad.DIAS_CRE = Values.DIAS_CRE;
                    Entidad.FCH_ULTCOM = Values.FCH_ULTCOM;
                    Entidad.VEND = Values.VEND;
                    Entidad.DESCUENTO = Values.DESCUENTO;
                    Entidad.LIM_CRED = Values.LIM_CRED;
                    Entidad.SALDO = Values.SALDO;
                    Entidad.VTAS = Values.VTAS;
                    Entidad.OBSER = Values.OBSER;
                    Entidad.COLONIA = Values.COLONIA;
                    Entidad.FAX = Values.FAX;
                    Entidad.EMAIL = Values.EMAIL;
                    Entidad.CURP = Values.CURP;
                    Entidad.CVE_ZONA = Values.CVE_ZONA;
                    Entidad.TELEFONOS = Values.TELEFONOS;
                    Entidad.ULT_PAGOD = Values.ULT_PAGOD;
                    Entidad.ULT_VENTD = Values.ULT_VENTD;
                    Entidad.CAMLIBRE1 = Values.CAMLIBRE1;
                    Entidad.CAMLIBRE2 = Values.CAMLIBRE2;
                    Entidad.CAMLIBRE3 = Values.CAMLIBRE3;
                    Entidad.ULTPAGOF = Values.ULTPAGOF;
                    Entidad.RETXFLETE = Values.RETXFLETE;
                    Entidad.CAMLIBRE4 = Values.CAMLIBRE4;
                    Entidad.PORCRETEN = Values.PORCRETEN;
                    Entidad.CAMLIBRE5 = Values.CAMLIBRE5;
                    Entidad.ULT_PAGOM = Values.ULT_PAGOM;
                    Entidad.ULT_VENTM = Values.ULT_VENTM;
                    Entidad.CAMLIBRE6 = Values.CAMLIBRE6;
            db.SubmitChanges();
            return Entidad;
                   

            }
    public MINV01 MovimientoInventarioUpdate (DB.MINV01 Entidad, MINV01DTOs Values)
            {
                    Entidad.CLV_ART = Values.CLV_ART;
                    Entidad.TIPO_MOV = Values.TIPO_MOV;
                    Entidad.FECHA_DOCU = Values.FECHA_DOCU;
                    Entidad.REFER = Values.REFER;
                    Entidad.CLAVE_CLPV = Values.CLAVE_CLPV;
                    Entidad.VEND = Values.VEND;
                    Entidad.CANT = Values.CANT;
                    Entidad.CANT_COST = Values.CANT_COST;
                    Entidad.PRECIO = Values.PRECIO;
                    Entidad.COSTO = Values.COSTO;
                    Entidad.ALMACEN = Values.ALMACEN;
                    Entidad.AFEC_COI = Values.AFEC_COI;
                    Entidad.OBS_MINV = Values.OBS_MINV;
                    Entidad.REG_SERIE = Values.REG_SERIE;
                    Entidad.UNI_VENTA = Values.UNI_VENTA;
                    Entidad.U4SEC = Values.U4SEC;
                    Entidad.DB8EXIST = Values.DB8EXIST;
                    Entidad.TIPO_PROD = Values.TIPO_PROD;
                    Entidad.FACTOR_CON = Values.FACTOR_CON;
                    Entidad.FECHAELAB = Values.FECHAELAB;
                    Entidad.USUARIO = Values.USUARIO;
                    Entidad.M_FLETE = Values.M_FLETE;
                    Entidad.DOCUMENTO = Values.DOCUMENTO;
                    Entidad.OBSERVACION = Values.OBSERVACION;
                    /*
                    Entidad.ALMACEN_DESTINO = Values.ALMACEN_DESTINO;
                    Entidad.TIPO = Values.TIPO;
                    Entidad.ALMACEN_ORIGEN = Values.ALMACEN_ORIGEN;
                    Entidad.CTLPOL = Values.CTLPOL;
                    Entidad.CVEFOLIO = Values.CVEFOLIO;
                    Entidad.CLV_ART_EQUI = Values.CLV_ART_EQUI;
                    Entidad.descr_EQUI = Values.descr_EQUI;
                    Entidad.NOMBRE = Values.NOMBRE;
                    Entidad.DIRECCION = Values.DIRECCION;
                    Entidad.TELEFONO = Values.TELEFONO;
                    Entidad.CONTACTO = Values.CONTACTO;
                    Entidad.EOS = Values.EOS;
                    Entidad.COP = Values.COP;
                    Entidad.Referencia = Values.Referencia;
                    Entidad.Referencia2 = Values.Referencia2;
                    Entidad.almacen2 = Values.almacen2;
                    Entidad.documento2 = Values.documento2;
                    Entidad.documento3 = Values.documento3;
                    Entidad.PXT = Values.PXT;
                    */
                    Entidad.contabilizado = Values.contabilizado;
            db.SubmitChanges();
            return Entidad;
            }
        public IMPU01 ImpuestoUpdate(DB.IMPU01 Entidad, IMPU01DTOs values)
            {         Entidad.NUM_REG = values.NUM_REG;
                  //  Entidad.CVEESQIMPU = values.CVEESQIMPU;
                    Entidad.DESCRIPESQ = values.DESCRIPESQ;
                    Entidad.IMPUESTO1 = values.IMPUESTO1;
                    Entidad.IMP1APLICA = values.IMP1APLICA;
                    Entidad.IMPUESTO2 = values.IMPUESTO2;
                    Entidad.IMP2APLICA = values.IMP2APLICA;
                    Entidad.IMPUESTO3 = values.IMPUESTO3;
                    Entidad.IMP3APLICA = values.IMP3APLICA;
                    Entidad.IMPUESTO4 = values.IMPUESTO4;
                    Entidad.IMP4APLICA = values.IMP4APLICA;
            db.SubmitChanges();
            return Entidad;
            }
        public EMPRESAS EMPRESASUpdate(DB.EMPRESAS Entidad, EMPRESASDTOs Values)
        {
            Entidad.COD_EMPRESA = Values.COD_EMPRESA;
            Entidad.NOMBRE_EMPRESA = Values.NOMBRE_EMPRESA;
            Entidad.CDKEY = Values.CDKEY;
            Entidad.ESTATUS = Values.ESTATUS;
            Entidad.REPORTE_FACTURA = Values.REPORTE_FACTURA;
            Entidad.REPORTE_CODBARRA = Values.REPORTE_CODBARRA;
            Entidad.REPORTE_FACTURA_CONTADO = Values.REPORTE_FACTURA_CONTADO;
            Entidad.REPORTE_FACTURA_CREDITO = Values.REPORTE_FACTURA_CREDITO;
            Entidad.CVEESQIMPU = Values.CVEESQIMPU;
            Entidad.SEG_FLETE = Values.SEG_FLETE;
            Entidad.SERVIDOR_SMTP = Values.SERVIDOR_SMTP;
            Entidad.PUERTO_SMTP = Values.PUERTO_SMTP;
            Entidad.AUTENTIFICACION_SMTP = Values.AUTENTIFICACION_SMTP;
            Entidad.EAN13HEADER = Values.EAN13HEADER;
            Entidad.RNC = Values.RNC;
            Entidad.SERVIDOR = Values.SERVIDOR;
            Entidad.NOMBRE_EMPRESAS = Values.NOMBRE_EMPRESAS;
            Entidad.DIRECCION = Values.DIRECCION;
            Entidad.TELEFONO = Values.TELEFONO;
            Entidad.FAX = Values.FAX;
            Entidad.CORREO = Values.CORREO;
            Entidad.OBSERVACION1 = Values.OBSERVACION1;
            Entidad.OBSERVACION2 = Values.OBSERVACION2;
            Entidad.OBSERVACION3 = Values.OBSERVACION3;
            Entidad.FACTURASINEXISTENCIAS = Values.FACTURASINEXISTENCIAS;
            Entidad.CLAVE_AUTORIZA = Values.CLAVE_AUTORIZA;
            Entidad.UDL = Values.UDL;
            Entidad.FORMATO = Values.FORMATO;
            Entidad.TIPO_NEGOCIO = Values.TIPO_NEGOCIO;
            Entidad.PUNTO_VENTA = Values.PUNTO_VENTA;
            Entidad.FACTUR_INVE = Values.FACTUR_INVE;
            Entidad.Supertmarket = Values.Supertmarket;
            Entidad.factrapida = Values.factrapida;
            Entidad.cdkey1 = Values.cdkey1;
            Entidad.ALMACEN_DEV = Values.ALMACEN_DEV;
            Entidad.nombre_comercial = Values.nombre_comercial;
            Entidad.NCF_GASTO_MENOR_AUT = Values.NCF_GASTO_MENOR_AUT;
            Entidad.CONFIRMA_ORDEN = Values.CONFIRMA_ORDEN;
            Entidad.OBSDEVOLUCION = Values.OBSDEVOLUCION;
            Entidad.BAL_IDENT = Values.BAL_IDENT;
            Entidad.stopprecioitbis = Values.stopprecioitbis;
            Entidad.Capturardatosencfact = Values.Capturardatosencfact;
            Entidad.FACT_REQ_VEND = Values.FACT_REQ_VEND;
            Entidad.PREGUNTA_IMPRIME = Values.PREGUNTA_IMPRIME;
            Entidad.DIV_PESO = Values.DIV_PESO;
            Entidad.FACTURA_HOLD = Values.FACTURA_HOLD;
            Entidad.udl2 = Values.udl2;
            Entidad.tipo_secuencia_factura = Values.tipo_secuencia_factura;
            Entidad.CLIENTELIBRE1 = Values.CLIENTELIBRE1;
            Entidad.CLIENTELIBRE2 = Values.CLIENTELIBRE2;
            Entidad.CLIENTELIBRE3 = Values.CLIENTELIBRE3;
            Entidad.CLIENTELIBRE4 = Values.CLIENTELIBRE4;
            Entidad.CLIENTELIBRE5 = Values.CLIENTELIBRE5;
            Entidad.CLIENTELIBRE6 = Values.CLIENTELIBRE6;
            Entidad.INVENTARIOLIBRE1 = Values.INVENTARIOLIBRE1;
            Entidad.INVENTARIOLIBRE2 = Values.INVENTARIOLIBRE2;
            Entidad.INVENTARIOLIBRE3 = Values.INVENTARIOLIBRE3;
            Entidad.INVENTARIOLIBRE4 = Values.INVENTARIOLIBRE4;
            Entidad.INVENTARIOLIBRE5 = Values.INVENTARIOLIBRE5;
            Entidad.INVENTARIOLIBRE6 = Values.INVENTARIOLIBRE6;
            Entidad.SUPERVISION = Values.SUPERVISION;
            Entidad.MuestraProducto = Values.MuestraProducto;
            Entidad.Fecha_fact_abierta = Values.Fecha_fact_abierta;
            Entidad.VENCIMIENTO = Values.VENCIMIENTO;
            Entidad.SEGURO = Values.SEGURO;
            Entidad.AbrirSoloUnaVez = Values.AbrirSoloUnaVez;
            Entidad.PREFERENCIAL = Values.PREFERENCIAL;
            Entidad.PesosxPuntos = Values.PesosxPuntos;
            Entidad.Control_Minimo = Values.Control_Minimo;
            Entidad.Maneja_Ofertas = Values.Maneja_Ofertas;
            Entidad.IMPRIME_PEDIDO = Values.IMPRIME_PEDIDO;
            Entidad.observacion4 = Values.observacion4;
            Entidad.observacion5 = Values.observacion5;
            Entidad.observacion6 = Values.observacion6;
            Entidad.observacion7 = Values.observacion7;
            Entidad.observacion9 = Values.observacion9;
            Entidad.observacion10 = Values.observacion10;
            Entidad.observacion11 = Values.observacion11;
            Entidad.observacion12 = Values.observacion12;
            Entidad.observacion13 = Values.observacion13;
            Entidad.observacion14 = Values.observacion14;
            Entidad.observacion15 = Values.observacion15;
            Entidad.observacion8 = Values.observacion8;
            Entidad.defectobarrazebra = Values.defectobarrazebra;
            Entidad.PromoSiempre = Values.PromoSiempre;
            Entidad.descuentoPref_x_Monto = Values.descuentoPref_x_Monto;
            Entidad.ExistProdNuevo = Values.ExistProdNuevo;
            Entidad.Precio_Almacen = Values.Precio_Almacen;
            Entidad.cant_sugerida = Values.cant_sugerida;
            Entidad.CantidadXcada = Values.CantidadXcada;
            Entidad.Convenio = Values.Convenio;
            Entidad.valida_nota = Values.valida_nota;
            Entidad.PuntoConAutorizacion = Values.PuntoConAutorizacion;
            Entidad.C_Contado = Values.C_Contado;
            Entidad.Factura_desde_Pedido = Values.Factura_desde_Pedido;
            Entidad.SCP = Values.SCP;
            Entidad.FACT_SIN_EXIST_COND = Values.FACT_SIN_EXIST_COND;
            Entidad.Montoadicionartarjeta = Values.Montoadicionartarjeta;
            Entidad.verificasaldo = Values.verificasaldo;
            Entidad.GeneraBoletos = Values.GeneraBoletos;
            Entidad.ValorXBoletos = Values.ValorXBoletos;
            Entidad.promoboletos = Values.promoboletos;
            Entidad.promoboletos2 = Values.promoboletos2;
            Entidad.controlaultcosto = Values.controlaultcosto;
            Entidad.SEC_GASTOS = Values.SEC_GASTOS;
            Entidad.SEC_GASTOMENOR = Values.SEC_GASTOMENOR;
            Entidad.SEC_NFCBANCO = Values.SEC_NFCBANCO;
            Entidad.SEC_REPOSICION = Values.SEC_REPOSICION;
            Entidad.SEC_DEVGASTO = Values.SEC_DEVGASTO;
            Entidad.permitecredito = Values.permitecredito;
            Entidad.color = Values.color;
            Entidad.capturahora = Values.capturahora;
            Entidad.horafactura = Values.horafactura;
            Entidad.valorxboletosmarca = Values.valorxboletosmarca;
            Entidad.CONEXION = Values.CONEXION;
            Entidad.TIPOMOV_LIQ = Values.TIPOMOV_LIQ;
            Entidad.usa_libra_en_producion = Values.usa_libra_en_producion;
            Entidad.PROVEEDORLIBRE1 = Values.PROVEEDORLIBRE1;
            Entidad.PROVEEDORLIBRE2 = Values.PROVEEDORLIBRE2;
            Entidad.PROVEEDORLIBRE3 = Values.PROVEEDORLIBRE3;
            Entidad.PROVEEDORLIBRE4 = Values.PROVEEDORLIBRE4;
            Entidad.PROVEEDORLIBRE5 = Values.PROVEEDORLIBRE5;
            Entidad.PROVEEDORLIBRE6 = Values.PROVEEDORLIBRE6;
            Entidad.almacenvirtual = Values.almacenvirtual;
            Entidad.rutaimagen = Values.rutaimagen;
            Entidad.rutaimagenes = Values.rutaimagenes;
            Entidad.pedidosinexistencias = Values.pedidosinexistencias;
            Entidad.autoriza_user_mov = Values.autoriza_user_mov;
            Entidad.valida_bono = Values.valida_bono;
            Entidad.PRECIO1TITULO = Values.PRECIO1TITULO;
            Entidad.PRECIO2TITULO = Values.PRECIO2TITULO;
            Entidad.PRECIO3TITULO = Values.PRECIO3TITULO;
            Entidad.PRECIO4TITULO = Values.PRECIO4TITULO;
            Entidad.PRECIO5TITULO = Values.PRECIO5TITULO;
            Entidad.TITULOPRECIOCOL = Values.TITULOPRECIOCOL;
            Entidad.TITULOPRECIOITBISCOL = Values.TITULOPRECIOITBISCOL;
            Entidad.modo_gobierno = Values.modo_gobierno;
            Entidad.CONTABILIDAD_EN_LINEA = Values.CONTABILIDAD_EN_LINEA;
            Entidad.CLAVE_INVE_AUTO = Values.CLAVE_INVE_AUTO;
            Entidad.HOLDOTRACAJA = Values.HOLDOTRACAJA;
            Entidad.urlApp = Values.urlApp;
            //Entidad.IDNUM_REG = Values.IDNUM_REG;
            db.SubmitChanges();
            return Entidad;

        }

        public DGII_RNC DGII_RNCUpdate( DB.DGII_RNC Entidad, DGII_RNCDTOs Values)
        {
            //  Entida.dRNC= Values.RNC
            Entidad.EMPRESA = Values.EMPRESA;
            Entidad.EMPRESA2 = Values.EMPRESA2;
            Entidad.ACTIVIDAD = Values.ACTIVIDAD;
            Entidad.CAMPO1 = Values.CAMPO1;
            Entidad.CAMPO2 = Values.CAMPO2;
            Entidad.CAMPO3 = Values.CAMPO3;
            Entidad.CAMPO4 = Values.CAMPO4;
            Entidad.FECHA = Values.FECHA;
            Entidad.NORMAL = Values.NORMAL;
            Entidad.ESTATUS = Values.ESTATUS;
            Entidad.TIPO_NCF = Values.TIPO_NCF;
            db.SubmitChanges();
            return Entidad;
        }
        public CTAS01 CuentasBancosUpdate(DB.CTAS01 Entidad, CTAS01DTOs Values)
            {Entidad.NUM_REG = Values.NUM_REG;
                    Entidad.CVE_CTA = Values.CVE_CTA;
                    Entidad.STATUS = Values.STATUS;
                    Entidad.NUM_CTA = Values.NUM_CTA;
                    Entidad.BANCO = Values.BANCO;
                    Entidad.SUCURSAL = Values.SUCURSAL;
                    Entidad.MONEDA = Values.MONEDA;
                    Entidad.CTA_CONTAB = Values.CTA_CONTAB;
                    Entidad.DIA_CORTE = Values.DIA_CORTE;
                    Entidad.SIG_CHEQUE = Values.SIG_CHEQUE;
                    Entidad.SALDO_INI = Values.SALDO_INI;
                    Entidad.SDO_CONCIL = Values.SDO_CONCIL;
                    Entidad.TOT_CARGOS = Values.TOT_CARGOS;
                    Entidad.TOT_ABONOS = Values.TOT_ABONOS;
                    Entidad.T_CAR_TRAN = Values.T_CAR_TRAN;
                    Entidad.T_ABO_TRAN = Values.T_ABO_TRAN;
                    Entidad.LIM_CRED = Values.LIM_CRED;
                    Entidad.TIPO_CTA = Values.TIPO_CTA;
                    Entidad.FECH_APER = Values.FECH_APER;
                    Entidad.PAGO_MIN = Values.PAGO_MIN;
                    Entidad.CHE_FTO = Values.CHE_FTO;
                    Entidad.BMP_BAN = Values.BMP_BAN;
                    Entidad.CONTACTO = Values.CONTACTO;
                    Entidad.TELEFONO = Values.TELEFONO;
                    Entidad.LEYEN_PST = Values.LEYEN_PST;
                    Entidad.COMISION = Values.COMISION;
                    Entidad.T_ACCIONES = Values.T_ACCIONES;
                    Entidad.CAR_INVERS = Values.CAR_INVERS;
                    Entidad.ABO_INVERS = Values.ABO_INVERS;
                    Entidad.RESTO = Values.RESTO;
            db.SubmitChanges();
            return Entidad;
            }
        public MULT01 ControlInvetarioUpdate(DB.MULT01 Entidad, MULT01DTOs Values)
        { 

                    Entidad.NUM_REG = Values.NUM_REG;
                  
                   // Entidad.CLV_ART = Values.CLV_ART;
                    Entidad.STATUS = Values.STATUS;
                    Entidad.ALMACEN = Values.ALMACEN;
                    Entidad.CTRL_ALM = Values.CTRL_ALM;
                    Entidad.EXIST = Values.EXIST;
                    Entidad.STOCK_MIN = Values.STOCK_MIN;
                    Entidad.STOCK_MAX = Values.STOCK_MAX;
                    Entidad.COMP_X_REC = Values.COMP_X_REC;
                    Entidad.EXISTREAL = Values.EXISTREAL;
                    Entidad.EXISTCONG = Values.EXISTCONG;
                    Entidad.BSECAPTIF = Values.BSECAPTIF;
                    Entidad.DB8INVFIS = Values.DB8INVFIS;
                    Entidad.STRCAMPO1 = Values.STRCAMPO1;
                    Entidad.STRCAMPO2 = Values.STRCAMPO2;
                    Entidad.STRCAMPO3 = Values.STRCAMPO3;
                    Entidad.NUMCAMPO4 = Values.NUMCAMPO4;
                    Entidad.NUMCAMPO5 = Values.NUMCAMPO5;
                    Entidad.NUMCAMPO6 = Values.NUMCAMPO6;
                    Entidad.apart = Values.Apart;
                    Entidad.invefisico = Values.Invefisico;
                    Entidad.USUARIO_TOMAINVE = Values.USUARIO_TOMAINVE;
                    Entidad.LUNES = Values.LUNES;
                    Entidad.MARTES = Values.MARTES;
                    Entidad.MIERCOLES = Values.MIERCOLES;
                    Entidad.JUEVES = Values.JUEVES;
                    Entidad.VIERNES = Values.VIERNES;
                    Entidad.SABADO = Values.SABADO;
                    Entidad.DIFERENCIA = Values.DIFERENCIA;
                    Entidad.cantidad_saco = Values.Cantidad_saco;
                    Entidad.SECUEN = Values.SECUEN;
                    Entidad.ENOFERTA = Values.ENOFERTA;
                    Entidad.PASILLOs = Values.PASILLOs;
                    Entidad.lado = Values.Lado;
                    Entidad.bandeja = Values.Bandeja;
                    Entidad.EXIST_ANTE = Values.EXIST_ANTE;
            db.SubmitChanges();
        
            return Entidad;
        }
        public CLIE_PREF ClientePrefUpdate(DB.CLIE_PREF Entidad, CLIE_PREFDTOs values)
        {//   Entidad.cve_pref = values.cve_pref;
            Entidad.Tarj_pref = values.Tarj_pref;
            Entidad.saludo = values.Saludo;
            Entidad.nombre = values.Nombre;
            Entidad.empresa = values.Empresa;
            Entidad.direccion = values.Direccion;
            Entidad.correoe = values.Correoe;
            Entidad.fecha_cumple = values.Fecha_cumple;
            Entidad.telefonos = values.Telefonos;
            Entidad.estado = values.Estado;
            Entidad.cedula = values.Cedula;
            Entidad.almacen = values.Almacen;
            Entidad.fecha_nacimiento = values.Fecha_nacimiento;
            Entidad.puntos = values.Puntos;
            db.SubmitChanges();
            return Entidad;
        }
            public CONC01 ClienteConcepUpdate(DB.CONC01 Entidad, CONC01DTOs values)
            {
                Entidad.NUM_REG = values.NUM_REG;
                //  Entidad.NUM_CPTO = values.NUM_CPTO;
                Entidad.DESCR = values.DESCR;
                Entidad.TIPO = values.TIPO;
                Entidad.CUEN_CONT = values.CUEN_CONT;
                Entidad.CON_REFER = values.CON_REFER;
                Entidad.GEN_CPTO = values.GEN_CPTO;
                Entidad.T_CRED = values.T_CRED;
                Entidad.CATEGORIA = values.CATEGORIA;
                Entidad.TIPO_TRANS = values.TIPO_TRANS;
                Entidad.NC = values.NC;
                Entidad.RETENCION = values.RETENCION;
                Entidad.TIPO_REF = values.TIPO_REF;
                Entidad.USA_BANCO = values.USA_BANCO;
            db.SubmitChanges();
            return Entidad;
            }
        public CONTAC01 ClienteCont(DB.CONTAC01 Entidad, CONTAC01DTOs Values)
        { 
            // Entidad.NUM_REG = Values.NUM_REG;
            Entidad.USRNAMESAE = Values.USRNAMESAE;
            Entidad.CCLIE = Values.CCLIE;
            Entidad.NOMBRE = Values.NOMBRE;
            Entidad.DIRECCION = Values.DIRECCION;
            Entidad.TELEFONO = Values.TELEFONO;
            Entidad.EMAIL = Values.EMAIL;
            Entidad.TIPOCONTAC = Values.TIPOCONTAC;
            Entidad.IDDEPTO = Values.IDDEPTO;
            // Entidad.secu = Values.secu;
            Entidad.CEDULA = Values.CEDULA;
            Entidad.CLV_TIPO = Values.CLV_TIPO;
            Entidad.NO_SOCIO = Values.NO_SOCIO;
            db.SubmitChanges();
            return Entidad;
        }
        public ALMACENE AlmacenUpdate(DB.ALMACENE Entidad, ALMACENESDTOs values)
        {
            Entidad.LETRA = values.LETRA;
            Entidad.DESC_ALMACEN = values.DESC_ALMACEN;
            Entidad.COD_TIPO_ALMACEN = values.COD_TIPO_ALMACEN;
            Entidad.ESTATUS = values.ESTATUS;
            Entidad.SEC_FACTURA_CONTADO = values.SEC_FACTURA_CONTADO;
            Entidad.SEC_FACTURA_CREDITO = values.SEC_FACTURA_CREDITO;
            Entidad.SEC_DEVOLUCION = values.SEC_DEVOLUCION;
            Entidad.SEC_COTIZACION = values.SEC_COTIZACION;
            Entidad.SEC_PEDIDO = values.SEC_PEDIDO;
            Entidad.SEC_CONDUCE = values.SEC_CONDUCE;
            Entidad.SEC_ENTRADA = values.SEC_ENTRADA;
            Entidad.SEC_SALIDA = values.SEC_SALIDA;
            Entidad.visible_compra = values.visible_compra;
            Entidad.RUTALOGO = values.RUTALOGO;
            Entidad.RUTA_ALM_DOCUMENTOS = values.RUTA_ALM_DOCUMENTOS;
            Entidad.SEC_FACTURA = values.SEC_FACTURA;
            Entidad.IMPRIME_CONDUCE = values.IMPRIME_CONDUCE;
            Entidad.SEC_FACTURACION = values.SEC_FACTURACION;
            Entidad.SEC_RECEPCION_MERC = values.SEC_RECEPCION_MERC;
            Entidad.SEC_ORDEN_COMP = values.SEC_ORDEN_COMP;
            Entidad.SEC_DEVOLUCION_MERC = values.SEC_DEVOLUCION_MERC;
            Entidad.DEVUELVE = values.DEVUELVE;
            Entidad.Toma_inventario = values.Toma_inventario;
            Entidad.SEC_GASTOS = values.SEC_GASTOS;
            Entidad.dir = values.dir;
            Entidad.Punto_Ventas = values.Punto_Ventas;
            Entidad.clv_vend = values.clv_vend;
            Entidad.sec_dev_gasto = values.sec_dev_gasto;
            Entidad.sec = values.sec;
            Entidad.TELEFONO = values.TELEFONO;
            Entidad.PrinterEtiqueta = values.PrinterEtiqueta;
            Entidad.DESC_INDEPENDIENTE = values.DESC_INDEPENDIENTE;
            Entidad.MONTO_OFERTA_IND = values.MONTO_OFERTA_IND;
            Entidad.PORC_OFERTA_IND = values.PORC_OFERTA_IND;
            Entidad.pedir_condicion = values.pedir_condicion;
            Entidad.promoSiempre = values.promoSiempre;
            Entidad.descprefalmacen = values.descprefalmacen;
            Entidad.Ofertaporcada1000 = values.Ofertaporcada1000;
            Entidad.NUM_CUADRE = values.NUM_CUADRE;
            Entidad.permite_transferencia = values.permite_transferencia;
            Entidad.SEC_TRANSF = values.SEC_TRANSF;
            Entidad.tiempoimagenes = values.tiempoimagenes;
            Entidad.rutaimagenes = values.rutaimagenes;
            Entidad.conduce_factura_varias = values.conduce_factura_varias;
            Entidad.cambio_precio_compra = values.cambio_precio_compra;         

            db.SubmitChanges();
            return Entidad;
        }

        public CLIN01 lineaUpdate(DB.CLIN01 Entidad, CLIN01DTOs Values)
        {
            
                
                Entidad.NUM_REG = Values.NUM_REG;
                Entidad.DESC_LIN = Values.DESC_LIN;
                Entidad.CUENTA_COI = Values.CUENTA_COI;
                Entidad.cuenta_coi_costo = Values.cuenta_coi_costo;
                Entidad.CUENTA_COI_VENTA = Values.CUENTA_COI_VENTA;           
            db.SubmitChanges();
            return Entidad;
        }

        public FABRICANTE FabricanteUpdate(DB.FABRICANTE Entidad, FABRICANTEDTOs values)
        {
            //  fABRICANTE.CODIGO_FAB = values.CODIGO_FAB;
            Entidad.DESCR_FAB = values.DESCR_FAB;
            Entidad.PAIS = values.PAIS;
            Entidad.ESTADO = values.ESTADO;        
            db.SubmitChanges();
            return Entidad;
         
        }
        public MOVS0101 MovimientoBancarioUpdate(DB.MOVS0101 Entidad, MOVS0101DTOs Values)
        {
            Entidad.NUM_REG = Values.NUM_REG;
            // Entidad.TIP_REG = Values.TIP_REG;
            Entidad.CON_PART = Values.CON_PART;
            //  Entidad.CTA_BAN = Values.CTA_BAN;
            Entidad.CVE_CONCEP = Values.CVE_CONCEP;
            Entidad.NUM_CHEQUE = Values.NUM_CHEQUE;
            Entidad.REF = Values.REF;
            Entidad.STATUS = Values.STATUS;
            Entidad.FECHA = Values.FECHA;
            Entidad.F_COBRO = Values.F_COBRO;
            Entidad.TIPO = Values.TIPO;
            Entidad.BAND_PRN = Values.BAND_PRN;
            Entidad.BAND_CONT = Values.BAND_CONT;
            Entidad.BAND_CONC = Values.BAND_CONC;
            Entidad.ACT_SAE = Values.ACT_SAE;
            Entidad.NUM_POL = Values.NUM_POL;
            Entidad.TIP_POL = Values.TIP_POL;
            Entidad.COBRADO = Values.COBRADO;
            Entidad.SAE_COI = Values.SAE_COI;
            Entidad.MONTO_TOT = Values.MONTO_TOT;
            Entidad.T_CAMBIO = Values.T_CAMBIO;
            Entidad.HORA = Values.HORA;
            Entidad.CLPV = Values.CLPV;
            Entidad.FACT = Values.FACT;
            Entidad.REFER = Values.REFER;
            Entidad.MONTO_DOC = Values.MONTO_DOC;
            Entidad.CTA_TRANSF = Values.CTA_TRANSF;
            Entidad.TRANSFER = Values.TRANSFER;
            Entidad.BENEF = Values.BENEF;
            Entidad.OBSER = Values.OBSER;
            Entidad.FECHA_LIQ = Values.FECHA_LIQ;
            Entidad.CVE_INST = Values.CVE_INST;
            Entidad.MONDIFSAE = Values.MONDIFSAE;
            Entidad.TCAMBIOSAE = Values.TCAMBIOSAE;
            Entidad.RFC = Values.RFC;
            Entidad.IVA = Values.IVA;
            Entidad.MONIVAACR = Values.MONIVAACR;
            Entidad.CONC_SAE = Values.CONC_SAE;
            Entidad.SOLICIT = Values.SOLICIT;
            Entidad.TRANS_COI = Values.TRANS_COI;
            Entidad.INTSAENOI = Values.INTSAENOI;
            Entidad.FORMA_PAGO = Values.FORMA_PAGO;
            Entidad.REVISADO = Values.REVISADO;
            Entidad.ASOCIADO = Values.ASOCIADO;
            Entidad.id_projecto = Values.id_projecto;
            Entidad.IDNUM_REG = Values.IDNUM_REG;          

            db.SubmitChanges();
            return Entidad;

        }
        public INVE01 ProductoUpdate(DB.INVE01 Entidad, INVE01DTOs Values)
        {


            Entidad.NUM_REG = Values.NUM_REG;
            // Entidad.CLV_ART = Values.CLV_ART;
            Entidad.STATUS = Values.STATUS;
            Entidad.DESCR = Values.DESCR;
            Entidad.NUM_SERIE = Values.NUM_SERIE;
            Entidad.UNI_MED = Values.UNI_MED;
            Entidad.UNI_EMP = Values.UNI_EMP;
            Entidad.LIN_PROD = Values.LIN_PROD;
            Entidad.CTRL_ALM = Values.CTRL_ALM;
            Entidad.IMPUESTO1 = Values.IMPUESTO1;
            Entidad.IMPUESTO2 = Values.IMPUESTO2;
            Entidad.PROVEEDOR1 = Values.PROVEEDOR1;
            Entidad.PROVEEDOR2 = Values.PROVEEDOR2;
            Entidad.TIEM_SURT = Values.TIEM_SURT;
            Entidad.STOCK_MIN = Values.STOCK_MIN;
            Entidad.STOCK_MAX = Values.STOCK_MAX;
            Entidad.TIP_COSTEO = Values.TIP_COSTEO;
            Entidad.TIPO_CAMB = Values.TIPO_CAMB;
            Entidad.PRECIO1 = Values.PRECIO1;
            Entidad.PRECIO2 = Values.PRECIO2;
            Entidad.PRECIO3 = Values.PRECIO3;
            Entidad.PRECIO4 = Values.PRECIO4;
            Entidad.PRECIO5 = Values.PRECIO5;
            Entidad.FCH_ULTCOM = Values.FCH_ULTCOM;
            Entidad.COMP_X_REC = Values.COMP_X_REC;
            Entidad.FCH_ULTVTA = Values.FCH_ULTVTA;
            Entidad.PEND_SURT = Values.PEND_SURT;
            Entidad.EXIST = Values.EXIST;
            Entidad.COSTO_PROM = Values.COSTO_PROM;
            Entidad.ULT_COSTO = Values.ULT_COSTO;
            Entidad.VTAS_ANL_C = Values.VTAS_ANL_C;
            Entidad.VTAS_ANL_M = Values.VTAS_ANL_M;
            Entidad.OBS_INV = Values.OBS_INV;
            Entidad.TIPO_ELE = Values.TIPO_ELE;
            Entidad.UNI_ALT = Values.UNI_ALT;
            Entidad.FAC_CONV = Values.FAC_CONV;
            Entidad.APART = Values.APART;
            Entidad.EXISCONG = Values.EXISCONG;
            Entidad.BLOTE = Values.BLOTE;
            Entidad.BPEDIMENTO = Values.BPEDIMENTO;
            Entidad.BSECAPTIF = Values.BSECAPTIF;
            Entidad.NUM_MON = Values.NUM_MON;
            Entidad.DB8INVFIS = Values.DB8INVFIS;
            Entidad.PESO = Values.PESO;
            Entidad.VOLUMEN = Values.VOLUMEN;
            Entidad.CAMPOSTRU1 = Values.CAMPOSTRU1;
            Entidad.CAMPOSTRU2 = Values.CAMPOSTRU2;
            Entidad.CAMPOSTRU3 = Values.CAMPOSTRU3;
            Entidad.CAMPOINTU = Values.CAMPOINTU;
            Entidad.CAMPODBL4 = Values.CAMPODBL4;
            Entidad.CAMPODBL8 = Values.CAMPODBL8;
            Entidad.COMPSANLC = Values.COMPSANLC;
            Entidad.COMPSANLM = Values.COMPSANLM;
            Entidad.CVEESQIMP = Values.CVEESQIMP;
            Entidad.COD_GRAB = Values.COD_GRAB;
            Entidad.CUENTA_CONTABLE = Values.CUENTA_CONTABLE;
            Entidad.CATEGORIA = Values.CATEGORIA;
            Entidad.COD_SUPLIDOR = Values.COD_SUPLIDOR;
            Entidad.LIBRABULTOS = Values.LIBRABULTOS;
            Entidad.OBSERVACION = Values.OBSERVACION;
            Entidad.LAYERS = Values.LAYERS;
            Entidad.PALETAS = Values.PALETAS;
            Entidad.MINIMO = Values.MINIMO;
            Entidad.VIDAUTIL = Values.VIDAUTIL;
            Entidad.DESCR_PROV = Values.DESCR_PROV;
            Entidad.Kits = Values.Kits;
            Entidad.Servicio = Values.Servicio;
            Entidad.Gestion_compra = Values.Gestion_compra;
            Entidad.TIPO_REG = Values.TIPO_REG;
            Entidad.CAMBIO = Values.CAMBIO;
            Entidad.CODIGO_SMKT = Values.CODIGO_SMKT;
            Entidad.SHORT_DESC = Values.SHORT_DESC;
            Entidad.LONG_DESC = Values.LONG_DESC;
            Entidad.DEPT_NUM = Values.DEPT_NUM;
            Entidad.FLAG_DESPACHO = Values.FLAG_DESPACHO;
            Entidad.FLAG_DESC = Values.FLAG_DESC;
            Entidad.FLAG_TAX = Values.FLAG_TAX;
            Entidad.FLAG_PREC_REQ = Values.FLAG_PREC_REQ;
            Entidad.FLAG_VBN = Values.FLAG_VBN;
            Entidad.MONTO_DESC = Values.MONTO_DESC;
            Entidad.FLAG_PESO_REQ = Values.FLAG_PESO_REQ;
            Entidad.precio1itbis = Values.Precio1itbis;
            Entidad.precio2itbis = Values.Precio2itbis;
            Entidad.precio3itbis = Values.Precio3itbis;
            Entidad.precio4itbis = Values.Precio4itbis;
            Entidad.precio5itbis = Values.Precio5itbis;
            Entidad.INVGRUM_CODIGO = Values.INVGRUM_CODIGO;
            Entidad.INVGRUD_CODIGO = Values.INVGRUD_CODIGO;
            Entidad.descr_abrev = Values.Descr_abrev;
            Entidad.REF_EAN = Values.REF_EAN;
            Entidad.CODIGO_FAB = Values.CODIGO_FAB;
            Entidad.CODIGO_MARCA = Values.CODIGO_MARCA;
            Entidad.PORCPREC1 = Values.PORCPREC1;
            Entidad.PORCPREC2 = Values.PORCPREC2;
            Entidad.PORCPREC3 = Values.PORCPREC3;
            Entidad.PORCPREC4 = Values.PORCPREC4;
            Entidad.PORCPREC5 = Values.PORCPREC5;
            Entidad.SECU = Values.SECU;
            Entidad.EAN = Values.EAN;
            Entidad.REFERENCIA = Values.REFERENCIA;
            Entidad.tiempo_entrega = Values.Tiempo_entrega;
            Entidad.EQUIVALENTES = Values.EQUIVALENTES;
            Entidad.CLV_ART_EQUI = Values.CLV_ART_EQUI;
            Entidad.CANTIDAD_EQUI = Values.CANTIDAD_EQUI;
            Entidad.DESCR_EQUI = Values.DESCR_EQUI;
            Entidad.StopCantidad = Values.StopCantidad;
            Entidad.fecha_vencimiento = Values.Fecha_vencimiento;
            Entidad.EnOferta = Values.EnOferta;
            Entidad.magellan = Values.Magellan;
            Entidad.IDARIES = Values.IDARIES;
            Entidad.porcprec6 = Values.Porcprec6;
            Entidad.precio6 = Values.Precio6;
            Entidad.precio6itbis = Values.Precio6itbis;
            Entidad.FECHA_CREA = Values.FECHA_CREA;
            Entidad.cant_sugerida = Values.Cant_sugerida;
            Entidad.costo_dollar = Values.Costo_dollar;
            Entidad.libre7 = Values.Libre7;
            Entidad.CAMPLIBRE7 = Values.CAMPLIBRE7;
            Entidad.CAMPLIBRE8 = Values.CAMPLIBRE8;
            Entidad.UNI_ENTRADA = Values.UNI_ENTRADA;
            Entidad.UNI_SALIDA = Values.UNI_SALIDA;
            Entidad.LOTE = Values.LOTE;
            Entidad.IMAGEN = Values.IMAGEN;
            Entidad.EXIST_ANTER = Values.EXIST_ANTER;
            Entidad.ULT_CANT_COMPRADA = Values.ULT_CANT_COMPRADA;
            Entidad.id_subsubgrupo = Values.Id_subsubgrupo;
            Entidad.cant_precio1 = Values.Cant_precio1;
            Entidad.cant_precio2 = Values.Cant_precio2;
            Entidad.cant_precio3 = Values.Cant_precio3;
            Entidad.cant_precio4 = Values.Cant_precio4;
            Entidad.cant_precio5 = Values.Cant_precio5;
            Entidad.comision_precio1 = Values.Comision_precio1;
            Entidad.comision_precio2 = Values.Comision_precio2;
            Entidad.comision_precio3 = Values.Comision_precio3;
            Entidad.comision_precio4 = Values.Comision_precio4;
            Entidad.comision_precio5 = Values.Comision_precio5;
            Entidad.ProdWeb = Values.ProdWeb;

            db.SubmitChanges();
            return Entidad;

        }
        public CLIE01 ClienteUpdate(DB.CLIE01 Entidad, CLIE01DTOs Values)
        {
            Entidad.NUM_REG = Values.NUM_REG;
            // Entidad.CCLIE = Values.CCLIE;
            Entidad.STATUS = Values.STATUS;
            Entidad.NOMBRE = Values.NOMBRE;
            Entidad.RFC = Values.RFC;
            Entidad.DIR = Values.DIR;
            Entidad.POB = Values.POB;
            Entidad.CODIGO = Values.CODIGO;
            Entidad.TELEFONO = Values.TELEFONO;
            Entidad.ATENCION = Values.ATENCION;
            Entidad.ATEN_COB = Values.ATEN_COB;
            Entidad.REV_PAG = Values.REV_PAG;
            Entidad.CLASIFIC = Values.CLASIFIC;
            Entidad.DIAS_CRE = Values.DIAS_CRE;
            Entidad.FCH_ULTCOM = Values.FCH_ULTCOM;
            Entidad.VEND = Values.VEND;
            Entidad.DESCUENTO = Values.DESCUENTO;
            Entidad.LIM_CRED = Values.LIM_CRED;
            Entidad.SALDO = Values.SALDO;
            Entidad.VTAS = Values.VTAS;
            Entidad.OBSER = Values.OBSER;
            Entidad.COLONIA = Values.COLONIA;
            Entidad.FAX = Values.FAX;
            Entidad.EMAIL = Values.EMAIL;
            Entidad.CURP = Values.CURP;
            Entidad.CVE_ZONA = Values.CVE_ZONA;
            Entidad.TELEFONOS = Values.TELEFONOS;
            Entidad.ULT_PAGOD = Values.ULT_PAGOD;
            Entidad.ULT_VENTD = Values.ULT_VENTD;
            Entidad.CAMLIBRE1 = Values.CAMLIBRE1;
            Entidad.CAMLIBRE2 = Values.CAMLIBRE2;
            Entidad.CAMLIBRE3 = Values.CAMLIBRE3;
            Entidad.ULTPAGOF = Values.ULTPAGOF;
            Entidad.RETXFLETE = Values.RETXFLETE;
            Entidad.CAMLIBRE4 = Values.CAMLIBRE4;
            Entidad.PORCRETEN = Values.PORCRETEN;
            Entidad.CAMLIBRE5 = Values.CAMLIBRE5;
            Entidad.ULT_PAGOM = Values.ULT_PAGOM;
            Entidad.ULT_VENTM = Values.ULT_VENTM;
            Entidad.CAMLIBRE6 = Values.CAMLIBRE6;
            Entidad.DESC_FIN = Values.DESC_FIN;
            Entidad.LISTA_PRECIO = Values.LISTA_PRECIO;
            Entidad.VEND_FRIO = Values.VEND_FRIO;
            Entidad.VEND_PAPEL = Values.VEND_PAPEL;
            Entidad.VEND_PAN = Values.VEND_PAN;
            Entidad.VEND_SECO = Values.VEND_SECO;
            Entidad.VEND_OTROS = Values.VEND_OTROS;
            Entidad.OBSERVACION = Values.OBSERVACION;
            Entidad.vend_harina = Values.Vend_harina;
            Entidad.VEND_FOOD = Values.VEND_FOOD;
            Entidad.DOMICILIO = Values.DOMICILIO;
            Entidad.DIR_ENVIO = Values.DIR_ENVIO;
            Entidad.CREDITO_SMA = Values.CREDITO_SMA;
            Entidad.paga_impuesto = Values.Paga_impuesto;
            Entidad.SEGURO = Values.SEGURO;
            Entidad.FECHA_CREA = Values.FECHA_CREA;
            Entidad.CLV_TIPO = Values.CLV_TIPO;
            Entidad.DIA_VISITA = Values.DIA_VISITA;
            Entidad.CODIGO_POSTAL = Values.Codigo_postal;
            Entidad.CUENTA_CONT = Values.Cuenta_cont;
            Entidad.CAMLIBRE7 = Values.Camlibre7;
            Entidad.CAMLIBRE8 = Values.Camlibre8;
            Entidad.CAMLIBRE6 = Values.CAMLIBRE6;
            Entidad.CAMLIBRE1 = Values.CAMLIBRE1;
            Entidad.CAMLIBRE2 = Values.CAMLIBRE2;
            Entidad.CAMLIBRE3 = Values.CAMLIBRE3;
            Entidad.CAMLIBRE4 = Values.CAMLIBRE4;
            Entidad.CAMLIBRE5 = Values.CAMLIBRE5;
            Entidad.precioclie = Values.Precioclie;
            Entidad.genera_boleto = Values.Genera_boleto;
            Entidad.gerena_boleto = Values.Gerena_boleto;
            db.SubmitChanges();
            return Entidad;

        }
        public CAJA01 CajaUpdate(DB.CAJA01 Entidad, CAJA01DTOs values)
        {

            // Entidad.ID_CAJA = values.ID_CAJA;
            Entidad.DESCR_CAJA = values.DESCR_CAJA;
            Entidad.ABRECAJON = values.ABRECAJON;
            Entidad.ALMACEN = values.ALMACEN;
            Entidad.LETRA = values.LETRA;
            Entidad.Sec_Facturacion = values.Sec_Facturacion;
            Entidad.SEC_DEVOLUCION = values.SEC_DEVOLUCION;
            Entidad.SEC_COTIZACION = values.SEC_COTIZACION;
            Entidad.EQUIPO = values.EQUIPO;
            Entidad.tienda = values.tienda;
            Entidad.NIVEL = values.NIVEL;
            Entidad.CAJA_FISCAL = values.CAJA_FISCAL;
            Entidad.CONCOPIA_FISCAL = values.CONCOPIA_FISCAL;
            Entidad.SEC_PEDIDO = values.SEC_PEDIDO;
            Entidad.AMitad = values.AMitad;
            Entidad.AdicionarTarjeta = values.AdicionarTarjeta;
            Entidad.usamagellan = values.usamagellan;
            Entidad.magellanc = values.magellanc;
            Entidad.PRINTERFISCAL = values.PRINTERFISCAL;
            Entidad.TIPOTARJETA = values.TIPOTARJETA;
            Entidad.TIPOTARJETA2 = values.TIPOTARJETA2;
            Entidad.COPIATARJETA = values.COPIATARJETA;
            Entidad.magellandivide = values.magellandivide;
            Entidad.magellantimeout = values.magellantimeout;
            Entidad.abrecajonextencion = values.abrecajonextencion;
            Entidad.CAJON = values.CAJON;
            Entidad.TIPOAZUL = values.TIPOAZUL;
            Entidad.VALIDAPRECIOCERO = values.VALIDAPRECIOCERO;
            Entidad.multipantalla = values.multipantalla;
            Entidad.ZEBRASCAN = values.ZEBRASCAN;
            Entidad.puertobixolon = values.puertobixolon;

            db.SubmitChanges();
            return Entidad;
        }

    }
}