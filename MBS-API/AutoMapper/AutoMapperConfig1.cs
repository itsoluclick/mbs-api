﻿using AutoMapper;
using Infraestructura.DTOs;
using MBS_API.DB;

namespace MBS_API.AutoMapper
{
    public class AutoMapperConfig1
    {
        public static IMapper _Imapper { get; set; }

        public static void init()
        {
            var Config = new MapperConfiguration(Cfg =>
            {
                Cfg.CreateMap<MARCADTOs, MARCA>();
                Cfg.CreateMap<MARCA, MARCADTOs>();
                Cfg.CreateMap<DGII_RNC, DGII_RNCDTOs>();
                Cfg.CreateMap<DGII_RNCDTOs, DGII_RNC>();
                Cfg.CreateMap<EMPRESASDTOs, EMPRESAS>();
                Cfg.CreateMap<EMPRESAS, EMPRESASDTOs>();
                Cfg.CreateMap<MOVS0101DTOs, MOVS0101>();
                Cfg.CreateMap<MOVS0101, MOVS0101DTOs>();
                Cfg.CreateMap<invefisico_fecha, InventariofisicoFechaDTOs>();
                Cfg.CreateMap<InventariofisicoFechaDTOs, invefisico_fecha>();
            });
            _Imapper = Config.CreateMapper();
           
        }



    }
}