﻿using AutoMapper;
using Infraestructura.DTOs;
using MBS_API.DB;

namespace MBS_API.AutoMapper
{
    public class AutoMapperConfig
    {
        public static IMapper   Mapper{ get;private  set; }
        public static void init()
        {
            var Config = new MapperConfiguration(cfg =>
             {
                 cfg.CreateMap<MINV01, MINV01DTOs>();
                 cfg.CreateMap<MINV01DTOs, MINV01>();
                 cfg.CreateMap<CTAS01, CTAS01DTOs>();
                 cfg.CreateMap<CTAS01DTOs, CTAS01>();
                 cfg.CreateMap<ALMACENE, ALMACENESDTOs>();
                 cfg.CreateMap<ALMACENESDTOs, ALMACENE>();
                 cfg.CreateMap<Caja_chica, Caja_chicaDTOs>();
                 cfg.CreateMap<Caja_chicaDTOs, Caja_chica>();
                 cfg.CreateMap<CajaChica_DETDTOs, CajaChica_DET>();
                 cfg.CreateMap<CajaChica_DET,CajaChica_DETDTOs> ();
                 cfg.CreateMap<CAJA01, CAJA01DTOs>();
                 cfg.CreateMap<CAJA01DTOs, CAJA01>();
                 cfg.CreateMap<cajaChica_enc, cajaChica_encDTOs>();
                 cfg.CreateMap<cajaChica_encDTOs, cajaChica_enc>();
                 cfg.CreateMap<MULT01, MULT01DTOs>();
                 cfg.CreateMap<MULT01DTOs, MULT01DTOs>();
                 cfg.CreateMap<CONTAC01, CONTAC01DTOs>();
                 cfg.CreateMap<CONTAC01DTOs, CONTAC01>();
                 cfg.CreateMap<CLIE_PREF, CLIE_PREFDTOs>();
                 cfg.CreateMap<CLIE_PREFDTOs, CLIE_PREF>();
                 cfg.CreateMap<CLIE01, CLIE01DTOs>();
                 cfg.CreateMap<CLIE01DTOs, CLIE01>();
                 cfg.CreateMap<CONC01, CONC01DTOs>();
                 cfg.CreateMap<CONC01DTOs, CONC01>();
                 cfg.CreateMap<CLIN01, CLIN01DTOs>();
                 cfg.CreateMap<CLIN01DTOs, CLIN01>();
                 cfg.CreateMap<COM0Y1TEMP1, COM0Y1TEMP1DTOs>();
                 cfg.CreateMap<COM0Y1TEMP1DTOs, COM0Y1TEMP1>();
                 cfg.CreateMap<COMP01, COMP01DTOs>();
                 cfg.CreateMap<COMP01DTOs, COMP01>();
                 cfg.CreateMap<CONP01, CONP01DTOs>();
                 cfg.CreateMap<CONP01DTOs, CONP01>();
                 cfg.CreateMap<CUEN01, CUEN01DTOs>();
                 cfg.CreateMap<CUEN01DTOs, CUEN01>();
                 cfg.CreateMap<DEPTODTOs, DEPTO>();
                 cfg.CreateMap<DEPTO, DEPTODTOs>();
                 cfg.CreateMap<FA0TY1DTOs, FA0TY1>();
                 cfg.CreateMap<FA0TY1, FA0TY1DTOs>();
                 cfg.CreateMap<FABRICANTEDTOs, FABRICANTE>();
                 cfg.CreateMap<FABRICANTE, FABRICANTEDTOs>();
                 cfg.CreateMap<FACT01, FACT01DTOs>();
                 cfg.CreateMap<FACT01DTOs, FACT01>();
                 cfg.CreateMap<IMPU01, IMPU01DTOs>();
                 cfg.CreateMap<IMPU01DTOs, IMPU01>();
                 cfg.CreateMap<INVE01DTOs, INVE01>();
                 cfg.CreateMap<INVE01, INVE01DTOs>();
                 cfg.CreateMap<INVGRUDDTOs, INVGRUD>();
                 cfg.CreateMap<INVGRUD, INVGRUDDTOs>();
                 cfg.CreateMap <INVGRUM, INVGRUMDTOs> ();
                 cfg.CreateMap <INVGRUMDTOs, INVGRUM> ();
                 cfg.CreateMap <MINV01, MINV01DTOs> ();
                 cfg.CreateMap <MINV01DTOs, MINV01> ();              
                 cfg.CreateMap <PROV01, PROV01DTOs> ();
                 cfg.CreateMap <PROV01DTOs, PROV01> ();
                 cfg.CreateMap <SUBDEPTODTOs, SUBDEPTO> ();
                 cfg.CreateMap <SUBDEPTO, SUBDEPTODTOs> ();
                 cfg.CreateMap <TB_REC_DETALLEDTOs, TB_REC_DETALLE> ();
                 cfg.CreateMap <TB_REC_DETALLE, TB_REC_DETALLEDTOs> ();

             });
            Mapper = Config.CreateMapper();



        }
    }


}